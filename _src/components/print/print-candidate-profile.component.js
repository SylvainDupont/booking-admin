/**
 * PrintCandidateProfileComponent
 * @desc angular component printCandidateProfile
 */
class PrintCandidateProfileComponent {

	/**
     * Constructor
     * @desc Create DashboardComponent
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {		
		this.templateUrl = './components/print/print-candidate-profile.component.html';
		this.controller = PrintCandidateProfileComponentController;
	}
}

/**
 * PrintCandidateProfileComponentController
 * @desc angular component printCandidateProfile controller
 */
class PrintCandidateProfileComponentController {

	/**
	 * constructor
	 * @desc Create PrintCandidateProfileComponentController
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} $timeout - service from angular-core
	 */
	constructor(PromoterService, $timeout) {
		this.PromoterService = PromoterService;
		this.$timeout = $timeout;
	}

	/**
     * $routerOnActivate
     * @desc On router component activate
     * @param {object} next - router params
     */
	$routerOnActivate(next) {
		this.getCandidate(next.params.candidateId);
	}

	/**
	 * getCandidate
	 * @desc Get candidate profile
	 * @param {object} candidateId - candidate uid
	 */
	getCandidate(candidateId) {
		this.candidateReference = this.PromoterService.getCandidate(candidateId);
		this.candidateReference.$loaded((candidate) => {
			this.initCandidate(candidate);

			this.candidateReference.$watch(() => {
				this.initCandidate(candidate);
			});
		});
	}

	/**
	 * initCandidate
	 * @desc Initialize candidate informations to display it
	 */
	initCandidate(candidate) {
		this.candidate = candidate;
		this.candidate.getSelectedOffer();
		this.candidate.getApplyOffer();
		this.candidate.offerLength = Object.keys(this.candidate.missions).length;

		this.$timeout(() => {
			this.saveToPDF();
		});
	}

	/**
	 * saveToPDF
	 * @desc Transform HTML page to PDF file
	 */
	saveToPDF() {

		let self = this;
		let printContainer = document.querySelector('.print-area');

		html2canvas(printContainer, {
		  	onrendered: function(canvas) {

		  		let imgData = canvas.toDataURL('image/png'); 
		  		let imgWidth = 210; 
				let pageHeight = 295;  
				let imgHeight = canvas.height * imgWidth / canvas.width;
				let heightLeft = imgHeight;
				let pdf = new jsPDF('p', 'mm');
				let position = 0;

				pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
				heightLeft -= pageHeight;

				while (heightLeft >= 0) {
				  position = heightLeft - imgHeight;
				  pdf.addPage();
				  pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
				  heightLeft -= pageHeight;
				}

				pdf.save(`candidat-booking-hotesses-${self.candidate.$id}.pdf`);         
		  	}
		});
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'PromoterService',
			'$timeout'
		];
	}
}

/**
 * @module print-candidate-profile
 */
angular
	.module('print-candidate-profile', [])
	.component('printCandidateProfile', new PrintCandidateProfileComponent());