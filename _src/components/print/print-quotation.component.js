/**
 * PrintQuotationComponent
 * @desc angular component printQuotation
 */
class PrintQuotationComponent {

	/**
     * Constructor
     * @desc Create PrintQuotationComponent
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
    constructor() {
        this.templateUrl = './components/print/print-quotation.component.html';
        this.controller = PrintQuotationComponentController;
    }
}

/**
 * PrintQuotationComponentController
 * @desc angular component printQuotation controller
 */
class PrintQuotationComponentController {

	/**
	 * constructor
	 * @desc Create PrintQuotationComponentController
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} $timeout - service from angular-core
	 */
    constructor(PromoterService, $timeout) {
        this.PromoterService = PromoterService;
        this.$timeout = $timeout;
    }

	/**
     * $routerOnActivate
     * @desc On router component activate
     * @param {object} next - router params
     */
    $routerOnActivate(next) {
        this.quotation = this.PromoterService.getQuotationById(next.params.quotationId);
        this.quotation.$loaded(() => {
            this.$timeout(() => {
			    this.saveToPDF();
		    });
        });
    }

	/**
	 * saveToPDF
	 * @desc Transform HTML page to PDF file
	 */
	saveToPDF() {

		let self = this;
		let printContainer = document.querySelector('.print-area');

		html2canvas(printContainer, {
		  	onrendered: function(canvas) {

		  		let imgData = canvas.toDataURL('image/png'); 
		  		let imgWidth = 210; 
				let pageHeight = 295;  
				let imgHeight = canvas.height * imgWidth / canvas.width;
				let heightLeft = imgHeight;
				let pdf = new jsPDF('p', 'mm');
				let position = 0;

				pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
				heightLeft -= pageHeight;

				while (heightLeft >= 0) {
				  position = heightLeft - imgHeight;
				  pdf.addPage();
				  pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
				  heightLeft -= pageHeight;
				}

				pdf.save(`booking-hotesses-devis-n°${self.quotation.$id}.pdf`);         
		  	}
		});
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array}
     */
    static get $inject() {
        return [
            'PromoterService',
            '$timeout'
        ];
    }
}

/**
 * @module print-quotation
 */
angular
    .module('print-quotation', [])
    .component('printQuotation', new PrintQuotationComponent());

