/**
 * CandidatesDisabledComponent
 * @desc angular component candidateDisabled
 */
class CandidatesDisabledComponent {

    /**
     * Constructor
     * @desc Create CandidatesDisabledComponent
     * @param {object} require - component require
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
    constructor() {
        this.require = {
            parent: '^candidates'
        };

        this.templateUrl = './components/candidates/candidates-disabled.component.html';
        this.controller = CandidatesDisabledComponentController;
    }
}

/**
 * CandidatesDisabledComponent
 * @desc angular component candidateDisabled controller
 */
class CandidatesDisabledComponentController {

    /**
	 * constructor
	 * @desc Create CandidatesDisabledComponentController
     * @param {object} $rootScope - service from angular-core
     * @param {object} $timeout - service from angular-core
	 */
    constructor($rootScope, $timeout) {
        this.$rootScope = $rootScope;
        this.$timeout = $timeout;
    }

    /**
     * $onInit
     * @desc On component init
     */
    $onInit() {
        this.candidatesReference = this.parent.candidatesReference;
        this.limit = 20;
        this.end = false;
        this.isFilterOpen = false;
    }

    /**
     * $routerOnActivate
     * @desc On router component activate
     * @param {object} next - router params
     */
    $routerOnActivate(next) {
        this.parent.selectedTabIndex = next.routeData.data.tabIndex;

        this.candidatesReference.$loaded((candidates) => {
            this.initializeCandidates(candidates);

            this.candidatesReference.$watch(() => {
                this.initializeCandidates(candidates);
            });
        });
    }

    /**
     * initializeCandidates
     * @desc Initialize candidates list
     * @param {array} candidates
     */
    initializeCandidates (candidates) {

        this.candidates = [];

        for(let candidate of candidates) {
            if(candidate.disabled) {
                this.candidates.push(candidate);
            }
        }

        if(this.limit >= this.candidates.length) {
            this.end = true;
        }
    }

    /**
     * getMore
     * @desc Get more candidates item
     */
    getMore() {
        this.limit += 50;

        if(this.limit >= this.candidates.length) {
            this.end = true;
        }
    }

    /**
     * toggleFilter
     * @desc Toggle filter bar
     */
    toggleFilter() {
        this.isFilterOpen = !this.isFilterOpen;
    }

    /**
     * resetFilter
     * @desc Reset filter bar
     */
    resetFilter() {
        this.searchSexe = undefined;
        this.searchVehicle = undefined;
        this.searchActive = undefined;
        this.searchLanguage = undefined;
        this.searchPerformance = undefined;
    		this.searchDispo = undefined;
    		this.searchSpecialite = undefined;
    		this.searchMarque = undefined;
        this.searchState = undefined;
        this.searchDate = undefined;
        this.searchDepartment = undefined;
        this.searchValidate = undefined;
    }

    /**
     * $inject
     * @desc Inject dependencies
     * @return {Array}
     */
    static get $inject() {
        return [
            '$rootScope',
            '$timeout'
        ];
    }
}

/**
 * @module candidates/candidates-disabled
 */
angular
    .module('candidates/candidates-disabled', [])
    .component('candidateDisabled', new CandidatesDisabledComponent());
