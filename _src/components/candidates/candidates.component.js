/**
 * CandidatesComponent
 * @desc angular component candidates
 */
class CandidatesComponent {

	/**
     * Constructor
     * @desc Create CandidatesComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
	 * @param {array} $routeConfig - component router configuration
     * @param {function} controller - component controller
     */
	constructor() {
		this.bindings = {
			$router: '<'
		};
		this.templateUrl = './components/candidates/candidates.component.html';
		this.$routeConfig = [
			{
                path: '/inscris',
                name: 'CandidateSubscribe',
                component: 'candidateSubscribe',
                useAsDefault: true,
                data: {
                	tabIndex: 0
                }
            },
            // {
            //     path: '/non-inscris',
            //     name: 'CandidateUnsubscribe',
            //     component: 'candidateUnsubscribe',
            //     data: {
            //     	tabIndex: 1
            //     }
            // },
            {
                path: '/desactive',
                name: 'CandidateDisabled',
                component: 'candidateDisabled',
                data: {
                	tabIndex: 2
                }
            }
		];
		this.controller = CandidatesComponentController;
	}
}

/**
 * CandidatesComponentController
 * @desc angular component candidates controller
 */
class CandidatesComponentController {

	/**
	 * constructor
	 * @desc Create CandidatesComponentController
	 * @param {object} CandidatesService - service manage candidates
	 * @param {object} $rootRouter - service from angular-component-router
	 * @param {object} $mdToast - service from angular-material
	 * @param {object} $mdDialog - service from angular-material
	 * @param {object} $timeout - service from angular-core
	 * @param {object} ToastService - service manage $mdToast from angular-material
	 * @param {object} $rootScope - service from angular-core
	 * @param {object} $firebaseObject - service from angularfire
	 */
	constructor (CandidatesService, $rootRouter, $mdToast, $mdDialog, $timeout, ToastService, $rootScope, $firebaseObject, ProfessionsService,SpecialitesService,MarquesService) {
		this.candidatesService = CandidatesService;
		this.$rootRouter = $rootRouter;
		this.$mdToast = $mdToast;
		this.$mdDialog = $mdDialog;
		this.$timeout = $timeout;
		this.toastService = ToastService;
		this.$rootScope = $rootScope;
		this.$firebaseObject = $firebaseObject;
		this.ProfessionsService = ProfessionsService;
		this.SpecialitesService = SpecialitesService;
		this.MarquesService = MarquesService;

		this.candidatesReference = this.candidatesService.get();
		this.viewCandidateProfile = this.viewCandidateProfile.bind(this);
		this.viewCandidatePicture = this.viewCandidatePicture.bind(this);
		this.professions = this.ProfessionsService.get();
		this.specialites = this.SpecialitesService.get();
		this.marques = this.MarquesService.get();
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit () {
		this.candidatesLength = 0;
		this.selectedTabIndex = 0;

		/**
		 * Router tab configuration
		 * @type {Array}
		 */
		this.routerTabConfig = [
			{
				router: 'CandidateSubscribe',
				label: 'Inscrits'
			},
			// {
			// 	router: 'CandidateUnsubscribe',
			// 	label: 'Non inscris'
			// },
			{
				router: 'CandidateDisabled',
				label: 'Désactivés'
			}
		];

		this.candidateLengthReference = this.$firebaseObject(
			firebase
				.database()
				.ref('config')
				.child('candidateLength')
		);

		this.candidateLengthReference.$loaded((candidateLength) => {
			this.candidatesLength = candidateLength.$value;
		});

		// this.loadingToast = this.$mdToast.show({
		// 	templateUrl: './components/toast/toast-loading.template.html',
		// 	hideDelay: 0,
		// 	position: 'bottom left'
		// });

		this.$timeout(() => {
			this.getCandidates();
		}, 1000);
	}

	/**
	 * $onDestroy
	 * @desc On component destroy
	 */
	$onDestroy() {
		this.candidatesReference.$destroy();
		// this.$mdToast.hide(this.loadingToast);
	}

	/**
	 * getCandidates
	 * @desc Get candidates list
	 */
	getCandidates () {

		this.candidatesReference.$loaded((candidates) => {
			this.candidatesLength = candidates.length;
			// this.$mdToast.hide(this.loadingToast);

			this.candidatesReference.$watch((e) => {
				switch(e.event) {
					case 'child_added':
						this.toastService.show({
							content: 'Vous avez un nouveau candidat',
							type: 'simple',
							position: 'bottom right'
						});
						break;
				}
			});
		});
	}

	/**
	 * routerTab
	 * @desc Navigate between components tabs
	 * @param {String} route - router name
	 */
	routerTab (route) {
		this.$router.navigate([route]);
	}

	viewCV(fileName, uid){
		const storage = firebase.storage();
		const userPicture = storage.ref('userPictures').child(uid);
		const finalName = (fileName.indexOf('.pdf') > 0) ? 'curriculum.pdf' : ((fileName.indexOf('.docx') ? 'curriculum.docx' : 'curriculum.doc'));
		userPicture.child(finalName).getDownloadURL().then((url) => {
			let a = document.createElement('a');
			a.target = "_blank";
			a.href = url;
			a.click();
		});
	}
	/**
	 * toggleActive
	 * @desc Toggle candidate active property
	 * @param {String} candidateId
	 */
	toggleActive (candidateId) {
		this.candidatesService.toggleActive(candidateId);
	}

	/**
	 * viewCandidateProfile
	 * @desc View candidate profile details
	 * @param {object} candidate
	 */
	viewCandidateProfile (candidate) {

		this.candidate = candidate;
		const dialog = {
			templateUrl: './components/candidates/candidate.profile.html',
			parent: angular.element(document.body),
			controller: CandidateProfilController,
  			controllerAs: '$ctrl',
			fullscreen: true,
			locals:{
				editActive: false,
				thisController: this,
				candidate: this.candidate,
				chosenPlaceDetails: {}
			},
			skipHide: true
		};

		// this.$mdDialog.show(dialog);
	
		function CandidateProfilController($scope, $mdDialog, editActive,thisController,candidate,chosenPlaceDetails, $rootScope){
			$scope.editActive = editActive;
			$scope.chosenPlaceDetails = chosenPlaceDetails;
			$scope.candidate = candidate;
			$scope.toggleActive = function() {
				$scope.editActive = true;
				window.setTimeout(()=>{
					var options = {
						types: [],
						componentRestrictions: {}
					};
					$scope.gPlace = new google.maps.places.Autocomplete(document.getElementById('googleplace'), options);
					google.maps.event.addListener($scope.gPlace, 'place_changed', function() {
						$scope.$apply(function() {
							let n = $scope.gPlace.getPlace();
							for(let i = 0; i < n.address_components.length; i ++){
								if(n.address_components[i].types.indexOf('postal_code') != -1){
									$scope.candidate.zip = n.address_components[i].long_name;
								}
								if(n.address_components[i].types.indexOf('locality') != -1){
									$scope.candidate.city = n.address_components[i].long_name;
								}
								if(n.address_components[i].types.indexOf('administrative_area_level_2') != -1){
									$scope.candidate.mobility = n.address_components[i].long_name;
								}
							}
							$scope.candidate.address_lat = n.geometry.location.lat();
							$scope.candidate.address_lng = n.geometry.location.lng();
							$scope.candidate.fulladdress = n.formatted_address;
						});
					});
				},1000);
			};
			$scope.viewCV = function(fileName, uid) {
				thisController.viewCV(fileName, uid);
			};
			$scope.viewCandidatePicture = function($event, index) {
				thisController.viewCandidatePicture ($event, index);
			}
			$scope.setValidAccount = function(candidateId, code) {
				thisController.setValidAccount();
			};
			$scope.setInvalidAccount = function(candidateId, code) {
				thisController.setValidAccount();
			};
			$scope.validUpdateAdresse = function() {
				$scope.editActive = false;
				thisController.saveAdress($scope.candidate);
			};
		}
		CandidateProfilController.$inject = ["$scope", "$mdDialog", "editActive", "thisController","candidate","chosenPlaceDetails", "$rootScope"];
		this.$mdDialog.show(dialog);
	}
	saveAdress (c) {
		this.candidatesService.saveAdress(c);
	}
	/**
	 * viewCandidatePicture
	 * @desc View candidate profile pictures
	 * @param {object} $event - targetEvent
	 * @param {int} index - picture index
	 */
	viewCandidatePicture ($event, index) {

		this.selectedPictureIndex = index;

		const dialog = {
			templateUrl: './components/candidates/candidate.profile.picture.html',
			parent: angular.element(document.body),
			targetEvent: $event,
			clickOutsideToClose: false,
			controller: () => this,
  			controllerAs: '$ctrl',
			fullscreen: true,
			hasBackdrop: false,
			skipHide: true
		};

		this.$mdDialog.show(dialog);
	}

	/**
	 * setInvalidAccount
	 * @desc Change candidate account to invalid
	 * @param {string} candidateId
	 * @param {string} type
	 */
	setInvalidAccount (candidateId, code) {
		this.candidatesService.setInvalidAccount(candidateId, code);
	}

	/**
	 * setValidAccount
	 * @desc Change candidate account to valid
	 * @param {string} candidateId
	 * @param {string} type
	 */
	setValidAccount (candidateId, code) {
		this.candidatesService.setValidAccount(candidateId, code);
	}

	/**
	 * disableAccount
	 * @desc Disable candidate account
	 * @param  {String} candidateId
	 */
	disableAccount(candidateId) {
		this.candidatesService.disableAccount(candidateId);
	}

	/**
	 * enableAccount
	 * @desc Enable candidate account
	 * @param  {String} candidateId
	 */
	enableAccount(candidateId) {
		this.candidatesService.enableAccount(candidateId);
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
    static get $inject() {
      	return [
        	'CandidatesService',
        	'$rootRouter',
        	'$mdToast',
        	'$mdDialog',
        	'$timeout',
        	'ToastService',
        	'$rootScope',
        	'$firebaseObject',
        	'ProfessionsService',
        	'SpecialitesService',
        	'MarquesService'
      	];
    }
}

/**
 * @module candidates
 */
angular
	.module('candidates', [
		'candidates/candidates-subscribe',
        // 'candidates/candidates-unsubscribe',
        'candidates/candidates-disabled',
	])
	.component('candidates', new CandidatesComponent());
