/**
 * ProfessionsListComponent
 * @desc angular component ProfessionsList
 */
class ProfessionsListComponent {

	/**
     * Constructor
     * @desc Create ProfessionsListComponent
     * @param {object} require - component require
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor () {
		this.require = {
			parent: '^professions'
		};
		this.templateUrl = './components/profession/profession-list.component.html';
		this.controller = ProfessionsListComponentController;
	}
}

/**
 * ProfessionsListComponentController
 * @desc angular component ProfessionsList controller
 */
class ProfessionsListComponentController {

	/**
	 * constructor
	 * @desc Create ProfessionsListComponentController
	 */
	constructor () {
		this.newProfession = {
			name: ''
		};
		this.currentUpdateId = {
			onUpdate: false,
			id: null
		};
  }

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.professions = [];
		this.limit = 50;
		this.end = false;
		this.isFilterOpen = false;

		this.parent.professionsReference.$loaded((professions) => {
			this.getProfessions(professions);
		});
	}

	/**
	 * getProfessions
	 * @desc Get professions list
	 * @param {array} professions - professions list
	 */
	getProfessions(professions) {
		this.professions = professions.sortList('name');
	}

	addProfession($event, profession){
		if($event.keyCode==13 && profession && profession.name.length > 0){
			this.parent.addProfession(profession);
			this.newProfession = {
				name: ''
			};
		}
	}

	valideUpdateProfession($event, profession, currentUpdateId){
		if($event.keyCode==13 && profession && profession.name.length > 0){
			this.parent.valideUpdateProfession(profession, currentUpdateId);
		}
	}

	valideUpdateProfessionBtn(profession, currentUpdateId){
		if(profession && profession.name.length > 0){
			this.parent.valideUpdateProfession(profession, currentUpdateId);
		}
	}

	showToUpdate(profession){
		this.currentUpdateId.onUpdate = true;
		this.currentUpdateId.id = profession.$id;
	}
}

/**
 * @module professions/professions-card
 */
angular
	.module('professions/professions-list', [])
	.component('professionsList', new ProfessionsListComponent());
