/**
 * ProfessionComponent
 * @desc angular component loader
 */
class ProfessionComponent {

	/**
     * Constructor
     * @desc Create professionsTransfertComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
	 	 * @param {array} $routeConfig - component router configuration
     * @param {function} controller - component controller
  */
	constructor () {
		this.bindings = {
			$router: '<'
		};
		this.$routeConfig = [
			{
                path: '/index',
                name: 'ProfessionsList',
                component: 'professionsList',
                useAsDefault: true,
                data: {
                	tabIndex: 0
                }
            },
            {
                path: '/specialites',
                name: 'SpecialitiesList',
                component: 'specialitiesList',
                data: {
                	tabIndex: 1
                }
            },
            {
                path: '/marques',
                name: 'MarquesList',
                component: 'marquesList',
                data: {
                	tabIndex: 2
                }
            }
		];
		this.templateUrl = './components/profession/profession.component.html';
		this.controller = ProfessionComponentController;
	}
}

/**
 * ProfessionComponentController
 * @desc angular component loader controller
 */
class ProfessionComponentController {
	/**
	 * constructor
	 * @desc Create professionsComponentController
	 * @param {object} ProfessionsService - service manage professions
	 * @param {object} SpecialitesService - service manage specialites
	 * @param {object} $mdPanel - service from angular-material
	 * @param {object} $rootRouter - service from angular-component-router
	 * @param {object} $mdToast - service from angular-material
	 * @param {object} $mdDialog - service from angular-material
	 * @param {object} $timeout - service from angular-core
	 * @param {object} $rootScope - service from angular-core
	 */
	constructor (ProfessionsService, SpecialitesService, MarquesService, $mdPanel, $rootRouter, $mdToast, $mdDialog, $timeout, $rootScope) {
		this.professionsService = ProfessionsService;
		this.specialitesService = SpecialitesService;
		this.marquesService = MarquesService;
		this.$mdPanel = $mdPanel;
		this.$rootRouter = $rootRouter;
		this.$mdToast = $mdToast;
		this.$mdDialog = $mdDialog;
		this.$timeout = $timeout;
		this.$rootScope = $rootScope;

		this.professionsReference = this.professionsService.get();
		this.specialitesReference = this.specialitesService.get();
		this.marquesReference = this.marquesService.get();
		/**
		 * Router tabs configuration
		 * @type {Array}
		 */
		this.routerTabConfig = [
			{
				router: 'ProfessionsList',
				label: 'Professions'

			},
			{
				router: 'SpecialitiesList',
				label: 'Specialités'

			},
			{
				router: 'MarquesList',
				label: 'Marques'
			}
		];
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.professionsReference = [];
		this.specialitesReference = [];
		this.marquesReference = [];

		this.selectedTabIndex = 0;

		// this.loadingToast = this.$mdToast.show({
		// 	templateUrl: './components/toast/toast-loading.template.html',
		// 	hideDelay: 0,
		// 	position: 'bottom left'
		// });

		this.professionsReference = this.professionsService.get();
		this.specialitesReference = this.specialitesService.get();
		this.marquesReference = this.marquesService.get();

		// this.professionsReference.$loaded((professions) => {
		// 	this.$mdToast.hide(this.loadingToast);
		// });
	}

	/**
	 * init
	 * @desc Init professions list with notifications
	 * @param {object} professions - professions list
	 */
	init(professions) {
		professions.initializeNotifications(true);
	}

	/**
	 * addProfession
	 * @desc add new Profession
	 * @param {string} profession
	 */
	addProfession(profession) {
		this.professionsService.addProfession(profession);
	}

	/**
	 * addSpecialite
	 * @desc add new Specialite
	 * @param {string} Specialite
	 */
	addSpecialite(specialite) {
		this.specialitesService.addSpecialite(specialite);
	}

	/**
	 * addMarque
	 * @desc add new marque
	 * @param {string} Marque
	 */
	addMarque(marque) {
		this.marquesService.addMarque(marque);
	}

	/**
	 * updateProfession
	 * @desc edit Profession
	 * @param {string} profession
	 */
	valideUpdateProfession(profession, currentUpdateId) {
		this.professionsService.updateProfession(profession, currentUpdateId);
	}
	/**
	 * updateSpecialite
	 * @desc edit Specialite
	 * @param {string} specialite
	 */
	valideUpdateSpecialite(specialite, currentUpdateId) {
		this.specialitesService.updateSpecialite(specialite, currentUpdateId);
	}
	/**
	 * updateMarque
	 * @desc edit Marque
	 * @param {string} marque
	 */
	valideUpdateMarque(marque, currentUpdateId) {
		this.marquesService.updateMarque(marque, currentUpdateId);
	}

	/**
	 * $onDestroy
	 * @desc On destroy component
	 */
	$onDestroy() {
		this.professionsReference.$destroy();
		this.specialitesReference.$destroy();
		this.marquesReference.$destroy();
		// this.$mdToast.hide(this.loadingToast);
	}

	/**
	 * routerTab
	 * @desc Navigate between component tabs
	 * @param {string} route - route name
	 */
	routerTab(route) {
		this.$router.navigate([route]);
	}

	askDeleteProfession(professionId) {
		let vm = this;
		var confirm = this.$mdDialog.confirm()
				.title('Confirmation')
				.textContent('Êtes-vous sûr de vouloir de vouloir supprimer cet élément ?')
				.ariaLabel('selected-warning')
				.ok('Confirmer')
				.cancel('Annuler');

		this.$mdDialog.show(confirm).then(function() {
			vm.professionsService.deleteProfession(professionId);
		});
	}

	askDeleteSpecialite(specialiteId) {
		let vm = this;
		var confirm = this.$mdDialog.confirm()
				.title('Confirmation')
				.textContent('Êtes-vous sûr de vouloir de vouloir supprimer cet élément ?')
				.ariaLabel('selected-warning')
				.ok('Confirmer')
				.cancel('Annuler');

		this.$mdDialog.show(confirm).then(function() {
			vm.specialitesService.deleteSpecialite(specialiteId);
		});
	}

	askDeleteMarque(marqueId) {
		let vm = this;
		var confirm = this.$mdDialog.confirm()
				.title('Confirmation')
				.textContent('Êtes-vous sûr de vouloir de vouloir supprimer cet élément ?')
				.ariaLabel('selected-warning')
				.ok('Confirmer')
				.cancel('Annuler');

		this.$mdDialog.show(confirm).then(function() {
			vm.marquesService.deleteMarque(marqueId);
		});
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
    static get $inject() {
        return [
            'ProfessionsService',
            'SpecialitesService',
            'MarquesService',
            '$mdPanel',
            '$rootRouter',
            '$mdToast',
            '$mdDialog',
            '$timeout',
            '$rootScope'
        ];
    }
}

/**
 * @module loader
 */
angular
	.module('professions', [
		'professions/professions-list',
		'professions/specialites-list',
		'professions/marques-list',
	])
	.component('professions', new ProfessionComponent());
