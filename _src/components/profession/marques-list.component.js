/**
 * MarquesListComponent
 * @desc angular component MarquesList
 */
class MarquesListComponent {

	/**
     * Constructor
     * @desc Create MarquesListComponent
     * @param {object} require - component require
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor () {
		this.require = {
			parent: '^professions'
		};
		this.templateUrl = './components/profession/marques-list.component.html';
		this.controller = MarquesListComponentController;
	}
}

/**
 * MarquesListComponentController
 * @desc angular component MarquesList controller
 */
class MarquesListComponentController {

	/**
	 * constructor
	 * @desc Create MarquesListComponentController
	 */
	constructor () {
		this.newMarque = {
			name: ''
		};
		this.currentUpdateId = {
			onUpdate: false,
			id: null
		};
  }

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.marques = [];
		this.limit = 50;
		this.end = false;
		this.isFilterOpen = false;

		this.parent.marquesReference.$loaded((marques) => {
			this.getMarques(marques);
		});
	}

	/**
	 * getMarques
	 * @desc Get marques list
	 * @param {array} marques - marques list
	 */
	getMarques(marques) {
		this.marques = marques.sortList('name');
		if(this.marques && this.limit >= this.marques.length) {
			this.end = true;
		}
	}

	addMarque($event, marque){
		if($event.keyCode==13 && marque && marque.name.length > 0){
			this.parent.addMarque(marque);
			this.newMarque = {
				name: ''
			};
		}
	}

	valideUpdateMarque($event, marque, currentUpdateId){
		if($event.keyCode==13 && marque && marque.name.length > 0){
			this.parent.valideUpdateMarque(marque, currentUpdateId);
		}
	}
	valideUpdateMarqueBtn(profession, currentUpdateId){
		if(profession && profession.name.length > 0){
			this.parent.valideUpdateMarque(profession, currentUpdateId);
		}
	}

	showToUpdate(marque){
		this.currentUpdateId.onUpdate = true;
		this.currentUpdateId.id = marque.$id;
	}

	/**
	 * getMore
	 * @desc Get more candidates item
	 */
	getMore() {
			this.limit += 50;

			if(this.limit >= this.marques.length) {
					this.end = true;
			}
	}
}

/**
 * @module professions/marques-list
 */
angular
	.module('professions/marques-list', [])
	.component('marquesList', new MarquesListComponent());
