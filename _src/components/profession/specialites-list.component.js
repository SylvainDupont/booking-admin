/**
 * SpecialitiesListComponent
 * @desc angular component SpecialitiesList
 */
class SpecialitiesListComponent {

	/**
     * Constructor
     * @desc Create SpecialitiesListComponent
     * @param {object} require - component require
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor () {
		this.require = {
			parent: '^professions'
		};
		this.templateUrl = './components/profession/specialites-list.component.html';
		this.controller = SpecialitiesListComponentController;
	}
}

/**
 * SpecialitiesListComponentController
 * @desc angular component SpecialitiesList controller
 */
class SpecialitiesListComponentController {

	/**
	 * constructor
	 * @desc Create SpecialitiesListComponentController
	 */
	constructor () {
		this.newSpecialite = {
			name: ''
		};
		this.currentUpdateId = {
			onUpdate: false,
			id: null
		};
  }

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.specialites = [];
		this.limit = 100;
		this.end = false;
		this.isFilterOpen = false;

		this.parent.specialitesReference.$loaded((specialites) => {
			this.getSpecialites(specialites);
		});
	}

	/**
	 * getSpecialites
	 * @desc Get specialites list
	 * @param {array} specialites - specialites list
	 */
	getSpecialites(specialites) {
		this.specialites = specialites.sortList('name');
	}

	addSpecialite($event, specialite){
		if($event.keyCode==13 && specialite && specialite.name.length > 0){
			this.parent.addSpecialite(specialite);
			this.newSpecialite = {
				name: ''
			};
		}
	}

	valideUpdateSpecialite($event, specialite, currentUpdateId){
		if($event.keyCode==13 && specialite && specialite.name.length > 0){
			this.parent.valideUpdateSpecialite(specialite, currentUpdateId);
		}
	}
	valideUpdateSpecialiteBtn(profession, currentUpdateId){
		if(profession && profession.name.length > 0){
			this.parent.valideUpdateSpecialite(profession, currentUpdateId);
		}
	}

	showToUpdate(specialite){
		this.currentUpdateId.onUpdate = true;
		this.currentUpdateId.id = specialite.$id;
	}
}

/**
 * @module professions/specialites-list
 */
angular
	.module('professions/specialites-list', [])
	.component('specialitiesList', new SpecialitiesListComponent());
