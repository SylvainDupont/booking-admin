/**
 * DashboardComponent
 * @desc angular component dashboard
 */
class DashboardComponent {

    /**
     * Constructor
     * @desc Create DashboardComponent
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
	 * @param {array} $routeConfig - component router configuration
     */
	constructor() {
		this.templateUrl = './components/dashboard/dashboard.component.html';
		this.controller = DashboardComponentController;
		this.$routeConfig = [
			{
                path: '/candidats/...',
                name: 'Candidates',
                component: 'candidates',
                useAsDefault: true
            },
            {
                path: '/agences/...',
                name: 'Agencies',
                component: 'agencies'
            },
            {
                path: '/recruteurs/...',
                name: 'Promoter',
                component: 'promoter'
            },
            {
                path: '/professions/...',
                name: 'Professions',
                component: 'professions'
            }
		];
	}
}

/**
 * DashboardComponentController
 * @desc angular component dashboard controller
 */
class DashboardComponentController {

    /**
	 * constructor
	 * @desc Create AgenciesComponentController
	 * @param {object} AuthService - service manage app auth
	 */
	constructor(AuthService) {
		this.authService = AuthService;
	}

    /**
     * $routerOnActivate
     * @desc On router component activate
     * @param {object} next - router params
     */
	$routerOnActivate(next) {
		return this.authService.redirectUnauthenticated();
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array}
     */
    static get $inject() {
        return [
            'AuthService'
        ];
    }

}

/**
 * @module dashboard
 */
angular
	.module('dashboard', [])
	.component('dashboard', new DashboardComponent());
