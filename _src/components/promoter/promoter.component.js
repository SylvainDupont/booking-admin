/**
 * PromoterComponent
 * @desc angular component promoter
 */
class PromoterComponent {

	/**
     * Constructor
     * @desc Create AgenciesTransfertComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
	 * @param {array} $routeConfig - component router configuration
     * @param {function} controller - component controller
     */
	constructor() {
		this.bindings = {
			$router: '<'
		};

		this.templateUrl = './components/promoter/promoter.component.html';
		this.$routeConfig = [
			{
                path: '/carte-bancaire',
                name: 'PromoterCard',
                component: 'promoterCard',
                useAsDefault: true,
                data: {
                	tabIndex: 0
                }
            }
            // {
            //     path: '/devis',
            //     name: 'PromoterQuotation',
            //     component: 'promoterQuotation',
            //     data: {
            //     	tabIndex: 1
            //     }
            // }
		];
		this.controller = PromoterComponentController;
	}
}

/**
 * PromoterComponentController
 * @desc angular component promoter controller
 */
class PromoterComponentController {

	/**
	 * constructor
	 * @desc Create PromoterComponentController
	 * @param {object} $rootRouter - service from angular-component-router
	 * @param {object} $mdToast - service from angular-material
	 * @param {object} $mdDialog - service from angular-material
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} $mdPanel - service from angular-material
	 */
	constructor($rootRouter, $mdToast, $mdDialog, PromoterService, $mdPanel) {
		this.$rootRouter     = $rootRouter;
		this.$mdToast        = $mdToast;
		this.$mdDialog       = $mdDialog;
		this.PromoterService = PromoterService;
		this.$mdPanel        = $mdPanel;

		// this.loadingToast = this.$mdToast.show({
		// 	templateUrl: './components/toast/toast-loading.template.html',
		// 	hideDelay: 0,
		// 	position: 'bottom left'
		// });

		/**
		 * Router tabs configuration
		 * @type {Array}
		 */
		this.routerTabConfig = [
			{
				router: 'PromoterCard',
				label: 'Virement',
				notifications: 0
			}
			// {
			// 	router: 'PromoterQuotation',
			// 	label: 'Devis',
			// 	notifications: 0
			// }
		];
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.promoterReference = [];
		this.selectedTabIndex = 0;

		this.promoterReference = this.PromoterService.get('promoter');
		this.promoterLength = 0;

		this.promoterReference.$loaded((promoters) => {
			this.promoterLength = promoters.length;
			// this.$mdToast.hide(this.loadingToast);
			this.init(promoters);

			this.promoterReference.$watch((e) => {
				this.init(promoters);
				switch(e.event) {
					case 'child_added':
						this.$mdToast
							.simple()
							.textContent('Vous avez un nouvel organisateur');
						break;
				}
			});
		});
	}

	/**
     * $routerOnActivate
     * @desc On router component activate
     * @param {object} next - router params
     */
	$routerOnActivate() {
		// this.$mdToast.hide(this.loadingToast);
	}

	/**
	 * init
	 * @desc Init promoters list with notifications
	 * @param {object} promoters - promoters list
	 */
	init(promoters) {
		promoters.initializeNotifications(true);
		this.routerTabConfig[0].notifications = promoters.globalNotifications.card;
	}

	/**
	 * routerTab
	 * @desc Navigate between component tabs
	 * @param {string} route - route name
	 */
	routerTab(route) {
		this.$router.navigate([route]);
	}

	/**
	 * $onDestroy
	 * @desc On destroy component
	 */
	$onDestroy() {
		this.promoterReference.$destroy();
		// this.$mdToast.hide(this.loadingToast);
	}

	/**
	 * viewPromoterProfile
	 * @desc View promoter profile
	 * @param {object} promoter - promoter informations
	 */
	viewPromoterProfile (promoter) {

		this.promoter = promoter;

		console.log(this.promoter);

		const position = this.$mdPanel
			.newPanelPosition()
      		.absolute()
      		.right()
      		.top();

      	const animation = this.$mdPanel
      		.newPanelAnimation();

      	animation.openFrom({
        	top:0,
        	left:document.documentElement.clientWidth
      	});

      	animation.withAnimation(this.$mdPanel.animation.SLIDE);

      	let panelConfig = {
		    animation: animation,
		    attachTo: angular.element(document.body),
		    controller: () => this,
		    controllerAs: '$ctrl',
		    template: `
		    	<promoter-panel
		    		promoter="$ctrl.promoter"
		    		panel-reference="$ctrl.panelReference">
		    	</promoter-panel>`,
		    panelClass: 'panel',
		    position: position,
		    clickEscapeToClose: true
		};

		this.panelReference = this.$mdPanel.create(panelConfig);
		this.panelReference.open();
	}

	/**
	 * toggleValid
	 * @desc Toggle promoter validation
	 * @param {string} promoterId
	 */
	toggleValid(promoterId) {
    	this.PromoterService.toggleValid(promoterId);
    }

	/**
	 * disableAccount
	 * @desc Disable promoter account
	 * @param {string} promoterId
	 */
	disableAccount(promoterId) {
		this.PromoterService.disableAccount(promoterId);
	}

	/**
	 * togglePaymentType
	 * @desc Toggle promoter payment type
	 * @param {string} promoterId
	 */
	togglePaymentType(promoterId) {
		this.PromoterService.togglePaymentType(promoterId);
	}

	/**
	 * changeRole
	 * @desc Change promoter role
	 * @param {string} promoterId
	 */
	changeRole(promoterId) {
		this.PromoterService.changeRole(promoterId, 'hostess');
	}

	/**
	 * deleteAccount
	 * @desc Delete promoter account
	 * @param {string} promoterId
	 */
	deleteAccount(promoterId) {
		this.PromoterService.deleteAccount(promoterId);
	}

	/**
	 * updateNbPublications
	 * @desc change promoter number publication
	 * @param {string} promoterId
	 */
	updateNbPublications($event,promoter) {
		let dialogConfig = {
			targetEvent: $event,
			controller: DialogController,
			locals: {
           item: promoter.nbrPublication
      },
			clickOutsideToClose: true,
			template: '<md-dialog aria-label="List dialog">' +
           '  		<md-dialog-content>'+
 					 '  			<div layout-padding>'+
 					 '  				<div>'+
	         '    				<md-input-container class="md-block" flex-xs="100" md-no-float> ' +
	         '    					<label>Nombres publications sur 30 jours glissants : </label> ' +
				 	 '   						<input type="number" ng-model="item" min=0 value=0>'+
				 	 ' 					  </md-input-container> '+
				 	 ' 					</div> '+
					 '   			</div> '+
           '  		</md-dialog-content>' +
		       '  		<md-dialog-actions>' +
		       '    	<md-button ng-click="closeDialog()" class="md-primary">Annuler</md-button>' +
		       '    		<md-button ng-click="validDialog()" class="md-primary">Confirmer</md-button>' +
		       '  		</md-dialog-actions>' +
      '</md-dialog>',
		};
		const confirm = this.$mdDialog.prompt(dialogConfig);
		this.$mdDialog.show(confirm);
		let vm = this;
		function DialogController($scope, $mdDialog, item){
			// let vm = this;
			$scope.item = item;
			$scope.closeDialog = function() {
				$mdDialog.hide();
			};
			$scope.validDialog = function() {
				vm.PromoterService.updateNbPublications(promoter.$id, $scope.item);
				$mdDialog.hide();
			};
		}
		DialogController.$inject = ["$scope", "$mdDialog", "item"];
	}
	/**
	 * setBackUpPublication
	 * @desc change promoter back Up publication
	 * @param {string} promoterId
	 */
	updateNbBackUpPublications($event,promoter) {
		let dialogConfig = {
			targetEvent: $event,
			controller: DialogController,
			locals: {
           item: promoter.nbrBackUpPublication
      },
			clickOutsideToClose: true,
			template: '<md-dialog aria-label="List dialog">' +
           '  		<md-dialog-content>'+
 					 '  			<div layout-padding>'+
 					 '  				<div>'+
	         '    				<md-input-container class="md-block" flex-xs="100" md-no-float> ' +
	         '    					<label>Nombre publications back up restantes : </label> ' +
				 	 '   						<input type="number" ng-model="item" min=0 value=0>'+
				 	 ' 					  </md-input-container> '+
				 	 ' 					</div> '+
					 '   			</div> '+
           '  		</md-dialog-content>' +
		       '  		<md-dialog-actions>' +
		       '    	<md-button ng-click="closeDialog()" class="md-primary">Annuler</md-button>' +
		       '    		<md-button ng-click="validDialog()" class="md-primary">Confirmer</md-button>' +
		       '  		</md-dialog-actions>' +
      '</md-dialog>',
		};
		const confirm = this.$mdDialog.prompt(dialogConfig);
		this.$mdDialog.show(confirm);
		let vm = this;
		function DialogController($scope, $mdDialog, item){
			// let vm = this;
			$scope.item = item;
			$scope.closeDialog = function() {
				$mdDialog.hide();
			};
			$scope.validDialog = function() {
				vm.PromoterService.updateNbBackUpPublications(promoter.$id, $scope.item);
				$mdDialog.hide();
			};
		}
		DialogController.$inject = ["$scope", "$mdDialog", "item"];
	}
	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'$rootRouter',
			'$mdToast',
			'$mdDialog',
			'PromoterService',
			'$mdPanel'
		];
	}
}

/**
 * @module promoter
 */
angular
	.module('promoter', [
		'promoter/promoter-card',
		'promoter/promoter-quotation',
		'promoter/promoter-panel-quotation',
		'promoter/promoter-panel',
		'promoter/promoter-panel-offer',
		'promoter/promoter-panel-offer-list',
		'promoter/promoter-panel-candidate',
		'promoter/promoter-panel-candidate-list'
     ])
	.component('promoter', new PromoterComponent());
