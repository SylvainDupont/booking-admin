/**
 * PromoterPanelOfferListComponent
 * @desc angular component promoterPanelOfferList
 */
class PromoterPanelOfferListComponent {

	/**
     * Constructor
     * @desc Create PromoterPanelOfferListComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.bindings = {
			promoter: "=",
			offerReference: '=',
			offers: '=',
			panelListReference: '=',
			panelTitle: '='
		};

		this.templateUrl = './components/promoter/promoter-panel-offer-list.component.html';
		this.controller = PromoterPanelOfferListComponentController;
	}
}

/**
 * PromoterPanelOfferListComponentController
 * @desc angular component promoterPanelOfferList controller
 */
class PromoterPanelOfferListComponentController {

	/**
	 * constructor
	 * @desc Create PromoterPanelOfferListComponentController
	 * @param {object} PromoterService - service manage agencies
	 */
	constructor(PromoterService, ProfessionsService, SpecialitesService, MarquesService) {
		this.PromoterService = PromoterService;
		this.ProfessionsService = ProfessionsService;
		this.SpecialitesService = SpecialitesService;
		this.MarquesService = MarquesService;
		this.professions = this.ProfessionsService.get();
		this.specialites = this.SpecialitesService.get();
		this.marques = this.MarquesService.get();
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.activeSearch = false;
		this.activeFilter = false;
		this.searchDate = '-publishDate';
	}

	/**
	 * toggleSearch
	 * @desc Toggle search input
	 */
	toggleSearch() {
		this.activeSearch = !this.activeSearch;
	}

	/**
	 * resetFilter
	 * @desc Reset filter bar
	 */
	resetFilter() {
		this.searchPerformance = undefined;
		this.searchDepartment = undefined;
	}

	/**
	 * viewOffer
	 * @desc View promoter offer details
	 * @param {object} offer
	 */
	viewOffer(offer) {
		this.PromoterService.viewOffer(offer, this.promoter);
	}

	/**
	 * closePanel
	 * @desc Close panel reference
	 */
	closePanel() {
		this.panelListReference.close();
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'PromoterService',
			'ProfessionsService',
			'SpecialitesService',
			'MarquesService'
		];
	}
}

/**
 * @module promoter/promoter-panel-offer-list
 */
angular
	.module('promoter/promoter-panel-offer-list', [])
	.component('promoterPanelOfferList', new PromoterPanelOfferListComponent());
