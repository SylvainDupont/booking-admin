/**
 * PromoterPanelOfferComponent
 * @desc angular component promoterPanelOffer
 */
class PromoterPanelOfferComponent {

	/**
     * Constructor
     * @desc Create PromoterPanelOfferComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.bindings = {
			promoter: '=',
			offerId: '=',
			panelReference: '='
		};

		this.templateUrl = './components/promoter/promoter-panel-offer.component.html';
		this.controller = PromoterPanelOfferComponentController;
	}
}

/**
 * PromoterPanelOfferComponentController
 * @desc angular component promoterPanelOffer controller
 */
class PromoterPanelOfferComponentController {

	/**
	 * constructor
	 * @desc Create PromoterCardComponentController
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} OfferCheckoutService - service manage offer checkout charges
	 * @param {object} $timeout - service from angular-core
	 * @param {object} $interval - service from angular-core
	 */
	constructor(PromoterService, OfferCheckoutService, $timeout, $interval) {
		this.PromoterService = PromoterService;
		this.OfferCheckoutService = OfferCheckoutService;
		this.$timeout = $timeout;
		this.$interval = $interval;
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.selectedCandidates = [];
		this.total = 0;
        this.tva = 0;
        this.managementFees = 0;
       
		this.getOffer();
		this.getCandidates();
	}

	/**
	 * $onDestroy
	 * @desc On component destroy
	 */
	$onDestroy() {
		if(this.offerReference) this.offerReference.$destroy();
		if(this.candidates) this.candidates.$destroy();
	}

	/**
	 * closePanel
	 * @desc Close panel reference
	 */
	closePanel() {
		this.panelReference.close();
	}

	/**
	 * checkPayment
	 * @desc Check offer payment
	 */
	checkPayment() {
		this.PromoterService.checkPayment(this.promoter.$id, this.offerId);
	}

	/**
	 * getOffer
	 * @desc Get offer details
	 */
	getOffer() {
		this.offerReference = this.PromoterService.getOffer(this.promoter.$id, this.offerId);
		this.offerReference.$loaded((offer) => {
			this.initOffer(offer);

			this.offerReference.$watch(() => {
				this.initOffer(offer);
			});
		});
	}

	/**
	 * getCandidates
	 * @desc Get offer candidates
	 */
	getCandidates() {
		this.candidates = this.PromoterService.getCandidates(this.promoter.$id, this.offerId);
		this.candidates.$loaded((candidates) => {
			this.initCandidates(candidates);

			this.candidates.$watch(() => {
				this.initCandidates(candidates);
			});
		});
	}

	/**
	 * initCandidates
	 * @desc Initialize offer candidates list
	 */
	initCandidates(candidates) {
		for(let candidate of candidates) {
			if(candidate.selected) this.selectedCandidates.push(candidate);
		}
	}

	/**
	 * initOffer
	 * @desc Initialize offer details informations
	 */
	initOffer(offer) {
		if(offer.$value !== null) {
			this.offer = offer;
			this.offer.getSelectedCandidatesLength();
			this.offer.getCheckoutCandidateLength();
			this.offer.getDuration();
			this.offer.getTotal('ht');
			this.getFees();
			this.getBonus();
		}
	}

	/**
	 * getFees
	 * @desc Get offers fees
	 */
    getFees() {
        this.checkoutFees = this.OfferCheckoutService.getCheckoutCharges({
            missionDuration: this.offer.duration, 
            selectedCandidatesLength: this.offer.notifications.selectedCandidateLength,
            startDate: this.offer.dates.startDate, 
            publishDate: this.offer.publishDate, 
            isEmergencyHours: 24
        });

        this.total = this.checkoutFees.total;
        this.tva = this.checkoutFees.tva;
        this.managementFees = this.checkoutFees.managementFees;
    }

	/**
	 * getBonus
	 * @desc Get offer bonus (iccp, ipp)
	 */
    getBonus() {
        this.offer = this.OfferCheckoutService.getBonus(this.offer, {
            ippPercent: 0.1,
            iccpPercent: 0.1
        });
    }

	/**
	 * viewCandidateProfile
	 * @desc View candidate profile details
	 * @param {object} candidate
	 */
    viewCandidateProfile(candidate) {
    	this.PromoterService.viewCandidateProfile(candidate, this.promoter.$id, this.offerId);
    }

	/**
	 * viewCandidateList
	 * @desc View offer candidate list
	 */
    viewCandidateList() {
    	this.PromoterService.viewCandidateList(this.candidates, this.promoter.$id, this.offerId);
    }

	/**
	 * removeOffer
	 * @desc remove promoter offer
	 */
    removeOffer() {
		this.closePanel();
    	this.PromoterService.removeOffer(this.promoter.$id, this.offerId);
    }

	/**
	 * shareOffer
	 * @desc Share offer to facebook
	 */
    shareOffer() {
    	this.PromoterService.shareOffer(this.promoter.$id, this.offer);
    }

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'PromoterService',
			'OfferCheckoutService',
			'$timeout',
			'$interval'
		];
	}
}

/**
 * @module promoter/promoter-panel-offer
 */
angular
	.module('promoter/promoter-panel-offer', [])
	.component('promoterPanelOffer', new PromoterPanelOfferComponent());