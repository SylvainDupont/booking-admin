/**
 * PromoterPanelCandidateListComponent
 * @desc angular component promoterPanelCandidateList
 */
class PromoterPanelCandidateListComponent {

	/**
     * Constructor
     * @desc Create PromoterPanelCandidateListComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.bindings = {
			candidates: '=',
			promoterId: '=',
			offerId: '=',
			panelReference: '='
		};

		this.templateUrl = './components/promoter/promoter-panel-candidate-list.component.html';
		this.controller =PromoterPanelCandidateListComponentController;
	}
}

/**
 * PromoterPanelCandidateListComponentController
 * @desc angular component promoterPanelCandidateList controller
 */
class PromoterPanelCandidateListComponentController {

	/**
	 * constructor
	 * @desc Create PromoterPanelCandidateListComponentController
	 * @param {object} PromoterService - service manage agencies
	 */
	constructor(PromoterService) {
		this.PromoterService = PromoterService;
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.activeSearch = false;
		this.activeFilter = false;
		this.applyDate = '-applyDate';
	}

	/**
	 * toggleSearch
	 * @desc Toggle search input
	 */
	toggleSearch() {
		this.activeSearch = !this.activeSearch;
	}

	/**
	 * resetFilter
	 * @desc Reset filter bar
	 */
	resetFilter() {
		this.searchSexe = undefined;
		this.searchLanguage = undefined;
		this.searchSkill = undefined;
	}

	/**
	 * viewCandidateProfile
	 * @desc View candidate profile details
	 * @param {object} candidate
	 */
	viewCandidateProfile(candidate) {
    	this.PromoterService.viewCandidateProfile(candidate, this.promoterId, this.offerId);
    }

	/**
	 * removeCandidateFromOffer
	 * @desc Remove candidate from promoter offer
	 * @param {object} candidate
	 */
    removeCandidateFromOffer(candidate) {
		this.PromoterService.removeCandidateFromOffer(candidate.$id, this.promoterId, this.offerId);
	}

	/**
	 * closePanel
	 * Close panel reference
	 */
	closePanel() {
		this.panelReference.close();
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'PromoterService'
		];
	}
}

/**
 * @module promoter/promoter-panel-candidate-list
 */
angular
	.module('promoter/promoter-panel-candidate-list', [])
	.component('promoterPanelCandidateList', new PromoterPanelCandidateListComponent());