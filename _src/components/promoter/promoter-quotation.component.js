/**
 * PromoterQuotationComponent
 * @desc angular component promoterQuotation
 */
class PromoterQuotationComponent {

	/**
     * Constructor
     * @desc Create PromoterQuotationComponent
     * @param {object} require - component require
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.require = {
			parent: '^promoter'
		};

		this.templateUrl = './components/promoter/promoter-quotation.component.html';
		this.controller = PromoterQuotationComponentController;
	}
}

/**
 * PromoterQuotationComponentController
 * @desc angular component promoterQuotation controller
 */
class PromoterQuotationComponentController {

	/**
	 * constructor
	 * @desc Create PromoterQuotationComponentController
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} $mdToast - service from angular-material
	 */
	constructor(PromoterService, $mdToast) {
		this.PromoterService = PromoterService;
		this.$mdToast = $mdToast;
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.promoters = [];
		this.limit = 20;
		this.end = false;
		this.quotationLength = 0;
	}

	/**
     * $routerOnActivate
     * @desc On router component activate
     * @param {object} next - router params
     */
	$routerOnActivate(next) {
        this.parent.selectedTabIndex = next.routeData.data.tabIndex;
        this.getQuotation();
    }

	/**
	 * getQuotation
	 * @desc Get quotation list
	 */
	getQuotation() {

		this.quotationLength = this.PromoterService.getQuotationLength();
		this.quotations = this.PromoterService.getQuotation();

		this.quotations.$loaded(() => {
			this.quotations.initializeQuotationNotifications();
			this.init();
			this.quotations.$watch((e) => {
				this.quotations.initializeQuotationNotifications();
				this.init();
				switch(e.event) {
					case 'child_added':
						let notification = this.$mdToast.simple({
							position: 'bottom right'
						});

						notification.textContent('Vous avez un nouveau devis');

			            this.$mdToast.show(notification);
						break;
				}
			});

			console.log(this.quotations);
		});

		this.quotationLength.$loaded(() => {
			if(this.limit >= this.quotationLength.$value) this.end = true;
		});
	}

	/**
	 * Init
	 * @desc Initialize notifications
	 */
	init() {
		this.parent.routerTabConfig[1].notifications = this.quotations.notifications.global;
	}

	/**
	 * getMore
	 * @desc Get more quotation items
	 */
	getMore() {
		this.limit += 50;
		if(this.limit >= this.quotationLength.$value) this.end = true;
	}

	/**
	 * viewQuotation
	 * @desc View quotation details
	 * @param {object} quotation - quotation informations
	 */
	viewQuotation(quotation) {
		this.PromoterService.viewQuotation(quotation);
	}

	/**
	 * checkQuotation
	 * @desc Check quotation is valid
	 * @param {object} quotation
	 */
	checkQuotation(quotation) {
		this.PromoterService.checkQuotation(quotation);
	}

	/**
	 * removeQuotation
	 * @desc Remove quotation item
	 * @param {object} quotation
	 */
	removeQuotation(quotation) {
		this.PromoterService.removeQuotation(quotation);
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'PromoterService',
			'$mdToast'
		];
	}
}

/**
 * @module promoter/promoter-quotation
 */
angular
	.module('promoter/promoter-quotation', [])
	.component('promoterQuotation', new PromoterQuotationComponent());