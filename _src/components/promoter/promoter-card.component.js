/**
 * PromoterCardComponent
 * @desc angular component promoterCard
 */
class PromoterCardComponent {

	/**
     * Constructor
     * @desc Create PromoterCardComponent
     * @param {object} require - component require
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.require = {
			parent: '^promoter'
		};

		this.templateUrl = './components/promoter/promoter-card.component.html';
		this.controller = PromoterCardComponentController;
	}
}

/**
 * PromoterCardComponentController
 * @desc angular component promoterCard controller
 */
class PromoterCardComponentController {

	/**
	 * constructor
	 * @desc Create PromoterCardComponentController
	 */
	constructor() {}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.promoters = [];
		this.limit = 20;
		this.end = false;
		this.isFilterOpen = false;

		this.parent.promoterReference.$loaded((promoters) => {
			this.getPromoter(promoters);

			this.parent.promoterReference.$watch(() => {
				this.getPromoter(promoters);
			});
		});
	}

	/**
	 * getPromoter
	 * @desc Get promoter list
	 * @param {object} promoters
	 */
	getPromoter(promoters) {
		this.promoters = promoters.getByKeys({
	        paymentType: 'CB'
	    });

		this.promoters = promoters;

        if(this.limit >= this.parent.promoterLength) this.end = true;
	}

	/**
	 * getMore
	 * @desc Get more promoter items
	 */
	getMore() {
		this.limit += 50;
		if(this.limit >= this.parent.promoterLength) this.end = true;
	}

	/**
	 * toggleFilter
	 * @desc Toggle filter bar
	 */
	toggleFilter() {
		this.isFilterOpen = !this.isFilterOpen;
	}

	/**
	 * resetFilter
	 * @desc Reset filter bar
	 */
  resetFilter() {
  	this.searchSexe = undefined;
  	this.searchVehicle = undefined;
  	this.searchActive = undefined;
  	this.searchLanguage = undefined;
  	this.searchPerformance = undefined;
		this.searchSpecialite = undefined;
		this.searchMarque = undefined;
  	this.searchState = undefined;
  	this.searchDate = undefined;
  	this.searchDepartment = undefined;
  	this.searchValidate = undefined;
  }
	
}

/**
 * @module promoter/promoter-card
 */
angular
	.module('promoter/promoter-card', [])
	.component('promoterCard', new PromoterCardComponent());
