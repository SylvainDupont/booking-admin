/**
 * PromoterPanelComponent
 * @desc angular component promoterPanel
 */
class PromoterPanelComponent {

	/**
     * Constructor
     * @desc Create PromoterPanelComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.bindings = {
			promoter: '=',
			panelReference: '='
		};

		this.templateUrl = './components/promoter/promoter-panel.component.html';
		this.controller = PromoterPanelComponentController;
	}
}

/**
 * PromoterPanelComponentController
 * @desc angular component promoterPanel controller
 */
class PromoterPanelComponentController {

	/**
	 * constructor
	 * @desc Create PromoterPanelComponentController
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} $timeout - service from angular-core
	 * @param {object} $mdPanel - service from angular-material
	 */
	constructor(PromoterService, $timeout, $mdPanel) {
		this.PromoterService = PromoterService;
		this.$timeout = $timeout;
		this.$mdPanel = $mdPanel;
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.limit = 5;

		this.$timeout(() => {
			this.getOffers();
		}, 1000);
	}

	/**
	 * getOffers
	 * @desc Get offer list
	 */
	getOffers() {
		this.currentOfferReference = this.PromoterService.getCurrentOffer(this.promoter.$id);
		this.historyOfferReference = this.PromoterService.getHistoryOffer(this.promoter.$id);

		this.currentOfferReference.$loaded((offers) => {
			this.initCurrentOffer(offers);
			this.initCheckoutOffer(offers);

			this.currentOfferReference.$watch(() => {
				this.initCurrentOffer(offers);
				this.initCheckoutOffer(offers);
			});
		});

		this.historyOfferReference.$loaded((offers) => {
			this.initHistoryOffer(offers);

			this.historyOfferReference.$watch(() => {
				this.initHistoryOffer(offers);
			});
		});
	}

	/**
	 * initCurrentOffer
	 * @desc Init current offer list
	 * @param {object} offers
	 */
	initCurrentOffer(offers) {
		offers.sortList('publishDate');
		this.currentOffers = offers.getByKeys({
			checkout: false
		});
	}

	/**
	 * initCheckoutOffer
	 * @desc Init checkout offer list
	 * @param {object} offers
	 */
	initCheckoutOffer(offers) {
		offers.sortList('publishDate');
		this.checkoutOffers = offers.getByKeys({
			checkout: true
		});
	}

	/**
	 * initHistoryOffer
	 * @desc Init history offer list
	 * @param {object} offers
	 */
	initHistoryOffer(offers) {
		offers.sortList('publishDate');
		this.historyOffers = offers;
	}

	/**
	 * closePanel
	 * @desc Close panel reference
	 */
	closePanel() {
		this.panelReference.close();
	}

	/**
	 * viewListOffers
	 * @desc View promoter offers list
	 * @param {string} type
	 */
	viewListOffers(type) {

		switch(type) {
			case 'current':
				this.panelTitle = 'Offres en cours';
				this.panelOffers = this.currentOffers;
				this.panelOfferReference = this.currentOfferReference;
				break;
			case 'checkout':
				this.panelTitle = 'Offres payées';
				this.panelOffers = this.checkoutOffers;
				this.panelOfferReference = this.currentOfferReference;
				break;
			case 'history':
				this.panelTitle = 'Historiques';
				this.panelOffers = this.historyOffers;
				this.panelOfferReference = this.historyOfferReference;
				break;
		}

		const position = this.$mdPanel
			.newPanelPosition()
      		.absolute()
      		.right()
      		.top();

      	const animation = this.$mdPanel
      		.newPanelAnimation();

      	animation.openFrom({
        	top: document.documentElement.clientHeight,
        	left: 0
      	});

      	animation.withAnimation(this.$mdPanel.animation.SLIDE);

      	let panelConfig = {
		    animation: animation,
		    attachTo: angular.element(document.body),
		    controller: () => this,
		    controllerAs: '$ctrl',
		    template: `
		    	<promoter-panel-offer-list
		    		promoter="$ctrl.promoter"
		    		offers="$ctrl.panelOffers" 
		    		offer-reference="$ctrl.panelOfferReference"
		    		panel-list-reference="$ctrl.panelListReference" 
		    		panel-title="$ctrl.panelTitle" >
				</promoter-panel-offer-list>`,
		    panelClass: 'panel',
		    position: position,
		    clickEscapeToClose: true
		};

		this.panelListReference = this.$mdPanel.create(panelConfig);
		this.panelListReference.open();
	}

	/**
	 * viewOffer
	 * @desc View promoter offer details
	 * @param {object} offer
	 */
	viewOffer(offer) {
		this.PromoterService.viewOffer(offer, this.promoter);
	}

	/**
	 * disableAccount
	 * @desc Disable promoter account
	 */
	disableAccount() {
		this.PromoterService.disableAccount(this.promoter.$id);
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'PromoterService',
			'$timeout',
			'$mdPanel'
		];
	}
}

/**
 * @module promoter/promoter-panel
 */
angular
	.module('promoter/promoter-panel', [])
	.component('promoterPanel', new PromoterPanelComponent());