/**
 * PromoterPanelCandidateComponent
 * @desc angular component PromoterPanelCandidateComponent
 */
class PromoterPanelCandidateComponent {

	/**
     * Constructor
     * @desc Create PromoterPanelCandidateComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.bindings = {
			offerSelected: '=',
			offerId: '=',
			promoterId: '=',
			candidateId: '=',
			panelReference: '='
		};

		this.templateUrl = './components/promoter/promoter-panel-candidate.component.html';
		this.controller = PromoterPanelCandidateComponentController;
	}
}

/**
 * PromoterPanelCandidateComponentController
 * @desc angular component promoterPanelCandidate controller
 */
class PromoterPanelCandidateComponentController {

	/**
	 * constructor
	 * @desc Create PromoterPanelOfferListComponentController
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} $rootRouter - service from angular-component-router
	 * @param {object} $location - service from angular-core
	 */
	constructor(PromoterService, $rootRouter, $location) {
		this.PromoterService = PromoterService;
		this.$rootRouter = $rootRouter;
		this.$location = $location;
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.getCandidate();
	}
	viewCV(fileName, uid){
		console.log(fileName);
		const storage = firebase.storage();
		const userPicture = storage.ref('userPictures').child(uid);
		const finalName = (fileName.indexOf('.pdf') > 0) ? 'curriculum.pdf' : ((fileName.indexOf('.docx') ? 'curriculum.docx' : 'curriculum.doc'));
		userPicture.child(finalName).getDownloadURL().then((url) => {
			let a = document.createElement('a');
			a.target = "_blank";
			a.href = url;
			a.click();
		});
	}
	/**
	 * getCandidate
	 * @desc Get candidate informations
	 */
	getCandidate() {
		this.candidateReference = this.PromoterService.getCandidate(this.candidateId, this.promoterId, this.offerId);
		this.candidateReference.$loaded((candidate) => {
			this.initCandidate(candidate);

			this.candidateReference.$watch(() => {
				this.initCandidate(candidate);
			});
		});
	}

	/**
	 * initCandidate
	 * @desc Initialize candidate informations
	 */
	initCandidate(candidate) {
		this.candidate = candidate;
		this.candidate.getSelectedOffer();
		this.candidate.getApplyOffer();
		this.candidate.offerLength = Object.keys(this.candidate.missions).length;
	}

	/**
	 * viewCandidatePicture
	 * @desc View candidate gallery
	 * @param {object} $event - targetEvent
	 * @param {int} selectedPictureIndex
	 * @param {object} candidate
	 */
	viewCandidatePicture ($event, selectedPictureIndex, candidate) {
		this.PromoterService.viewCandidatePicture($event, selectedPictureIndex, this.candidate);
	}

	/**
	 * removeCandidateFromOffer
	 * @desc Remove candidate from promoter offer
	 */
	removeCandidateFromOffer() {
		this.PromoterService.removeCandidateFromOffer(this.candidate.$id, this.promoterId, this.offerId);
	}

	/**
	 * printCandidateProfile
	 * @desc Save candidate profile to PDF
	 */
	printCandidateProfile() {

		let url = this.$rootRouter.generate(['PrintCandidateProfile', {candidateId: this.candidate.$id}]);
	
		let protocol = this.$location.protocol();
		let host = this.$location.host();
		let port = this.$location.port();
		let toUrl = url.toLinkUrl();

		var win = window.open(`${protocol}:/\\/${host}:${port}/${toUrl}`, '_blank');
  		win.focus();
	}

	/**
	 * closePanel
	 * @desc Close panel reference
	 */
	closePanel() {
		this.panelReference.close();
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'PromoterService',
			'$rootRouter',
			'$location'
		];
	}
}

/**
 * @module promoter/promoter-panel-candidate
 */
angular
	.module('promoter/promoter-panel-candidate', [])
	.component('promoterPanelCandidate', new PromoterPanelCandidateComponent());