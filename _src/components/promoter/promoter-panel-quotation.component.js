/**
 * PromoterPanelQuotationComponent
 * @desc angular component promoterPanelQuotation
 */
class PromoterPanelQuotationComponent {

	/**
     * Constructor
     * @desc Create PromoterPanelQuotationComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.bindings = {
			quotation: '=',
			panelReference: '='
		};

		this.templateUrl = './components/promoter/promoter-panel-quotation.component.html';
		this.controller = PromoterPanelQuotationComponentController;
	}
}

/**
 * PromoterPanelQuotationComponentController
 * @desc angular component promoterPanelQuotation controller
 */
class PromoterPanelQuotationComponentController {

	/**
	 * constructor
	 * @desc Create PromoterPanelComponentController
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} $rootRouter - service from angular-component-router
	 */
	constructor(PromoterService, $rootRouter) {
		this.PromoterService = PromoterService;
		this.$rootRouter = $rootRouter;
	}

	/**
	 * closePanel
	 * @desc Close panel reference
	 */
	closePanel() {
		this.panelReference.close();
	}

	/**
	 * checkQuotation
	 * @desc Check if quotation is valid
	 */
	checkQuotation() {
		this.PromoterService.checkQuotation(this.quotation);
	}

	/**
	 * removeQuotation
	 * @desc Remove quotation item
	 */
	removeQuotation() {
		this.PromoterService.removeQuotation(this.quotation);
	}

	/**
	 * exportPDF
	 * @desc Export quotation to PDF
	 */
	exportPDF() {
		let instruction = this.$rootRouter.generate(['PrintQuotation', {quotationId: this.quotation.$id}]);
		let host = location.host;
		let route = instruction.toLinkUrl();
		let url = `http://${host}/${route}`;
		let win = window.open(url, '_blank');
		/*win.data = {
			anonymous: this.anonymous
		};*/
		
		win.focus();
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
	static get $inject() {
		return [
			'PromoterService',
			'$rootRouter'
		];
	}
}

/**
 * @module promoter/promoter-panel-quotation
 */
angular
	.module('promoter/promoter-panel-quotation', [])
	.component('promoterPanelQuotation', new PromoterPanelQuotationComponent());