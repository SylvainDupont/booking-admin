/**
 * SidenavComponent
 * @desc angular component sidenav
 */
class SidenavComponent {

	/**
     * Constructor
     * @desc Create SidenavComponent
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.templateUrl = './components/sidenav/sidenav.component.html';
        this.controller = SidenavComponentController;
	}
}

/**
 * SidenavComponentController
 * @desc angular component sidenav controller
 */
class SidenavComponentController {

	/**
	 * constructor
	 * @desc Create SidenavComponentController
	 */
	constructor () {
	}
}

/**
 * @module sidenav
 */
angular
	.module('sidenav', [])
	.component('sidenav', new SidenavComponent());
