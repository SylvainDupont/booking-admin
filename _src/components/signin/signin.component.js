/**
 * SignInComponent
 * @desc angular component signin
 */
class SignInComponent {

	/**
     * Constructor
     * @desc Create SignInComponent
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor() {
		this.templateUrl = './components/signin/signin.component.html';
		this.controller = SignInComponentController;
	}
}

/**
 * SignInComponentController
 * @desc angular component signin controller
 */
class SignInComponentController {

	/**
	 * constructor
	 * @desc Create AgenciesComponentController
	 * @param {object} AuthService - service manage app auth
	 */
	constructor(AuthService) {
		this.authService = AuthService;
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.inputType = 'password';
	}

	/**
	 * showPassword
	 * @desc Toggle password/text input type
	 */
	showPassword() {
		if(this.inputType != 'password') {
			this.inputType = 'password';
		}else {
			this.inputType = 'text';
		}
	}

	/**
	 * signIn
	 * @desc Signin user to app
	 */
	signIn() {
		this.authService.signIn({
			mail: this.signInform.mail,
			password: this.signInform.password
		});
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
    static get $inject() {
        return [
            'AuthService'
        ];
    }
}

/**
 * @module signin
 */
angular
	.module('signin', [])
	.component('signin', new SignInComponent());