/**
 * LoaderComponent
 * @desc angular component loader
 */
class LoaderComponent {

	/**
     * Constructor
     * @desc Create LoaderComponent
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor () {
		this.templateUrl = './components/loader/loader.component.html';
		this.controller = LoaderComponentController;
	}
}

/**
 * LoaderComponentController
 * @desc angular component loader controller
 */
class LoaderComponentController {}

/**
 * @module loader
 */
angular
	.module('loader', [])
	.component('loader', new LoaderComponent());