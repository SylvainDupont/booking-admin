/**
 * AgenciesComponent
 * @desc angular component agencies
 */
class AgenciesComponent {

	/**
     * Constructor
     * @desc Create AgenciesTransfertComponent
     * @param {object} bindings - component bindings
     * @param {string} templateUrl - component template
	 * @param {array} $routeConfig - component router configuration
     * @param {function} controller - component controller
     */
	constructor () {
		this.bindings = {
			$router: '<'
		};
		this.templateUrl = './components/agencies/agencies.component.html';
		this.$routeConfig = [
			// {
      //           path: '/carte-bancaire',
      //           name: 'AgenciesCard',
      //           component: 'agenciesCard',
      //           useAsDefault: true,
      //           data: {
      //           	tabIndex: 0
      //           }
      //       },
            {
                path: '/virement',
                name: 'AgenciesTransfert',
                component: 'agenciesTransfert',
								useAsDefault: true,
                data: {
                	tabIndex: 0
                }
            }
		];
		this.controller = AgenciesComponentController;
	}
}

/**
 * AgenciesComponentController
 * @desc angular component agencies controller
 */
class AgenciesComponentController {

	/**
	 * constructor
	 * @desc Create AgenciesComponentController
	 * @param {object} AgenciesService - service manage agencies
	 * @param {object} PromoterService - service manage agencies
	 * @param {object} $mdPanel - service from angular-material
	 * @param {object} $rootRouter - service from angular-component-router
	 * @param {object} $mdToast - service from angular-material
	 * @param {object} $mdDialog - service from angular-material
	 * @param {object} $timeout - service from angular-core
	 * @param {object} $rootScope - service from angular-core
	 */
	constructor (AgenciesService, PromoterService, $mdPanel, $rootRouter, $mdToast, $mdDialog, $timeout, $rootScope, ToastService) {
		this.agenciesService = AgenciesService;
		this.PromoterService = PromoterService;
		this.$mdPanel = $mdPanel;
		this.$rootRouter = $rootRouter;
		this.$mdToast = $mdToast;
		this.$mdDialog = $mdDialog;
		this.$timeout = $timeout;
		this.$rootScope = $rootScope;
		this.ToastService = ToastService;

		this.agenciesReference = this.agenciesService.get();
		this.agenciesLength = 0;
		/**
		 * Router tabs configuration
		 * @type {Array}
		 */
		this.routerTabConfig = [
			// {
			// 	router: 'AgenciesCard',
			// 	label: 'Carte bancaire',
			// 	notifications: 0
			//
			// },
			{
				router: 'AgenciesTransfert',
				label: 'Virement',
				notifications: 0
			}
		];
	}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.agenciesReference = [];
		this.selectedTabIndex = 0;

		// this.loadingToast = this.$mdToast.show({
		// 	templateUrl: './components/toast/toast-loading.template.html',
		// 	hideDelay: 0,
		// 	position: 'bottom left'
		// });

		this.agenciesReference = this.PromoterService.get('hostess');
		this.agenciesLength = 0;

		this.agenciesReference.$loaded((agencies) => {
			this.agenciesLength = agencies.length;
			// this.$mdToast.hide(this.loadingToast);
			this.init(agencies);

			this.agenciesReference.$watch((e) => {
				this.init(agencies);
				switch(e.event) {
					case 'child_added':
						this.$mdToast
							.simple()
							.textContent('Vous avez une nouvelle agence');
						break;
				}
			});
		});
	}

	/**
	 * init
	 * @desc Init agencies list with notifications
	 * @param {object} agencies - agencies list
	 */
	init(agencies) {
		agencies.initializeNotifications(true);
		// this.routerTabConfig[0].notifications = agencies.globalNotifications.card;
		this.routerTabConfig[0].notifications = agencies.globalNotifications.transfert;
	}

	/**
	 * $onDestroy
	 * @desc On destroy component
	 */
	$onDestroy() {
		this.agenciesReference.$destroy();
		// this.$mdToast.hide(this.loadingToast);
	}

	/**
	 * routerTab
	 * @desc Navigate between component tabs
	 * @param {string} route - route name
	 */
	routerTab(route) {
		this.$router.navigate([route]);
	}

	/**
	 * viewPromoterProfile
	 * @desc View agency profile
	 * @param {object} promoter - agency informations
	 */
	viewPromoterProfile(promoter) {

		this.promoter = promoter;


		const position = this.$mdPanel
			.newPanelPosition()
      		.absolute()
      		.right()
      		.top();

      	const animation = this.$mdPanel
      		.newPanelAnimation();

      	animation.openFrom({
        	top:0,
        	left:document.documentElement.clientWidth
      	});

      	animation.withAnimation(this.$mdPanel.animation.SLIDE);

      	let panelConfig = {
		    animation: animation,
		    attachTo: angular.element(document.body),
		    controller: () => this,
		    controllerAs: '$ctrl',
		    template: `
		    	<promoter-panel
		    		promoter="$ctrl.promoter"
		    		panel-reference="$ctrl.panelReference">
		    	</promoter-panel>`,
		    panelClass: 'panel',
		    position: position,
		    clickEscapeToClose: true
		};

		this.panelReference = this.$mdPanel.create(panelConfig);
		this.panelReference.open();
	}

	/**
	 * disableAccount
	 * @desc Disable agency account
	 * @param {string} promoterId
	 */
	disableAccount(promoterId) {
		this.PromoterService.disableAccount(promoterId);
	}

	/**
	 * togglePaymentType
	 * @desc Toggle agency payment type
	 * @param {string} promoterId
	 */
	togglePaymentType(promoterId) {
		this.PromoterService.togglePaymentType(promoterId);
	}

	/**
	 * updateNbPublications
	 * @desc change agency number publication
	 * @param {string} promoterId
	 */
	updateNbPublications($event,agency) {
		let dialogConfig = {
			targetEvent: $event,
			controller: DialogController,
			locals: {
           item: agency.nbrPublication
      },
			clickOutsideToClose: true,
			template: '<md-dialog aria-label="List dialog">' +
           '  		<md-dialog-content>'+
 					 '  			<div layout-padding>'+
 					 '  				<div>'+
	         '    				<md-input-container class="md-block" flex-xs="100" md-no-float> ' +
	         '    					<label>Nombres publications sur 30 jours glissants : </label> ' +
				 	 '   						<input type="number" ng-model="item" min=0 value=0>'+
				 	 ' 					  </md-input-container> '+
				 	 ' 					</div> '+
					 '   			</div> '+
           '  		</md-dialog-content>' +
		       '  		<md-dialog-actions>' +
		       '    	<md-button ng-click="closeDialog()" class="md-primary">Annuler</md-button>' +
		       '    		<md-button ng-click="validDialog()" class="md-primary">Confirmer</md-button>' +
		       '  		</md-dialog-actions>' +
      '</md-dialog>',
		};
		const confirm = this.$mdDialog.prompt(dialogConfig);
		this.$mdDialog.show(confirm);
		let vm = this;
		function DialogController($scope, $mdDialog, item){
			// let vm = this;
			$scope.item = item;
			$scope.closeDialog = function() {
				$mdDialog.hide();
			};
			$scope.validDialog = function() {
				vm.agenciesService.updateNbPublications(agency.$id, $scope.item);
				$mdDialog.hide();
			};
		}
		DialogController.$inject = ["$scope", "$mdDialog", "item"];
	}
	/**
	 * setBackUpPublication
	 * @desc change agency back Up publication
	 * @param {string} promoterId
	 */
	updateNbBackUpPublications($event,agency) {
		let dialogConfig = {
			targetEvent: $event,
			controller: DialogController,
			locals: {
           item: agency.nbrBackUpPublication
      },
			clickOutsideToClose: true,
			template: '<md-dialog aria-label="List dialog">' +
           '  		<md-dialog-content>'+
 					 '  			<div layout-padding>'+
 					 '  				<div>'+
	         '    				<md-input-container class="md-block" flex-xs="100" md-no-float> ' +
	         '    					<label>Nombre publications back up restantes : </label> ' +
				 	 '   						<input type="number" ng-model="item" min=0 value=0>'+
				 	 ' 					  </md-input-container> '+
				 	 ' 					</div> '+
					 '   			</div> '+
           '  		</md-dialog-content>' +
		       '  		<md-dialog-actions>' +
		       '    	<md-button ng-click="closeDialog()" class="md-primary">Annuler</md-button>' +
		       '    		<md-button ng-click="validDialog()" class="md-primary">Confirmer</md-button>' +
		       '  		</md-dialog-actions>' +
      '</md-dialog>',
		};
		const confirm = this.$mdDialog.prompt(dialogConfig);
		this.$mdDialog.show(confirm);
		let vm = this;
		function DialogController($scope, $mdDialog, item){
			// let vm = this;
			$scope.item = item;
			$scope.closeDialog = function() {
				$mdDialog.hide();
			};
			$scope.validDialog = function() {
				vm.agenciesService.updateNbBackUpPublications(agency.$id, $scope.item);
				$mdDialog.hide();
			};
		}
		DialogController.$inject = ["$scope", "$mdDialog", "item"];
	}

	/**
	 * toggleValid
	 * @desc Toggle agency validation
	 * @param {string} agencyId
	 */
	toggleValid (agencyId) {
    	this.PromoterService.toggleValid(agencyId);
    }

	/**
	 * changeRole
	 * @desc Change agency role
	 * @param {string} agencyId
	 */
	changeRole(agencyId) {
		this.PromoterService.changeRole(agencyId, 'promoter');
	}

	/**
	 * deleteAccount
	 * @desc Delete agency account
	 * @param {string} promoterId
	 */
	deleteAccount(promoterId) {
		this.PromoterService.deleteAccount(promoterId);
	}

	/**
	 * $inject
     * @desc Inject dependencies
     * @return {array}
     */
    static get $inject() {
        return [
            'AgenciesService',
            'PromoterService',
            '$mdPanel',
            '$rootRouter',
            '$mdToast',
            '$mdDialog',
            '$timeout',
            '$rootScope'
        ];
    }

}

/**
 * @module agencies
 */
angular
	.module('agencies', [
		// 'agencies/agencies-card',
        'agencies/agencies-transfert'
        //'agencies/agencies-profile',
        //'agencies/agencies-profile-current',
        //'agencies/agencies-profile-checkout',
        //'agencies/agencies-profile-history',
        //'agencies/agencies-profile-offer',
     ])
	.component('agencies', new AgenciesComponent());
