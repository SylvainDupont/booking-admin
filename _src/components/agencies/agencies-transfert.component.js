/**
 * AgenciesTransfertComponent
 * @desc angular component agenciesTransfert
 */
class AgenciesTransfertComponent {

	/**
     * Constructor
     * @desc Create AgenciesTransfertComponent
     * @param {object} require - component require
     * @param {string} templateUrl - component template
     * @param {function} controller - component controller
     */
	constructor () {
		this.require = {
			parent: '^agencies'
		};

		this.templateUrl = './components/agencies/agencies-transfert.component.html';
		this.controller = AgenciesTransfertComponentController;
	}
}

/**
 * AgenciesTransfertComponent
 * @desc angular component agenciesTransfert controller
 */
class AgenciesTransfertComponentController {

	/**
	 * constructor
	 * @desc Create AgenciesTransfertComponentController
	 */
	constructor () {}

	/**
	 * $onInit
	 * @desc On component init
	 */
	$onInit() {
		this.agencies = [];
		this.limit = 20;
		this.end = false;
		this.isFilterOpen = false;

		this.parent.agenciesReference.$loaded((agencies) => {
			this.getAgencies(agencies);

			this.parent.agenciesReference.$watch(() => {
				this.getAgencies(agencies);
			});
		});
	}

	/**
	 * getAgencies
	 * @desc Get agencies list
	 * @param {array} agencies - agencies list
	 */
	getAgencies(agencies) {
		// this.agencies = agencies.getByKeys({
	  //       paymentType: 'TRANSFERT'
	  //   });
		this.agencies = agencies;

        if(this.limit >= this.parent.agenciesLength) this.end = true;
	}

	/**
	 * getMore
	 * @desc Get more agencies items
	 */
	getMore () {
		this.limit += 50;

		if(this.limit >= this.agencies.length) {
    	 	this.end = true;
    	}
	}

	/**
	 * toggleFilter
	 * @desc Toggle filter bar
	 */
	toggleFilter () {
		this.isFilterOpen = !this.isFilterOpen;
	}

	/**
	 * resetFilter
	 * @desc Reset filter bar
	 */
    resetFilter () {
    	this.searchSexe = undefined;
    	this.searchVehicle = undefined;
    	this.searchActive = undefined;
    	this.searchLanguage = undefined;
    	this.searchPerformance = undefined;
    	this.searchState = undefined;
    	this.searchDate = undefined;
    	this.searchDepartment = undefined;
    	this.searchValidate = undefined;
  }
}

/**
 * @module agencies/agencies-transfert
 */
angular
	.module('agencies/agencies-transfert', [])
	.component('agenciesTransfert', new AgenciesTransfertComponent());
