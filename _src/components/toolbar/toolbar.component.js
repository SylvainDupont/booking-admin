/**
 * ToolbarComponent
 * @desc angular component toolbar
 */
class ToolbarComponent {

    /**
     * Constructor
     * @desc Create ToolbarComponent
     * @param {object} require - component require
     * @param {string} templateUrl - component template
     * @param {boolean} transclude - component html transclude
     * @param {function} controller - component controller
     */
    constructor() {
        this.require = {
            parent: '^dashboard'
        };
        this.templateUrl = './components/toolbar/toolbar.component.html';
        this.transclude = true;
        this.controller = ToolbarComponentController;
    }
}

/**
 * ToolbarComponentController
 * @desc angular component toolbar controller
 */
class ToolbarComponentController {

    /**
     * Constructor
     * @desc Create ToolbarComponentController
     * @param {object} AuthService - service manage app auth
     * @param {object} $rootScope - service from angular-core
     */
    constructor(AuthService, $rootScope) {
        this.authService = AuthService;
        this.$rootScope = $rootScope;
    }

    /**
     * signout
     * @desc Signout firebase user
     */
    signOut() {
        this.authService.signOut();
    }

    /**
     * $inject
     * @desc Inject dependencies
     * @return {array}
     */
    static get $inject() {
        return [
            'AuthService',
            '$rootScope' 
        ];
    }
}

/**
 * @module toolbar
 */
angular
	.module('toolbar', [])
	.component('toolbar', new ToolbarComponent());