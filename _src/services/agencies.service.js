/**
 * AgenciesService
 * @desc angular service for manage agencies
 */
class AgenciesService {

	/**
	 * constructor
	 * @desc Create AgenciesService
	 * @param {object} $q - service from angular-core
	 * @param {object} $rootScope - service from angular-core
	 * @param {object} Auth - factory return $auth service from angularfire
	 * @param {object} $firebaseObject - service from angularfire
	 * @param {object} firebaseArrayCustom - service extends $firebaseArray from angularfire
	 * @param {object} firebaseObjectCustom - service extends $firebaseObject from angularfire
	 * @param {object} MailService - service manage email
	 * @param {object} ToastService - service manage $mdToast from angular-material
	 * @param {object} UtilsService - service with multiple tools
	 * @param {object} $mdDialog - service from angular-material
	 */
	constructor($q, $rootScope, Auth, $firebaseObject, firebaseArrayCustom, firebaseObjectCustom, MailService, ToastService, UtilsService, $mdDialog) {
		this.$q                   = $q;
		this.$rootScope           = $rootScope;
		this.Auth                 = Auth;
		this.$firebaseObject      = $firebaseObject;
		this.firebaseArrayCustom  = firebaseArrayCustom;
		this.firebaseObjectCustom = firebaseObjectCustom;
		this.MailService          = MailService;
		this.ToastService         = ToastService;
		this.UtilsService         = UtilsService;
		this.$mdDialog            = $mdDialog;
	}

	/**
	 * get
	 * @desc Get all agencies
	 * @return {array}
	 */
	get() {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.orderByChild('role')
				.equalTo('hostess')
		);
	}

	/**
	 * getCard
	 * @desc Get all agencies with card payment type
	 * @return {array}
	 */
	getCard() {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.orderByChild('paymentType')
				.equalTo('CB')
		);
	}

	/**
	 * getTransfert
	 * @desc Get all agencies with transfert payment type
	 * @return {array}
	 */
	getTransfert() {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.orderByChild('paymentType')
				.equalTo('TRANSFERT')
		);
	}

	/**
	 * getById
	 * @desc Get agency by id
	 * @param {string} agencyId - agency uid
	 * @return {object}
	 */
	getById(agencyId) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
		);
	}

	/**
	 * getCurrentOffers
	 * @desc Get current offers
	 * @param {string} agencyId - agency uid
	 * @return {array}
	 */
	getCurrentOffers(agencyId) {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
				.child('missions')
				.orderByChild('dates/endDate')
				.startAt(moment().subtract(1, 'day').format())
		);
	}

	/**
	 * getHistoryOffers
	 * @desc Get history offers
	 * @param {string} agencyId - agency uid
	 * @return {array}
	 */
	getHistoryOffers(agencyId) {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
				.child('missions')
				.orderByChild('dates/endDate')
				.endAt(moment().add(1, 'day').format())
		);
	}

	/**
	 * getOfferById
	 * @desc Get offer by id
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 * @return {array}
	 */
	getOfferById(agencyId, offerId) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
				.child('missions')
				.child(offerId)
		);
	}

	/**
	 * removeOffer
	 * @desc Remove agency offer
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 */
	removeOffer(agencyId, offerId) {

		//Configure dialog
		const confirm = this.$mdDialog.confirm()
	        .title('Suppression de l\'offre')
	        .textContent('Voulez-vous vraiment supprimer cette offre ?')
	        .ariaLabel('Remove offer')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
    	this.$mdDialog.show(confirm).then(() => {

    		//Get offer reference
			let offerReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('agency')
					.child(agencyId)
					.child('missions')
					.child(offerId)
			);

			//Remove offer
			offerReference.$remove().then(() => {

				//Get candidates reference
				var candidatesReference = this.firebaseArrayCustom(
					firebase
						.database()
						.ref('candidate')
						.orderByChild('active')
						.equalTo(true)
				);

				candidatesReference.$loaded((candidates) => {

					//Loop over the candidates reference
					for(var i = 0, length = candidates.length; i < length; i++) {

						//If candidate have a offers list
						if(candidates[i].missions) {

							//If candidate offers list have this offer
							if(_.contains(Object.keys(candidates[i].missions), offerId)){

								//Delete offer
								delete candidatesReference[i].missions[offerId];

								//Save candidates reference
								candidatesReference.$save(i);
							}
						}
					}
				});
			});
    	});
	}

	/**
	 * removeOfferCandidate
	 * @desc Remove candidate from agency offer
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 * @param {string} candidateId - candidate uid
	 */
	removeOfferCandidate(agencyId, offerId, candidateId) {

		//Configure dialog
		const confirm = this.$mdDialog.confirm({
				skipHide: true
			})
	        .title('Suppression d\'un candidat')
	        .textContent('Voulez-vous vraiment supprimer ce candidat de l\'offre ?')
	        .ariaLabel('Remove candidate from offer')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
    	this.$mdDialog.show(confirm).then(() => {

    		//Get offer reference
			var offerReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('agency')
					.child(agencyId)
					.child('missions')
					.child(offerId)
			);

			offerReference.$loaded(() => {

				//Delete offer
				delete offerReference.candidates[candidateId];

				//Save offer reference
				offerReference.$save().then(() => {

					//Get candidate reference
					var candidateReference = this.firebaseObjectCustom(
						firebase
							.database()
							.ref('candidate')
							.child(candidateId)
					);

					candidateReference.$loaded(() => {

						//Delete candidate offer
						delete candidateReference.missions[offerId];

						//Save candidate reference
						candidateReference.$save().then(() => {

							//Success notification
							this.ToastService.show({
								content: 'Le candidat "' + candidateReference.firstname + ' ' + candidateReference.lastname + '" a bien été supprimé de l\'offre "' + offerId + '"',
								type: 'simple',
								position: 'bottom right'
							});
						});
					});
				});
			});
    	});
	}

	/**
	 * getCandidates
	 * @desc Get offer candidates
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 * @return {array}
	 */
	getCandidates(agencyId, offerId) {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
				.child('missions')
				.child(offerId)
				.child('candidates')
		);
	}

	/**
	 * getSelectedCandidates
	 * @desc Get offer selected candidates
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 */
	getSelectedCandidates(agencyId, offerId) {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
				.child('missions')
				.child(offerId)
				.child('candidates')
				.orderByChild('selected')
				.equalTo(true)
		);
	}

	/**
	 * toggleValid
	 * @desc Toggle agency validation
	 * @param {string} agencyId - agency uid
	 */
	toggleValid(agencyId) {

		//Get agency reference
		const agencyReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
		);

		agencyReference.$loaded((agency) => {

			//Toggle validation
			agency.validate = !agency.validate;

			//Save agency reference
			agencyReference.$save().then(() => {

				//Send validate mail to agency
				this.MailService.send({
					mail: agency.mail,
					firstname: agency.name,
					code: 'valid-agency'
				}).then((res) => {
					let content = '';

					//Set good notification text
					if(agency.validate) {
						content = 'L\'agence ' + agency.name + ' est désormais valide';
					}else {
						content = 'L\'agence ' + agency.name + ' est désormais invalide';
					}

					//Success notification
					this.ToastService.show({
						content: content,
						type: 'simple',
						position: 'bottom right'
					});
				});
			});
		});
	}


	/**
	 * updateNbPublications
	 * @desc Update Nbr Publication agency
	 * @param {string} agencyId - agency uid
	 * @param {integer} nbrPublication - publication number
	 */
	updateNbPublications(agencyId, nbrPublication) {

		//Get agency reference
		const agencyReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
		);

		agencyReference.$loaded((agency) => {
			//Toggle validation
			agency.nbrPublication = nbrPublication;

			//Save agency reference
			agencyReference.$save().then(() => {
				//Success notification
				this.ToastService.show({
					content: 'Modification effectuée avec succès',
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}

	/**
	 * updateNbBackUpPublications
	 * @desc Update Nbr Publication BackUp agency
	 * @param {string} agencyId - agency uid
	 * @param {integer} nbrPublication - publication number
	 */
	updateNbBackUpPublications(agencyId, nbrPublication) {

		//Get agency reference
		const agencyReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
		);

		agencyReference.$loaded((agency) => {
			//Toggle validation
			agency.nbrBackUpPublication = nbrPublication;

			//Save agency reference
			agencyReference.$save().then(() => {
				//Success notification
				this.ToastService.show({
					content: 'Modification effectuée avec succès',
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}

	/**
	 * togglePaymentType
	 * @desc Toggle agency payment method
	 * @param {string} agencyId - agency uid
	 */
	togglePaymentType(agencyId) {

		//Get agency reference
		const agencyReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
		);

		agencyReference.$loaded((agency) => {
			let content = '';

			//Set new payment method
			switch(agency.paymentType) {
				case 'CB':
					agency.paymentType = 'TRANSFERT';
					content = 'Le type de paiement de l\'agence "' + agency.name + '" est désormais par prélèvement';
					break;
				case 'TRANSFERT':
					agency.paymentType = 'CB';
					content = 'Le type de paiement de l\'agence "' + agency.name + '" est désormais par carte bancaire';
					break;
			}

			//Save agency reference
			agencyReference.$save().then(() => {

				//Success notification
				this.ToastService.show({
					content: content,
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}


	/**
	 * viewNewOffer
	 * @desc Set mission admin new property to false
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 */
	viewNewOffer(agencyId, offerId) {

		//Get offer reference
		var offerReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(agencyId)
				.child('missions')
				.child(offerId)
		);

		offerReference.$loaded((offer) => {
			offer.admin.newCurrent = false;

			//Save offer reference
			offerReference.$save();
		});
	}

	/**
	 * validatePaymentOffer
	 * @desc Validate offer payment
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 */
	validatePaymentOffer(agencyId, offerId) {

		//Configure dialog
		const confirm = this.$mdDialog.confirm({
				skipHide: true
			})
	        .title('Traitement du paiement')
	        .textContent('Etes-vous sûr de valider l\'offre ?')
	        .ariaLabel('check-payment')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
	    this.$mdDialog.show(confirm).then(() => {

	    	//Get offer reference
			const offerReference = this.$firebaseObject(
				firebase
					.database()
					.ref('agency')
					.child(agencyId)
					.child('missions')
					.child(offerId)
			);

			offerReference.$loaded((offer) => {

				offer.admin.newCheckout = false;
				offer.admin.paymentCheck = true;

				//Save offer reference
				offerReference.$save().then(() => {

					//Success notification
					this.ToastService.show({
						content: 'Le paiement de l\'offre "' + offer.title + '" a bien été validé',
						type: 'simple',
						position: 'bottom right'
					});
				});
			});
		});
	}

	/**
	 * shareOffer
	 * @desc Share offer on facebook
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 */
	shareOffer(agencyId, offer) {

		let shareDescription;

		//Set good date
		if(offer.dates.type !== 'uniq') {
			shareDescription = `Du ${moment(offer.dates.startDate).format('DD/MM/YYYY')} au ${moment(offer.dates.endDate).format('DD/MM/YYYY')} à ${offer.address}`;
		}else {
			shareDescription = `le ${moment(offer.dates.startDate).format('DD/MM/YYYY')} à ${offer.address}`;
		}

		//Initialize facebook API post
		FB.ui({
		  	method: 'share',
		  	href: 'https://app.candidat.booking-hotesses.com/job-board/' + agencyId + '/' + offer.$id,
		  	title: `${offer.title}`,
		  	caption: 'Pour plus de détails et pour postuler, cliquez ici | Booking Hôtesses',
		  	description: shareDescription,
		  	picture: 'https://app.admin.booking-hotesses.com/img/booking-hotesses-1200x650-fb-final.jpg'
		}, (response) => {

			//Debug response
			console.log(response);
		});
	}

	/**
	 * disableAccount
	 * @desc Disable agency account
	 * @param {string} offerId - offer uid
	 */
	disableAccount(agencyId) {

		//Configure dialog
		const confirm = this.$mdDialog.confirm()
	        .title('Désactivation d\'un compte agence')
	        .textContent('Etes-vous sur de vouloir desactiver ce compte ?')
	        .ariaLabel('disable-account-agency')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
	    this.$mdDialog.show(confirm).then(() => {

			//Get agency reference
			const agencyReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('agency')
					.child(agencyId)
			);

			agencyReference.$loaded((agency) => {
				agency.disabled = true;

				//Save agency reference
				agencyReference.$save().then(() => {

					//Success notification
					this.ToastService.show({
						content: content = `Le compte de ${agency.name} est désormais désactivé`,
						type: 'simple',
						position: 'bottom right'
					});
				});
			});
		});
	}

	/**
	 * enableAccount
	 * @desc Enable agency
	 * @param {string} agencyId - agency uid
	 */
	enableAccount(agencyId) {

		//Configure dialog
		const confirm = $mdDialog.confirm()
	        .title('Activation d\'un compte agence')
	        .textContent('Etes-vous sur de vouloir activer ce compte ?')
	        .ariaLabel('disable-account-agency')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
	    this.$mdDialog.show(confirm).then(() => {

			//Get agency reference
			const agencyReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('agency')
					.child(agencyId)
			);

			agencyReference.$loaded((agency) => {
				agency.disabled = false;

				//Save agency reference
				agencyReference.$save().then(() => {

					//Success notification
					this.ToastService.show({
						content: `Le compte de ${agency.name} est désormais activé`,
						type: 'simple',
						position: 'bottom right'
					});
				});
			});
		});
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array} dependencies
     */
	static get $inject() {
		return [
			'$q',
			'$rootScope',
			'Auth',
			'$firebaseObject',
			'firebaseArrayCustom',
			'firebaseObjectCustom',
			'MailService',
			'ToastService',
			'UtilsService',
			'$mdDialog'
		];
	}
}

/**
 * @module agencies-service
 */
angular
	.module('agencies-service', [])
    .service('AgenciesService', AgenciesService);
