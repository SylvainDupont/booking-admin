/**
 * firebaseObjectCustom
 * @desc This is extension for $firebaseObject service from angularfire
 */
const firebaseObjectCustom = ($firebaseObject) => {
    
    //Initialize extension object
    let extension = {};

    /**
     * getSelectedCandidates
     * @desc Get offer selected candidates
     * @return {array}
     */
    extension.getSelectedCandidates = function() {

        var selectedCandidates = [];

        if(this.candidates) {
            for(var candidate in this.candidates) {
                if(this.candidates[candidate].selected) {
                    selectedCandidates.push(candidate);
                }
            }
        }

        return selectedCandidates;
    };

    /**
     * getNewCandidates
     * @desc Get offer new candidates length
     */
    extension.getNewCandidates = function() {
        var newCandidateLength = 0;

        if(this.candidates) {
            for(var candidate in this.candidates) {
                if(this.candidates[candidate].new) {
                    newCandidateLength++;
                }
            }
        }

        this.newCandidateLength = newCandidateLength;
    };

    /**
     * getCandidates
     * @desc Get offer candidates
     * @return {array}
     */
    extension.getCandidates = function() {

        var candidates = [];

        if(this.candidates) {
            for(var candidate in this.candidates) {
                candidates.push(this.candidates[candidate]);
            }
        }

        return candidates;
    };

    /**
     * getCandidatesLength
     * @desc Get offer candidates length
     */
    extension.getCandidatesLength = function() {
        var candidatesLength = 0;

        for(var candidate in this.candidates) {
            candidatesLength++;
        }

        this.candidatesLength = candidatesLength;
    };

    /**
     * getDuration
     * @desc Get offer duration in days
     */
    extension.getDuration = function() {
        if(this.dates) {
            this.duration = moment(this.dates.endDate).diff(this.dates.startDate, 'days') + 1;
        }
    };

    /**
     * getSelectedOffer
     * @desc Get selected offer length
     */
    extension.getSelectedOffer = function() {
        let selectedOfferLength = 0;

        if(this.missions) {
            for(let mission in this.missions) {
                if(this.missions[mission].selected) selectedOfferLength++;
            }
        }

        this.selectedOfferLength = selectedOfferLength;
    };

    /**
     * getApplyOffer
     * @desc Get offer apply length
     */
    extension.getApplyOffer = function() {
        let applyOfferLength = 0;

        if(this.missions) {
            for(let mission in this.missions) {
                if(this.missions[mission].apply) applyOfferLength++;
            }
        }

        this.applyOfferLength = applyOfferLength;
    };

    /**
     * getTotal
     * @desc Get total amount offer
     */
    extension.getTotal = function() {
        let total = 0;

        if(this.payments) {
            for(let payment of this.payments) {
                total += payment.total;
            }
        }

        this.total = total;
    };


    /**
     * initializeNotifications
     * @desc Notifications system
     * @type {Object}
     */
    extension.initializeNotifications = init;

    /**
     * init
     * @desc Initialize notifications
     */
    function init() {
        var notifications = {
            newOffers: 0,
            newOffersCheckout: 0,
        };

        getNewCandidates(notifications);
        getNewOffers(notifications);
        getNewOffersCheckout(notifications);
        getGlobal(notifications);

        this.$list[i].notifications = notifications;
    }

    /**
     * getNewOffers
     * @desc Get new offers length
     * @param {object} notifications - notifications informations
     */
    function getNewOffers(notifications) {
        if(this.missions) { 
            for(var key in this.missions) {
                if(this.missions[key].admin && this.missions[key].admin.newCurrent) {
                    notifications.newOffers++;
                }
            }
        }
    }

    /**
     * getNewOffersCheckout
     * @desc Get new checkout offers length
     * @param {object} notifications - notifications informations
     */
    function getNewOffersCheckout(notifications) {
        if(this.missions) { 
            for(var key in this.missions) {
                if(this.missions[key].admin && this.missions[key].admin.newCheckout) {
                    notifications.newOffersCheckout++;
                }
            }
        }
    }

    /**
     * getGlobal
     * @desc Get global length of notifications
     * @param {object} notifications - notifications informations
     */
    function getGlobal(notifications) {
        for(var notification in notifications) {
            if(notification !== 'global') {
                notifications.global += notifications[notification];
            }
        }
    }

    /**
     * getSelectedCandidatesLength
     * @desc Get offer selected candidates length
     */
    extension.getSelectedCandidatesLength = function () {
        let selectedCandidateLength = 0;

        if(this.candidates) {
            for(let candidate in this.candidates) {
                if(this.candidates[candidate].selected) {
                    selectedCandidateLength++;
                }
            }
        }

        if(!this.notifications) this.notifications = {};
        this.notifications.selectedCandidateLength = selectedCandidateLength;
        
    };

    /**
     * getCheckoutCandidateLength
     * @desc Get offer checkout candidates length
     */
    extension.getCheckoutCandidateLength = function() {
        let checkoutCandidateLength = 0;

        if(this.candidates) {
            for(let candidate in this.candidates) {
                if(this.candidates[candidate].checkout) {
                    checkoutCandidateLength++;
                }
            }
        }

        if(!this.notifications) this.notifications = {};
        this.notifications.checkoutCandidateLength = checkoutCandidateLength;
    };

    return $firebaseObject.$extend(extension);
};

/**
 * $inject
 * @desc Inject dependencies
 * @return {array} dependencies
 */
firebaseObjectCustom.$inject = [
    '$firebaseObject'
];

angular
    .module('app')
    .factory('firebaseObjectCustom', firebaseObjectCustom);