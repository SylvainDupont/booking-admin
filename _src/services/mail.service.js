/**
 * MailService
 * @desc angular service for manage email
 */
class MailService {

	/**
	 * constructor
	 * @desc Create MailService
	 * @param {object} $http - service from angular-core
	 * @param {object} $q - service from angular-core
	 */
	constructor($http, $q) {
		this.$http = $http;
		this.$q = $q;
	}

	/**
	 * send
	 * @desc send email with code
	 * @param {object} params
	 * @return {object}
	 */
	send(params) {

		//Create promise
		const defer = this.$q.defer();

		//Create http request
		this.$http({
			method: 'POST',
			url: '/api/components/sendMail.php',
			data: {
				mail: params.mail,
				firstname: params.firstname,
				code: params.code
			},
			headers : { 
				'Content-Type': 'application/x-www-form-urlencoded' 
			}
		})
		.success((res) => {

			//Resolve promise
			defer.resolve(res);
		})
		.error((error) => {

			//Reject promise
			defer.reject(error);
		});

		return defer.promise;
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array} dependencies
     */
	static get $inject() {
		return [
			'$http',
			'$q'
		];
	}
}

angular
	.module('app')
	.service('MailService', MailService);