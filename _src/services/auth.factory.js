/**
 * Auth
 * Factory return $firebaseAuth service
 * @param {Object} $firebaseAuth - service from angularfire 
 * @return {Object} $firebaseAuth
 */
const Auth = ($firebaseAuth) => {
	return $firebaseAuth();
};

/**
 * $inject
 * @desc Inject dependencies
 * @return {array} dependencies
 */
Auth.$inject = [
	'$firebaseAuth'
];

angular
	.module('app')
	.factory('Auth', Auth);