/**
 * OfferCheckoutService
 * @desc service manage offer checkout
 */
class OfferCheckoutService {

    /**
     * constructor
     * @desc Create OfferCheckoutService
     */

    /*@ngInject*/
    constructor() {}

    /**
     * getManagementFees
     * @desc Calculate management fees
     * @param {object} params - params for management fees
     */
    getManagementFees(params) {

        //Init
        let config = {
            total: 0,
            duration: params.duration,
            selectedCandidates: params.selectedCandidates,
            isEmergencyHours: params.isEmergencyHours,
            isEmergency: () => {
                let start = moment(params.publishDate);
                let end = moment(params.startDate);
                let duration = moment.duration(end.diff(start));
                let h = duration.asHours();
                
                return h <= params.isEmergencyHours;
            }
        };

        console.log('Start');
        console.log('duration:' + config.duration);
        console.log('isEmergency:' + config.isEmergency());
        console.log('selectedCandidates:' + config.selectedCandidates);
        console.log('isEmergencyHours:' + config.isEmergencyHours);
        console.log('total:' + config.total);

        /**
         * Mission is emergency
         */
        if(!config.isEmergency()) {

            /**
             * 1-10 candidates
             */
            if(config.selectedCandidates >= 1 && config.selectedCandidates <= 10) {

                /**
                 * 1-3 days
                 */
                if(config.duration >= 1 && config.duration <= 3) {

                    config.total = 6 * config.selectedCandidates;

                /**
                 * 4-7 days
                 */
                }else if(config.duration >= 4 && config.duration <= 7) {

                    config.total = 5.50 * config.selectedCandidates;

                /**
                 * 8-14 days
                 */
                }else if(config.duration >= 8 && config.duration <= 14) {

                    config.total = 5 * config.selectedCandidates;

                /**
                 * 15-30 days
                 */
                }else if(config.duration >= 15 && config.duration <= 30) {

                    config.total = 4.50 * config.selectedCandidates;

                }
            /**
             * 11-30 candidates
             */
            }else if(config.selectedCandidates >= 11 && config.selectedCandidates <= 30) {

                /**
                 * 1-3 days
                 */
                if(config.duration >= 1 && config.duration <= 3) {

                    config.total = 5.50 * config.selectedCandidates;

                /**
                 * 4-7 days
                 */
                }else if(config.duration >= 4 && config.duration <= 7) {

                    config.total = 5 * config.selectedCandidates;

                /**
                 * 8-14 days
                 */
                }else if(duration >= 8 && duration <= 14) {

                    config.total = 4.50 * config.selectedCandidates;

                /**
                 * 15-30 days
                 */
                }else if(duration >= 15 && duration <= 30) {

                    config.total = 4 * config.selectedCandidates;

                }
            /**
             * 31-50 candidates
             */
            }else if(config.selectedCandidates >= 31 && config.selectedCandidates <= 50) {
                /**
                 * 1-3 days
                 */
                if(config.duration >= 1 && config.duration <= 3) {

                    config.total = 5 * config.selectedCandidates;

                /**
                 * 4-7 days
                 */
                }else if(config.duration >= 4 && config.duration <= 7) {

                    config.total = 4.50 * config.selectedCandidates;

                /**
                 * 8-14 days
                 */
                }else if(duration >= 8 && duration <= 14) {

                    config.total = 4 * config.selectedCandidates;

                /**
                 * 15-30 days
                 */
                }else if(duration >= 15 && duration <= 30) {

                    config.total = 3.50 * config.selectedCandidates;

                }
            /**
             * 31-50 candidates
             */
            }else if(config.selectedCandidates >= 51 && config.selectedCandidates <= 100) {
                /**
                 * 1-3 days
                 */
                if(config.duration >= 1 && config.duration <= 3) {

                    config.total = 4.50 * config.selectedCandidates;

                /**
                 * 4-7 days
                 */
                }else if(config.duration >= 4 && config.duration <= 7) {

                    config.total = 4 * config.selectedCandidates;

                /**
                 * 8-14 days
                 */
                }else if(duration >= 8 && duration <= 14) {

                    config.total = 3.50 * config.selectedCandidates;

                /**
                 * 15-30 days
                 */
                }else if(duration >= 15 && duration <= 30) {

                    config.total = 3 * config.selectedCandidates;

                }
            /**
             * +100 candidates
             */
            }else if(config.selectedCandidates > 100) {
                /**
                 * 1-3 days
                 */
                if(config.duration >= 1 && config.duration <= 3) {

                    config.total = 4 * config.selectedCandidates;

                /**
                 * 4-7 days
                 */
                }else if(config.duration >= 4 && config.duration <= 7) {

                    config.total = 3.50 * config.selectedCandidates;

                /**
                 * 8-14 days
                 */
                }else if(duration >= 8 && duration <= 14) {

                    config.total = 3 * config.selectedCandidates;

                /**
                 * 15-30 days
                 */
                }else if(duration >= 15 && duration <= 30) {

                    config.total = 2.50 * config.selectedCandidates;

                }
            }
        }else {
            config.total = 15 * config.selectedCandidates;
        }

        // console.log('End');
        // console.log('duration:' + config.duration);
        // console.log('isEmergency:' + config.isEmergency());
        // console.log('selectedCandidates:' + config.selectedCandidates);
        // console.log('isEmergencyHours:' + config.isEmergencyHours);
        // console.log('total:' + config.total);

        return config.total;
    }

    /**
     * getTVA
     * @desc Get offer tva
     * @param {object} params
     */
    getTVA(params) {
        return params.total / 100 * params.percent;
    }

    /**
     * getTotal
     * @desc Get offer total
     * @param {object} params
     */
    getTotal(params) {
        return params.managementFees + params.tva;
    }

    /**
     * getCheckoutCharges
     * @desc Get offer checkout charges
     * @param {object} params
     * @return {object}
     */
    getCheckoutCharges(params) {

        let managementFees = this.getManagementFees({
            duration: params.missionDuration, 
            selectedCandidates: params.selectedCandidatesLength,
            startDate: params.startDate, 
            publishDate: params.publishDate, 
            isEmergencyHours: params.isEmergencyHours
        });

        let tva = this.getTVA({
            total: managementFees, 
            percent: 20
        });

        let total = this.getTotal({
            managementFees: managementFees,
            tva: tva
        });

        return {
            managementFees: managementFees,
            tva: tva,
            total: total
        };
    }

    /**
     * getBonus
     * @desc Get offer bonus (ipp, iccp)
     * @param {object} mission - mission informations
     * @param {object} params
     */
    getBonus(mission, params) {
        let config = {
            ipp: 0,
            iccp: 0
        };

        if(mission.baseSalary && mission.workTime) {

            mission.totalSalary = mission.baseSalary * mission.workTime;
            config.ipp = mission.totalSalary * params.ippPercent;
            config.iccp = mission.totalSalary * params.iccpPercent;

            if(mission.bonus) {
                if(mission.bonus.ipp) mission.totalSalary += config.ipp;
                if(mission.bonus.iccp) mission.totalSalary += config.iccp;
            }
        }

        return mission;
    }
}

/**
 * @module offer-checkout-service
 */
angular
    .module('offer-checkout-service', [])
    .service('OfferCheckoutService', OfferCheckoutService);