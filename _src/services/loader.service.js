/**
 * LoaderService
 * @desc angular service for app loading screen
 */
class LoaderService {

	/**
	 * constructor
	 * @desc Create LoaderService
	 * @param {Object} $rootScope - service from angular-core
	 * @param {Object} $timeout - service from angular-core
	 */
	constructor($rootScope, $timeout) {
		this.$rootScope = $rootScope;
		this.$timeout = $timeout;
	}

	/**
	 * start
	 * @desc start loading screen
	 */
	start() {
		this.$rootScope.isLoad = true;
	}

	/**
	 * stop
	 * @desc stop loading screen
	 * @param {int} delay - stop delay
	 * @return {object}
	 */
	stop(delay, cb) {
		delay = delay || 1000;

		this.$timeout(() => {
			this.$rootScope.isLoad = false;

			if(cb) cb();
		}, delay);
	}

	/**
	 * $inject
	 * @desc Inject dependencies
	 * @return {array}
	 */
	static get $inject() {
		return [
			'$rootScope',
			'$timeout'
		];
	}
}

angular
	.module('app')
	.service('LoaderService', LoaderService);