/**
 * UtilsService
 * @desc angular service for manage toolbox
 */
class UtilsService {

	/**
	 * constructor
     * @desc Create UtilsService
     */
	constructor() {}

	/**
     * generateRegisterDate
     * @desc Generate register date in JSON format
     * @param {date} date
     * @return {date}
     */
	generateRegisterDate(date) {
		return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds())).toJSON();
	}

	/**
     * getCurrentDate
     * Get current date
     * @return {date}
     */
	getCurrentDate() {
		return new Date().toJSON();
	}
}

angular
	.module('app')
	.service('UtilsService', UtilsService);

