/**
 * CandidatesService
 * @desc angular service for manage candidates
 */
class CandidatesService {

	/**
	 * constructor
	 * @desc Create CandidatesService
	 * @param {object} $q - service from angular-core
	 * @param {object} $rootScope - service from angular-core
	 * @param {object} Auth - factory return $auth service from angularfire
	 * @param {object} firebaseArrayCustom - service extends $firebaseArray from angularfire
	 * @param {object} firebaseObjectCustom - service extends $firebaseObject from angularfire
	 * @param {object} MailService - service manage email
	 * @param {object} ToastService - service manage $mdToast from angular-material
	 * @param {object} $mdDialog - service from angular-material
	 */
	constructor ($q, $rootScope, Auth, firebaseArrayCustom, firebaseObjectCustom, MailService, ToastService, $mdDialog) {
		this.$q = $q;
		this.$rootScope = $rootScope;
		this.Auth = Auth;
		this.firebaseArrayCustom = firebaseArrayCustom;
		this.firebaseObjectCustom = firebaseObjectCustom;
		this.MailService = MailService;
		this.ToastService = ToastService;
		this.$mdDialog = $mdDialog;
	}

	/**
	 * get
     * @desc Get all candidates
     * @return {array}
     */
    get() {
        return this.firebaseArrayCustom(
            firebase
                .database()
                .ref('candidate')
                .orderByKey()
        );
    }

    /**
	 * getById
     * @desc Get candidate by id
     * @param  {string} candidateId - candidate uid
     * @return {object}
     */
	getById(candidateId) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('candidate')
				.child(candidateId)
		);
	}

	/**
	 * toggleActive
	 * @desc Toggle candidate active property
	 * @param {string} candidateId - candidate uid
	 */
	toggleActive(candidateId) {
		//get candidate reference
		const candidateReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('candidate')
				.child(candidateId)
		);

		candidateReference.$loaded((candidate) => {

			//Toggle property
			candidate.active = !candidate.active;
			//Save candidate reference
			candidateReference.$save().then(() => {
				this.syncCandidateIndexAsync(candidateId, (candidate.active) ? 'add' : 'remove');
				//Send email active to candidate
				if(candidate.active) {
					this.MailService.send({
						mail: candidate.mail,
						firstname: candidate.firstname,
						code: 'candidate-active'
					}).then((res) => {
						if(res.status) {

							//Success notification
							this.ToastService.show({
								content: `Le compte de ${candidate.firstname} ${candidate.lastname} devient actif`,
								type: 'simple',
								position: 'bottom right'
							});
						}
					});
				} else {

					//Success notification
					this.ToastService.show({
						content: `Le compte de ${candidate.firstname} ${candidate.lastname} devient inactif`,
						type: 'simple',
						position: 'bottom right'
					});
				}
			});
		});
	}

	saveAdress(c) {
		//get candidate reference
		const candidateReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('candidate')
				.child(c.$id)
		);

		candidateReference.$loaded((candidate) => {

			//Toggle property
			candidate.fulladdress = c.fulladdress;
			candidate.address_lat = c.address_lat;
			candidate.address_lng = c.address_lng;
			//Save candidate reference
			candidateReference.$save().then(() => {
				//Success notification
				this.ToastService.show({
					content: `L'adresse de ${candidate.firstname} ${candidate.lastname} a bien été mis à jour`,
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}

	/**
	 * setInvalidAccount
	 * @desc Change candidate account to invalid
	 * @param {string} candidateId - candidate uid
	 * @param {string} code - email code
	 */
	setInvalidAccount(candidateId, code) {

		//Configure dialog
		const confirm = this.$mdDialog.confirm()
	        .title('Compte candidat invalide')
	        .textContent('Etes-vous sur que ce candidat est invalide ?')
	        .ariaLabel('invalid-account-candidate')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
	    this.$mdDialog.show(confirm).then(() => {

			//Get candidate reference
			const candidateReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('candidate')
					.child(candidateId)
			);

			candidateReference.$loaded((candidate) => {

				let isValid = true;

				if(candidate.validateType) {

					//Check email code
					switch(code) {
						case 'invalid-picture':
							candidate.validateType.picture = false;
							break;
						case 'invalid-experience':
							candidate.validateType.experience = false;
							break;
					}

					//If all properties is not valid
					for(let validate in candidate.validateType) {
						if(!candidate.validateType[validate]) isValid = false;
					}
				}else {
					isValid = false;
				}


				candidate.validate = isValid;
				if(!candidate.validate) {
					candidate.indexGroup = {};
				}else {
					candidate.indexGroup = this.buildIndexGroup(candidate.mobility, candidate.sexe);
				}
				//Save candidate reference
				candidateReference.$save().then(() => {
					this.syncCandidateIndexAsync(candidateId, 'remove');

					//Send invalid email
					this.MailService.send({
						mail: candidate.mail,
						firstname: candidate.firstname,
						code: code
					}).then((res) => {
						if(res.status && !isValid) {

							//Success notification
							this.ToastService.show({
								content: `Le compte de ${candidate.firstname} ${candidate.lastname} est désormais invalide, email envoyé`,
								type: 'simple',
								position: 'bottom right'
							});
						}
					});
				});
			});
		});
	}

	/**
	 * setValidAccount
	 * @desc Change candidate account to valid
	 * @param {string} candidateId - candidate uid
	 * @param {string} code - email code
	 */
	setValidAccount(candidateId, code) {

		//Configure dialog
		const confirm = this.$mdDialog.confirm()
	        .title('Compte candidat valide')
	        .textContent('Etes-vous sur que ce candidat est valide ?')
	        .ariaLabel('valid-account-candidate')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
	    this.$mdDialog.show(confirm).then(() => {

			//Get candidate reference
			const candidateReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('candidate')
					.child(candidateId)
			);

			candidateReference.$loaded((candidate) => {

				let isValid = true;

				if(candidate.validateType) {

					//Check email code
					switch(code) {
						case 'invalid-picture':
							candidate.validateType.picture = true;
							break;
						case 'invalid-experience':
							candidate.validateType.experience = true;
							break;
					}

					//If all properties is not valid
					for(let validate in candidate.validateType) {
						if(!candidate.validateType[validate]) isValid = false;
					}
				}

				candidate.validate = isValid;
				if(!candidate.validate) {
					candidate.indexGroup = {};
				}else {
					candidate.indexGroup = this.buildIndexGroup(candidate.mobility, candidate.sexe);
				}
				//Save candidate reference
				candidateReference.$save().then(() => {
					this.syncCandidateIndexAsync(candidateId, 'add');
					if(isValid) {

						//Success notification
						this.ToastService.show({
							content: `Le compte de ${candidate.firstname} ${candidate.lastname} est désormais valide`,
							type: 'simple',
							position: 'bottom right'
						});
					}
				});
			});
		});
	}

	/**
	 * disableAccount
	 * @desc Disable candidate account
	 * @param {string} candidateId - candidate uid
	 */
	disableAccount(candidateId) {

		//Configure dialog
		const confirm = this.$mdDialog.confirm()
	        .title('Désactivation d\'un compte candidat')
	        .textContent('Etes-vous sur de vouloir desactiver ce compte ?')
	        .ariaLabel('disable-account-candidate')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
	    this.$mdDialog.show(confirm).then(() => {

			//Get candidate reference
			const candidateReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('candidate')
					.child(candidateId)
			);

			candidateReference.$loaded((candidate) => {
				candidate.disabled = true;
				candidate.indexGroup = {};

				//Save candidate reference
				candidateReference.$save().then(() => {
					this.syncCandidateIndexAsync(candidateId, 'remove');
					//Success notification
					this.ToastService.show({
						content: content = `Le compte de ${candidate.firstname} ${candidate.lastname} est désormais désactivé`,
						type: 'simple',
						position: 'bottom right'
					});
				});
			});
		});
	}

	/**
	 * enableAccount
	 * @desc Enable candidate account
	 * @param {string} candidateId - candidate uid
	 */
	enableAccount(candidateId) {

		//Configure dialog
		const confirm = this.$mdDialog.confirm()
	        .title('Activation d\'un compte candidat')
	        .textContent('Etes-vous sur de vouloir activer ce compte ?')
	        .ariaLabel('disable-account-candidate')
	        .ok('Oui')
	        .cancel('Non');

	    //Open dialog
	    this.$mdDialog.show(confirm).then(() => {

			//Get candidate reference
			const candidateReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('candidate')
					.child(candidateId)
			);

			candidateReference.$loaded((candidate) => {
				candidate.disabled = false;
				candidate.indexGroup = this.buildIndexGroup(candidateReference.mobility, candidateReference.sexe);
				//Save candidate reference
				candidateReference.$save().then(() => {
					this.syncCandidateIndexAsync(candidateId, 'add');
					//Success notification
					this.ToastService.show({
						content: `Le compte de ${candidate.firstname} ${candidate.lastname} est désormais activé`,
						type: 'simple',
						position: 'bottom right'
					});
				});
			});
		});
	}

	buildIndexGroup(mobilityArray, gender, filterConfig)  {
        let groups = {};

        if(!gender || !mobilityArray || mobilityArray.length === 0) {
            return groups;
        }

        for (let i = 0; i < mobilityArray.length; i += 1) {
            let dpt = mobilityArray[i];

            if (!filterConfig.dptTranslation[dpt]) {
                console.log("WARNING - Translation not found for : " + dpt);
                continue;
            }

            groups["dpt" + filterConfig.dptTranslation[dpt]  + "-" + gender] = true;

        }

        return groups;

    }

    removeCandidateFromSpecificIndexAsync(candidate) {

        //Create promise
    	const promise = new Promise((resolve, reject) => {

	        const candidateReference = this.firebaseObjectCustom(firebase
	            .database()
	            .ref('candidateIndex')
	            .child(candidate.$id));

	        candidateReference.$loaded().then((data) => {
                if(data) {
                    candidateReference.$remove().then(() => {
                        resolve();
                    });
                } else {
                    resolve();
                }
            });
        });

    	return promise;
    }

    addCandidateToSpecificIndexAsync(candidateReference) {
        //Create promise
    	const promise = new Promise((resolve, reject) => {
		    const indexReference = this.firebaseObjectCustom(firebase
            .database()
            .ref('candidateIndex'));
        indexReference.$loaded(() => {

            indexReference[candidateReference.$id] = {
							languages: candidateReference.languages,
							freelance: candidateReference.freelance,
							searchPerformance: candidateReference.searchPerformance,
							contratType: candidateReference.contratType,
							specialites: candidateReference.specialites,
							dispoDerniereMinute: candidateReference.dispoDerniereMinute,
							driver: candidateReference.driver,
							vehicle: candidateReference.vehicle,
							mail: candidateReference.mail,
							marques: candidateReference.marques,
							firstname: candidateReference.firstname,
							lat: candidateReference.address_lat,
							lng: candidateReference.address_lng
            };

            indexReference.$save().then(() => {
                resolve();
            });

        });
    	});
    	return promise;
    }

    syncCandidateIndexAsync(candidateId, action) {
        //Create promise
    	const promise = new Promise((resolve, reject) => {

            /* Reference for syncing is the candidates indexGroup child */
            const candidateReference = this.firebaseObjectCustom(
                firebase
                    .database()
                    .ref('candidate')
                    .child(candidateId)
            );

            candidateReference.$loaded(() => {
                let promises = [];

                if(candidateReference && action == 'add') {
                    promises.push(this.addCandidateToSpecificIndexAsync(candidateReference));
                } else {
                    promises.push(this.removeCandidateFromSpecificIndexAsync(candidateReference));
                }


                Promise.all(promises).then(() => { resolve(); });

            });
        });

    	return promise;

    }

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array} dependencies
     */
	static get $inject() {
		return [
			'$q',
			'$rootScope',
			'Auth',
			'firebaseArrayCustom',
			'firebaseObjectCustom',
			'MailService',
			'ToastService',
			'$mdDialog'
		];
	}
}

/**
 * @module candidates-service
 */
angular
    .module('candidates-service', [])
    .service('CandidatesService', CandidatesService);
