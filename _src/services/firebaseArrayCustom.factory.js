/**
 * firebaseArrayCustom
 * @desc This is a extension for $firebaseArray service from angularfire
 */
const firebaseArrayCustom = ($firebaseArray) => {

    //Initialize extension object
    const extension = {};

    /**
     * getByKey
     * @desc Get items by key
     * @param  {String} key
     * @param  {Any} value
     * @return {Array} output
     */
    extension.getByKey = function(key, value) {

        var temp = [];

        for(var i = 0, length = this.$list.length; i < length; i++) {
            if(this.$list[i][key] === value) temp.push(this.$list[i]);
        }

        return temp;
    };

    /**
     * getByKeys
     * @desc Get items by multiple keys
     * @param  {String} keys
     * @return {Array} $list
     */
    extension.getByKeys = function(keys) {
        return _.where(this.$list, keys);
    };

    /**
     * rejectByKeys
     * @desc Reject items by multiple keys
     * @param  {String} keys
     * @return {Array} $list
     */
    extension.rejectByKeys = function (keys) {
        return _.reject(this.$list, keys);
    };

    /**
     * filter
     * @desc filter list with get or reject key/value
     * @param {object} params - params for filter array
     * @param {object} params.get - get object with key/value
     * @param {object} params.reject - reject object with key/value
     * @return {array} $list
     */
    extension.filter = function (params) {
        let list = this.$list;
        let filtered = [];

        if(params.get) {
            filtered = _.where(list, params.get);
        }

        if(params.reject) {
            filtered = _.reject(list, params.reject);
        }

        list.length = 0;


        for(let i = 0, length = filtered.length; i < length; i++) {
            list.push(filtered[i]);
        }

        return list;
    };

    /**
     * getCandidates
     * @desc Get offer candidates length
     */
    extension.getCandidates = function () {

        for(var i = 0, length = this.$list.length; i < length; i++) {
            if(this.$list[i].candidates) {
                this.$list[i].candidateLength = Object.keys(this.$list[i].candidates).length;
            }else {
                this.$list[i].candidateLength = 0;
            }
        }
    };

    /**
     * getSelectedCandidates
     * @desc Get offer selected candidates length
     */
    extension.getSelectedCandidates = function () {

        for(var i = 0, length = this.$list.length; i < length; i++) {
            var selectedCandidateLength = 0;

            if(this.$list[i].candidates) {
                for(var key in this.$list[i].candidates) {
                    if(this.$list[i].candidates[key].selected) {
                        selectedCandidateLength++;
                    }
                }
            }

            this.$list[i].selectedCandidateLength = selectedCandidateLength;
        }
    };

    /**
     * isNewCandidate
     * @desc Check if new candidate into offer
     */
    extension.isNewCandidate = function () {
        for(var i = 0, length = this.$list.length; i < length; i++) {
            if(this.$list[i].candidates) {
                for(var key in this.$list[i].candidates) {
                    if(this.$list[i].candidates[key].new) {
                        this.$list[i].isNew = true;
                        return;
                    }
                }
            }
        }
    };

    /**
     * sortByDate
     * @desc Sort candidates by registerDate
     * @return sorted list
     */
    extension.sortByDate = function () {
        this.$list.sort(function (a, b) {
            return moment(b.registerDate) - moment(a.registerDate);
        });
    };

    /**
     * sortList
     * @desc Sort list array with property
     * @param {string} property - any property
     * @return sorted list
     */
    extension.sortList = function(property) {
        this.$list.sort(function (a, b) {
            return moment(b[property]) - moment(a[property]);
        });
    };

    /**
     * isFinish
     * @desc Check if offer is finish
     */
    extension.isFinish = function () {
        let list = this.$list;

        for(let i = 0, length = list.length; i < length; i++) {
            if(moment(moment(), 'day').isAfter(moment(list[i].dates.endDate), 'day')) {
                list[i].isFinish = true;
            }else {
                list[i].isFinish = false;
            }
        }
    };

    /**
     * getTotal
     * @desc Get total of offer
     * @param {string} type - type of amount (ht/ttc)
     */
    extension.getTotal = function(type) {
        for(let item of this.$list) {

            let total = 0;

            if(item.payments) {
                for(let payment of item.payments) {
                    if(type === 'ht') total += payment.managementFees;

                    if(type === 'ttc') total += payment.total;
                }
            }

            item.total = total;
        }
    };

    /**
     * initializeNotifications
     * @desc Notifications system
     * @type {Object}
     */
    extension.initializeNotifications = init;

    /**
     * init
     * @desc Initialize notifications
     * @param {boolean} type - single or multiple
     */
    function init (type) {
        type = type || false;
        this.$list.globalNotifications = 0;

        if(type) {
            this.$list.globalNotifications = {
                card: 0,
                transfert: 0
            };
        }

        let notifications;

        for(var i = 0, length = this.$list.length; i < length; i++) {

            if(type) {
                notifications = {
                    card: {
                        newOffers: 0,
                        newOffersCheckout: 0,
                        isNotChecked: 0,
                        global: 0
                    },
                    transfert: {
                        newOffers: 0,
                        newOffersCheckout: 0,
                        isNotChecked: 0,
                        global: 0
                    }
                };

                switch(this.$list[i].paymentType) {
                    case 'CB':
                        initNotifications(this.$list[i], notifications.card);
                        this.$list[i].notifications = notifications.card;
                        this.$list.globalNotifications.card += notifications.card.global;
                        break;
                    case 'TRANSFERT':
                        initNotifications(this.$list[i], notifications.transfert);
                        this.$list[i].notifications = notifications.transfert;
                        this.$list.globalNotifications.transfert += notifications.transfert.global;
                        break;
                }

            } else {
                notifications = {
                    newOffers: 0,
                    newOffersCheckout: 0,
                    isNotChecked: 0,
                    global: 0
                };

                initNotifications(this.$list[i], notifications);

                this.$list[i].notifications = notifications;
                this.$list.globalNotifications += notifications.global;
            }
        }
    }

    /**
     * initNotifications
     * @desc Init all notifications
     * @param {object} listItem - $list item
     * @param {object} notifications - notifications informations
     */
    function initNotifications (listItem, notifications) {
        getNewOffers(listItem, notifications);
        getNewOffersCheckout(listItem, notifications);
        getIsNotChecked(listItem, notifications);
        getGlobal(notifications);
    }

    /**
     * getNewOffers
     * @desc Get new offers
     * @param {object} listItem - $list item
     * @param {object} notifications - notifications informations
     */
    function getNewOffers (listItem, notifications) {
        if(listItem.missions) {
            for(var key in listItem.missions) {
                if(listItem.missions[key].admin && listItem.missions[key].admin.newCurrent) {
                    notifications.newOffers++;
                }
            }
        }
    }

    /**
     * getNewOffersCheckout
     * @desc Get new checkout offers
     * @param {object} listItem - $list item
     * @param {object} notifications - notifications informations
     */
    function getNewOffersCheckout (listItem, notifications) {
        if(listItem.missions) {
            for(var key in listItem.missions) {
                if(listItem.missions[key].admin && listItem.missions[key].admin.newCheckout) {
                    notifications.newOffersCheckout++;
                }
            }
        }
    }

    /**
     * getIsNotChecked
     * @desc Get offers is not checked
     * @param {object} listItem - $list item
     * @param {object} notifications - notifications informations
     */
    function getIsNotChecked (listItem, notifications) {
        if(listItem.missions) {
            for(var key in listItem.missions) {
                if(listItem.missions[key].dates) {
                    if(!listItem.missions[key].checkout && !listItem.missions[key].abort && !moment(moment(), 'day').isAfter(moment(listItem.missions[key].dates.endDate), 'day')) {
                        notifications.isNotChecked++;
                    }
                }
            }
        }
    }

    /**
     * getGlobal
     * @desc Get global length of notifications
     * @param {object} notifications - notifications informations
     */
    function getGlobal (notifications) {
        for(var notification in notifications) {
            if(notification !== 'global') {
                notifications.global += notifications[notification];
            }
        }
    }

    /**
     * initializeQuotationNotifications
     * @desc Init notifications for quotations
     */
    extension.initializeQuotationNotifications = function() {

        let notifications = {
            notChecked: 0,
            newCurrent: 0,
            global: 0
        };

        for(let item of this.$list) {
            if(!item.admin.check) {
                notifications.notChecked++;
            }

            if(item.admin.newCurrent) {
                notifications.newCurrent++;
            }
        }

        notifications.global += notifications.notChecked;
        notifications.global += notifications.newCurrent;

        this.$list.notifications = notifications;
    };

    return $firebaseArray.$extend(extension);
};

/**
 * $inject
 * @desc Inject dependencies
 * @return {array} dependencies
 */
firebaseArrayCustom.$inject = [
    '$firebaseArray'
];

angular
    .module('app.firebase-array-custom', [])
    .factory('firebaseArrayCustom', firebaseArrayCustom);
