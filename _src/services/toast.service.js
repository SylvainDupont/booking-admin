/**
 * ToastService
 * @desc angular service for manage $mdToast from angular-material
 */
class ToastService {

	/**
	 * constructor
	 * @desc Create ToastService
	 * @param {object} $mdToast - service from angular-material
	 */
	constructor($mdToast) {
		this.$mdToast = $mdToast;
	}

	/**
	 * show
	 * @desc Show toast component
	 * @param {object} params - toast configuration
	 * @param {string} params.type - toast type
	 * @param {string} params.content - toast content
	 * @param {int} params.delay - toast delay
	 * @param {string} params.position - toast position
	 */
	show(params) {
		this.$mdToast.show({
			template: `
				<md-toast class="${params.type}">
					<span class="md-toast-text" flex>
						${params.content}
					</span>
				</md-toast>`,
			hideDelay: (params.delay) ? params.delay : 3000,
			position: (params.position) ? params.position : 'left bottom'
		});
	}

	/**
	 * $inject
	 * @desc Inject dependencies
	 * @return {array} dependencies
	 */
	static get $inject() {
		return [
			'$mdToast'
		];
	}
}

angular
	.module('app')
	.service('ToastService', ToastService);
