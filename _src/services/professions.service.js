/**
 * ProfessionsService
 * @desc angular service for manage agencies
 */
class ProfessionsService {

	/**
	 * constructor
	 * @desc Create ProfessionsService
	 * @param {object} $q - service from angular-core
	 * @param {object} $rootScope - service from angular-core
	 * @param {object} Auth - factory return $auth service from angularfire
	 * @param {object} $firebaseObject - service from angularfire
	 * @param {object} firebaseArrayCustom - service extends $firebaseArray from angularfire
	 * @param {object} firebaseObjectCustom - service extends $firebaseObject from angularfire
	 * @param {object} MailService - service manage email
	 * @param {object} ToastService - service manage $mdToast from angular-material
	 * @param {object} UtilsService - service with multiple tools
	 * @param {object} $mdDialog - service from angular-material
	 */
	constructor($q, $rootScope, Auth, $firebaseObject, firebaseArrayCustom, firebaseObjectCustom, MailService, ToastService, UtilsService, $mdDialog) {
		this.$q                   = $q;
		this.$rootScope           = $rootScope;
		this.Auth                 = Auth;
		this.$firebaseObject      = $firebaseObject;
		this.firebaseArrayCustom  = firebaseArrayCustom;
		this.firebaseObjectCustom = firebaseObjectCustom;
		this.MailService          = MailService;
		this.ToastService         = ToastService;
		this.UtilsService         = UtilsService;
		this.$mdDialog            = $mdDialog;
	}

	/**
	 * get
	 * @desc Get all professions
	 * @return {array}
	 */
	get() {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('professions')
				.orderByChild('name')
		);
	}


	/**
	 * getById
	 * @desc Get profession by id
	 * @param {string} professionId - profession uid
	 * @return {object}
	 */
	getById(professionId) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('professions')
				.child(professionId)
		);
	}

	/**
	 * addProfession
	 * @desc add new profession
	 * @param {object} profession - profession
	 * @return {object}
	 */
	addProfession(profession) {
		let professionRef = firebase.database().ref('professions');
		professionRef.push(profession).then(function (ref) {
			return ref;
		});
	}

	/**
	 * deleteProfession
	 * @desc delete profession
	 * @param {object} profession - profession
	 * @return {object}
	 */
	deleteProfession(professionId) {
		firebase
			.database()
			.ref('professions')
			.child(professionId).remove();
	}

	/**
	 * updateProfession
	 * @desc update profession
	 * @param {object} profession - profession
	 * @return {object}
	 */
	updateProfession(profession, currentUpdateId) {
		//Create promise
		let defer = this.$q.defer();

		let professionReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('professions')
				.child(profession.$id)
		);

		professionReference.$loaded(() => {
			professionReference.name = profession.name;
			professionReference.$save().then(() => {
				currentUpdateId.onUpdate = false;
				currentUpdateId.id = null;
			}).catch(()=> {
				this.ToastService.show({
					content: `Une erreur est survenue`,
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array} dependencies
     */
	static get $inject() {
		return [
			'$q',
			'$rootScope',
			'Auth',
			'$firebaseObject',
			'firebaseArrayCustom',
			'firebaseObjectCustom',
			'MailService',
			'ToastService',
			'UtilsService',
			'$mdDialog'
		];
	}
}

/**
 * @module agencies-service
 */
angular
	.module('professions-service', [])
    .service('ProfessionsService', ProfessionsService);
