/**
 * SidenavService
 * @desc angular service for manage $mdSidenav from angular-material
 */
class SidenavService {

	/**
	 * constructor
	 * @desc Create SidenavService
	 * @param {Object} $mdSidenav - service from angular-material
	 * @param {Object} $mdComponentRegistry - service from angular-material
	 */
	constructor($mdSidenav, $mdComponentRegistry) {
		this.$mdSidenav = $mdSidenav;
		this.$mdComponentRegistry = $mdComponentRegistry;
	}

	/**
	 * open
	 * @desc open event sidenav
	 * @param {String} id - sidenav id
	 */
	open(id) {
		this.$mdComponentRegistry
			.when(id)
			.then((id) => {
		  		id.open();
			});
	}

	/**
	 * close
	 * @desc close event sidenav
	 * @param {String} id - sidenav id
	 */
	close(id) {
		this.$mdComponentRegistry
			.when(id)
			.then((id) => {
		  		id.close();
			});
	}

	/**
	 * toggle
	 * @desc toggle event sidenav
	 * @param {String} id - sidenav id
	 */
	toggle(id) {
		this.$mdComponentRegistry
			.when(id)
			.then((id) => {
		  		id.toggle();
			});
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array} dependencies
     */
	static get $inject() {
		return [
			'$mdSidenav',
			'$mdComponentRegistry'
		];
	}
}

angular
	.module('app')
	.service('SidenavService', SidenavService);