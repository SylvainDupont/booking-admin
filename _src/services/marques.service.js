/**
 * MarquesService
 * @desc angular service for manage sepcialites
 */
class MarquesService {

	/**
	 * constructor
	 * @desc Create MarquesService
	 * @param {object} $q - service from angular-core
	 * @param {object} $rootScope - service from angular-core
	 * @param {object} Auth - factory return $auth service from angularfire
	 * @param {object} $firebaseObject - service from angularfire
	 * @param {object} firebaseArrayCustom - service extends $firebaseArray from angularfire
	 * @param {object} firebaseObjectCustom - service extends $firebaseObject from angularfire
	 * @param {object} MailService - service manage email
	 * @param {object} ToastService - service manage $mdToast from angular-material
	 * @param {object} UtilsService - service with multiple tools
	 * @param {object} $mdDialog - service from angular-material
	 */
	constructor($q, $rootScope, Auth, $firebaseObject, firebaseArrayCustom, firebaseObjectCustom, MailService, ToastService, UtilsService, $mdDialog) {
		this.$q                   = $q;
		this.$rootScope           = $rootScope;
		this.Auth                 = Auth;
		this.$firebaseObject      = $firebaseObject;
		this.firebaseArrayCustom  = firebaseArrayCustom;
		this.firebaseObjectCustom = firebaseObjectCustom;
		this.MailService          = MailService;
		this.ToastService         = ToastService;
		this.UtilsService         = UtilsService;
		this.$mdDialog            = $mdDialog;
	}

	/**
	 * get
	 * @desc Get all marques
	 * @return {array}
	 */
	get() {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('marques')
				.orderByChild('name')
		);
	}

	/**
	 * getById
	 * @desc Get marque by id
	 * @param {string} marqueId - marque uid
	 * @return {object}
	 */
	getById(marqueId) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('marques')
				.child(marqueId)
		);
	}

	/**
	 * addMarque
	 * @desc add new marque
	 * @param {object} marque - marque
	 * @return {object}
	 */
	addMarque(marque) {
		let marqueRef = firebase.database().ref('marques');
		marqueRef.push(marque).then(function (ref) {
			return ref;
		});
	}


	/**
	 * deleteMarque
	 * @desc delete marque
	 * @param {object} marque - marque
	 * @return {object}
	 */
	deleteMarque(marqueId) {
		firebase
			.database()
			.ref('marques')
			.child(marqueId).remove();
	}

	/**
	 * updateMarque
	 * @desc update marque
	 * @param {object} marque - marque
	 * @return {object}
	 */
	updateMarque(marque, currentUpdateId) {
		//Create promise
		let defer = this.$q.defer();

		let marqueReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('marques')
				.child(marque.$id)
		);

		marqueReference.$loaded(() => {
			marqueReference.name = marque.name;
			marqueReference.$save().then(() => {
				currentUpdateId.onUpdate = false;
				currentUpdateId.id = null;
			}).catch(()=> {
				this.ToastService.show({
					content: `Une erreur est survenue`,
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array} dependencies
     */
	static get $inject() {
		return [
			'$q',
			'$rootScope',
			'Auth',
			'$firebaseObject',
			'firebaseArrayCustom',
			'firebaseObjectCustom',
			'MailService',
			'ToastService',
			'UtilsService',
			'$mdDialog'
		];
	}
}

/**
 * @module agencies-service
 */
angular
	.module('marques-service', [])
    .service('MarquesService', MarquesService);
