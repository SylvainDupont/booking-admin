/**
 * PromoterService
 * @desc angular service manage promoter and agency
 */
class PromoterService {

	/**
	 * constructor
	 * @desc Create PromoterService
	 * @param {object} firebaseArrayCustom - service extends $firebaseArray from angularfire
	 * @param {object} firebaseObjectCustom - service extends $firebaseObject from angularfire
	 * @param {object} $firebaseObject - service from angularfire
	 * @param {object} MailService - service manage email
	 * @param {object} $mdPanel - service from angular-material
	 * @param {object} $mdDialog - service from angular-material
	 * @param {object} $mdToast - service from angular-material
	 */
	constructor(firebaseArrayCustom, firebaseObjectCustom, $firebaseObject, MailService, $mdPanel, $mdDialog, $mdToast) {
		this.firebaseArrayCustom = firebaseArrayCustom;
		this.firebaseObjectCustom = firebaseObjectCustom;
		this.$firebaseObject = $firebaseObject;
		this.MailService = MailService;
		this.$mdPanel = $mdPanel;
		this.$mdDialog = $mdDialog;
		this.$mdToast = $mdToast;
	}

	/**
	 * get
	 * @desc Get agency list
	 * @param {string} role - agency role (hostess, promoter)
	 * @return {array}
	 */
	get(role) {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.orderByChild('role')
				.equalTo(role)
		);
	}

	/**
	 * getCurrentOffer
	 * @desc Get agency current offer list
	 * @param {string} promoterId - agency uid
	 * @return {array}
	 */
	getCurrentOffer(promoterId) {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
				.child('missions')
				.orderByChild('dates/endDate')
				.startAt(moment().subtract(1, 'day').format())
		);
	}

	/**
	 * getHistoryOffer
	 * @desc Get agency history offer list
	 * @param {string} promoterId - agency uid
	 * @return {array}
	 */
	getHistoryOffer(promoterId) {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
				.child('missions')
				.orderByChild('dates/endDate')
				.endAt(moment().subtract(1, 'day').format())
		);
	}

	/**
	 * getQuotation
	 * @desc Get quotation list
	 * @return {array}
	 */
	getQuotation() {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('quotations')
		);
	}

	/**
	 * getQuotationById
	 * @desc Get quotation item
	 * @return {array}
	 */
	getQuotationById(id) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('quotations')
				.child(id)
		);
	}

	/**
	 * getQuotationLength
	 * @desc Get quotation list length
	 * @return {array}
	 */
	getQuotationLength() {
		return this.$firebaseObject(
			firebase
				.database()
				.ref('config')
				.child('quotationLength')
		);
	}

	/**
	 * checkPayment
	 * @desc check offer payment
	 * @param {string} agencyId - agency uid
	 * @param {string} offerId - offer uid
	 */
	checkPayment(agencyId, offerId) {

		const confirm = this.$mdDialog.confirm({
				skipHide: true
			})
	        .title('Traitement du paiement')
	        .textContent('Etes-vous sûr de valider l\'offre ?')
	        .ariaLabel('check-payment')
	        .ok('Oui')
	        .cancel('Non');

	    this.$mdDialog.show(confirm).then(() => {

			const offerReference = this.$firebaseObject(
				firebase
					.database()
					.ref('agency')
					.child(agencyId)
					.child('missions')
					.child(offerId)
			);

			offerReference.$loaded((offer) => {

				offer.admin.newCheckout = false;
				offer.admin.paymentCheck = true;

				offerReference.$save().then(() => {

					this.$mdToast.show(
						this.$mdToast
							.simple({
								position: 'bottom right'
							})
							.textContent(`Le paiement de "${offer.title}" a bien été validé`)
					);
				});
			});
		});
	}

	/**
	 * viewOffer
	 * @desc View agency offer details
	 * @param {object} offer - offer informations
	 * @param {object} promoter - agency informations
	 */
	viewOffer(offer, promoter) {

		this.promoter = promoter;
		this.offerId = offer.$id;

		if(offer.admin.newCurrent) {
			let offerReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('agency')
					.child(promoter.$id)
					.child('missions')
					.child(offer.$id)
			);

			offerReference.$loaded(() => {
				offerReference.admin.newCurrent = false;
				offerReference.$save();
			});
		}

		const position = this.$mdPanel
			.newPanelPosition()
      		.absolute()
      		.right()
      		.top();

      	const animation = this.$mdPanel
      		.newPanelAnimation();

      	animation.openFrom({
        	top: document.documentElement.clientHeight,
        	left: 0
      	});

      	animation.withAnimation(this.$mdPanel.animation.SLIDE);

      	let panelConfig = {
		    animation: animation,
		    attachTo: angular.element(document.body),
		    controller: () => this,
		    controllerAs: '$ctrl',
		    template: `
		    	<promoter-panel-offer
		    		promoter="$ctrl.promoter"
		    		offer-id="$ctrl.offerId"
		    		panel-reference="$ctrl.panelOfferReference">
		    	</promoter-panel-offer>`,
		    panelClass: 'panel',
		    position: position,
		    clickEscapeToClose: true
		};

		this.panelOfferReference = this.$mdPanel.create(panelConfig);
		this.panelOfferReference.open();
	}

	/**
	 * getOffer
	 * @desc Get agency offer item
	 * @param {string} promoterId - agency uid
	 * @param {string} offerId - offer uid
	 * @return {object}
	 */
	getOffer(promoterId, offerId) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
				.child('missions')
				.child(offerId)
		);
	}

	/**
	 * getCandidate
	 * @desc Get agency offer candidates list
	 * @param {string} promoterId - agency uid
	 * @param {string} offerId - offer uid
	 * @return {array}
	 */
	getCandidates(promoterId, offerId) {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
				.child('missions')
				.child(offerId)
				.child('candidates')
		);
	}

	/**
	 * getCandidate
	 * @desc Get candidate profile
	 * @param {string} candidateId - candidate uid
	 * @param {string} promoterId - promoter uid
	 * @param {string} offerId - offer uid
	 * @return {object}
	 */
	getCandidate(candidateId, promoterId, offerId) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
				.child('missions')
				.child(offerId)
				.child('candidates')
				.child(candidateId)
		);
	}

	/**
	 * removeCandidateFromOffer
	 * @desc Remove candidate to offer candidate list
	 * @param {string} candidateId - candidate uid
	 * @param {string} promoterId - agency uid
	 * @param {string} offerId - offer uid
	 */
	removeCandidateFromOffer(candidateId, promoterId, offerId) {

		const confirm = this.$mdDialog.confirm({
				skipHide: true
			})
	        .title('Suppression d\'un candidat')
	        .textContent('Voulez-vous vraiment supprimer ce candidat de l\'offre ?')
	        .ariaLabel('Remove candidate from offer')
	        .ok('Oui')
	        .cancel('Non');

    	this.$mdDialog.show(confirm).then(() => {

			let offerReference = this.firebaseObjectCustom(
				firebase
					.database()
					.ref('agency')
					.child(promoterId)
					.child('missions')
					.child(offerId)
			);

			offerReference.$loaded(() => {

				delete offerReference.candidates[candidateId];

				offerReference.$save().then(() => {

					let candidateReference = this.firebaseObjectCustom(
						firebase
							.database()
							.ref('candidate')
							.child(candidateId)
					);

					candidateReference.$loaded(() => {

						delete candidateReference.missions[offerId];
						candidateReference.$save().then(() => {

							this.$mdToast.show(
								this.$mdToast
									.simple({
										position: 'bottom right'
									})
									.textContent(`Le candidat ${candidateReference.firstname} ${candidateReference.lastname} a bien été supprimé de ${offerId}`)
							);
						});

					});
				});
			});
    	});
	}

	/**
	 * viewCandidateProfile
	 * @desc View candidate profile
	 * @param {object} candidate - candidate informations
	 * @param {string} promoterId - agency uid
	 * @param {string} offerId - offer uid
	 */
	viewCandidateProfile(candidate, promoterId, offerId) {

		this.candidateId = candidate.$id;
		this.offerSelected = candidate.selected;
		this.promoterId = promoterId || false;
		this.offerId = offerId || false;

		const position = this.$mdPanel
			.newPanelPosition()
      		.absolute()
      		.right()
      		.top();

      	const animation = this.$mdPanel
      		.newPanelAnimation();

      	animation.openFrom({
        	top: 0,
        	left: document.documentElement.clientWidth
      	});

      	animation.withAnimation(this.$mdPanel.animation.SLIDE);

      	let panelConfig = {
		    animation: animation,
		    attachTo: angular.element(document.body),
		    controller: () => this,
		    controllerAs: '$ctrl',
		    template: `
		    	<promoter-panel-candidate
		    		offer-selected="$ctrl.offerSelected"
		    		candidate-id="$ctrl.candidateId"
		    		promoter-id="$ctrl.promoterId"
		    		offer-id="$ctrl.offerId"
		    		panel-reference="$ctrl.panelCandidateReference">
		    	</promoter-panel-candidate>`,
		    panelClass: 'panel',
		    position: position,
		    clickEscapeToClose: true
		};

		this.panelCandidateReference = this.$mdPanel.create(panelConfig);
		this.panelCandidateReference.open();
	}

	/**
	 * viewCandidatePicture
	 * @desc View candidate gallery
	 * @param {object} $event - targetEvent
	 * @param {int} selectedPictureIndex - picture index
	 * @param {object} candidate - candidate informations
	 */
	viewCandidatePicture($event, selectedPictureIndex, candidate) {

		this.selectedPictureIndex = selectedPictureIndex;
		this.candidate = candidate;

		const dialog = {
			templateUrl: './components/candidates/candidate.profile.picture.html',
			parent: angular.element(document.body),
			targetEvent: $event,
			clickOutsideToClose: false,
			controller: () => this,
  			controllerAs: '$ctrl',
			fullscreen: true,
			hasBackdrop: false,
			skipHide: true
		};

		this.$mdDialog.show(dialog);
	}

	/**
	 * viewCandidateList
	 * @desc View agency offer candidates list
	 * @param {object} candidates - candidates list
	 * @param {string} promoterId - agency uid
	 * @param {string} offerId - offer uid
	 */
	viewCandidateList(candidates, promoterId, offerId) {
		this.panelCandidates = candidates;
		this.panelPromoterId = promoterId;
		this.panelOfferId = offerId;

		const position = this.$mdPanel
			.newPanelPosition()
      		.absolute()
      		.right()
      		.top();

      	const animation = this.$mdPanel
      		.newPanelAnimation();

      	animation.openFrom({
        	top: document.documentElement.clientHeight,
        	left: 0
      	});

      	animation.withAnimation(this.$mdPanel.animation.SLIDE);

      	let panelConfig = {
		    animation: animation,
		    attachTo: angular.element(document.body),
		    controller: () => this,
		    controllerAs: '$ctrl',
		    template: `
		    	<promoter-panel-candidate-list
		    		candidates="$ctrl.panelCandidates"
		    		promoter-id="$ctrl.panelPromoterId"
		    		offer-id="$ctrl.panelOfferId"
		    		panel-reference="$ctrl.panelCandidateListReference">
		    	</promoter-panel-candidate-list>`,
		    panelClass: 'panel',
		    position: position,
		    clickEscapeToClose: true
		};

		this.panelCandidateListReference = this.$mdPanel.create(panelConfig);
		this.panelCandidateListReference.open();
	}

	/**
	 * removeOffer
	 * @desc Remove agency offer
	 * @param {string} promoterId - agency uid
	 * @param {string} offerId - offer uid
	 * @return {object}
	 */
	removeOffer(promoterId, offerId) {

		let promise = new Promise((resolve, reject) => {
			const confirm = this.$mdDialog.confirm()
				.title('Suppression de l\'offre')
				.textContent('Voulez-vous vraiment supprimer cette offre ?')
				.ariaLabel('Remove offer')
				.ok('Oui')
				.cancel('Non');

			this.$mdDialog.show(confirm).then(() => {

				let offerReference = this.firebaseObjectCustom(
					firebase
						.database()
						.ref('agency')
						.child(promoterId)
						.child('missions')
						.child(offerId)
				);

				offerReference.$remove().then(() => {

					let candidatesReference = this.firebaseArrayCustom(
						firebase
							.database()
							.ref('candidate')
							.orderByChild(`missions/${offerId}/agencyId`)
							.equalTo(promoterId)
					);

					candidatesReference.$loaded((candidates) => {

						for(let candidate of candidatesReference) {
							if(candidate.missions) {
								if(_.contains(Object.keys(candidate.missions), offerId)) {
									console.log(candidate);
									delete candidate.missions[offerId];
									candidatesReference.$save(candidate);
								}
							}
						}

						resolve();
					});
				});
			});
		});

		return promise;
	}

	/**
	 * shareOffer
	 * @desc Share offer on facebook
	 * @param {string} promoterId - agency uid
	 * @param {object} offer - offer informations
	 */
	shareOffer(promoterId, offer) {

		let shareDescription;

		if(offer.dates.type !== 'uniq') {
			shareDescription = `Du ${moment(offer.dates.startDate).format('DD/MM/YYYY')} au ${moment(offer.dates.endDate).format('DD/MM/YYYY')} à ${offer.address}`;
		}else {
			shareDescription = `le ${moment(offer.dates.startDate).format('DD/MM/YYYY')} à ${offer.address}`;
		}

		FB.ui({
		  	method: 'share',
		  	href: 'https://app.candidat.booking-hotesses.com/job-board/' + promoterId + '/' + offer.$id,
		  	title: `${offer.title}`,
		  	caption: 'Pour plus de détails et pour postuler, cliquez ici | Booking Hôtesses',
		  	description: shareDescription,
		  	picture: 'https://app.admin.booking-hotesses.com/img/booking-hotesses-1200x650-fb-final.jpg'
		}, (response) => {
			console.log(response);
		});
	}

	/**
	 * disableAccount
	 * @desc Disable agency account
	 * @param {string} promoterId - agency uid
	 */
	disableAccount(promoterId) {

		let notification = this.$mdToast.simple({
			position: 'bottom right'
		});

		let promoterReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
		);

		promoterReference.$loaded(() => {

			promoterReference.disabled = !promoterReference.disabled;

			promoterReference.$save().then(() => {

				if(promoterReference.disabled) {
					notification.textContent(`${promoterReference.name} a été désactivé`);
				}else {
					notification.textContent(`${promoterReference.name} a été activé`);
				}

				this.$mdToast.show(notification);
			});

		});
	}

	/**
	 * toggleValid
	 * @desc Toggle agency validation
	 * @param {string} promoterId - agency uid
	 */
	toggleValid(promoterId) {

		let notification = this.$mdToast.simple({
			position: 'bottom right'
		});

		let promoterReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
		);

		promoterReference.$loaded(() => {

			promoterReference.validate = !promoterReference.validate;

			promoterReference.$save().then(() => {

				if(promoterReference.validate) {
					this.MailService.send({
						mail: promoterReference.mail,
						firstname: promoterReference.name,
						code: 'agency-active'
					}).then((res) => {
						notification.textContent(`${promoterReference.name} est désormais valide`);
						this.$mdToast.show(notification);
					});
				}else {
					notification.textContent(`${promoterReference.name} est désormais invalide`);
					this.$mdToast.show(notification);
				}
			});
		});
    }

	/**
	 * togglePaymentType
	 * @desc Toggle agency payment type
	 * @param {string} promoterId - agency uid
	 */
	togglePaymentType(promoterId) {

		let notification = this.$mdToast.simple({
			position: 'bottom right'
		});

		let promoterReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
		);

		promoterReference.$loaded(() => {

			switch(promoterReference.paymentType) {
				case 'CB':
					promoterReference.paymentType = 'TRANSFERT';
					notification.textContent('Le type de paiement de l\'agence "' + promoterReference.name + '" est désormais par prélèvement');
					break;
				case 'TRANSFERT':
					promoterReference.paymentType = 'CB';
					notification.textContent('Le type de paiement de l\'agence "' + promoterReference.name + '" est désormais par carte bancaire');
					break;
			}

			promoterReference.$save().then(() => {
				this.$mdToast.show(notification);
			});
		});
	}

	/**
	 * changeRole
	 * @desc Change agency role
	 * @param {string} promoterId - agency uid
	 * @param {string} role - agency role (hostess, promoter)
	 */
	changeRole(promoterId, role) {

		let notification = this.$mdToast.simple({
			position: 'bottom right'
		});

		let promoterReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
		);

		promoterReference.$loaded(() => {

			promoterReference.role = role;

			switch(promoterReference.role) {
				case 'hostess':
					notification.textContent('Le role de "' + promoterReference.name + '" est désormais agence');
					break;
				case 'promoter':
					promoterReference.paymentType = 'CB';
					notification.textContent('Le role de "' + promoterReference.name + '" est désormais organisateur');
					break;
			}

			promoterReference.$save().then(() => {
				this.$mdToast.show(notification);
			});
		});
	}

	/**
	 * deleteAccount
	 * @desc Delete agency account
	 * @param {string} promoterId - agency uid
	 */
	deleteAccount(promoterId) {
		let notification = this.$mdToast.simple({
			position: 'bottom right'
		});

		let promoterReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
		);

		promoterReference.$remove().then(() => {
			notification.textContent('Le compte de "' + promoterReference.name + '" a été supprimé');
			this.$mdToast.show(notification);
		});
	}

	/**
	 * viewQuotation
	 * @desc View quotation details
	 * @param {object} quotation - quotation informations
	 */
	viewQuotation(quotation) {

		this.quotation = quotation;

		if(quotation.admin.newCurrent) {
			let quotationReference = this.$firebaseObject(
				firebase
					.database()
					.ref('quotations')
					.child(quotation.$id)
			);

			quotationReference.$loaded(() => {
				quotationReference.admin.newCurrent = false;
				quotationReference.$save();
			});
		}

		const position = this.$mdPanel
			.newPanelPosition()
      		.absolute()
      		.right()
      		.top();

      	const animation = this.$mdPanel
      		.newPanelAnimation();

      	animation.openFrom({
        	top: document.documentElement.clientHeight,
        	left: 0
      	});

      	animation.withAnimation(this.$mdPanel.animation.SLIDE);

      	let panelConfig = {
		    animation: animation,
		    attachTo: angular.element(document.body),
		    controller: () => this,
		    controllerAs: '$ctrl',
		    template: `
		    	<promoter-panel-quotation
		    		quotation="$ctrl.quotation"
		    		panel-reference="$ctrl.panelQuotationReference">
		    	</promoter-panel-quotation>`,
		    panelClass: 'panel',
		    position: position,
		    clickEscapeToClose: true
		};

		this.panelQuotationReference = this.$mdPanel.create(panelConfig);
		this.panelQuotationReference.open();
	}

	/**
	 * checkQuotation
	 * @desc Check quotation is valid
	 * @param {object} quotation - quotation informations
	 */
	checkQuotation(quotation) {

		let notification = this.$mdToast.simple({
			position: 'bottom right'
		});

		let quotationReference = this.$firebaseObject(
			firebase
				.database()
				.ref('quotations')
				.child(quotation.$id)
		);

		quotationReference.$loaded(() => {

			quotationReference.admin.check = true;

			quotationReference.$save().then(() => {

				notification.textContent('Le devis n°' + quotationReference.$id + ' a été validé');
				this.$mdToast.show(notification);
			});
		});
	}

	/**
	 * removeQuotation
	 * @desc Remove quotation item
	 * @param {object} quotation - quotation information
	 */
	removeQuotation(quotation) {
		let notification = this.$mdToast.simple({
			position: 'bottom right'
		});

		let quotationReference = this.$firebaseObject(
			firebase
				.database()
				.ref('quotations')
				.child(quotation.$id)
		);

		quotationReference.$remove().then(() => {
			notification.textContent('Le devis n°' + quotationReference.$id + ' a bien été supprimé');
			this.$mdToast.show(notification);
		});
	}

	/**
	 * updateNbPublications
	 * @desc Update Nbr Publication promoter
	 * @param {string} promoterId - promoter uid
	 * @param {integer} nbrPublication - publication number
	 */
	updateNbPublications(promoterId, nbrPublication) {

		//Get agency reference
		const promoterReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
		);

		promoterReference.$loaded((promoter) => {
			//Toggle validation
			promoter.nbrPublication = nbrPublication;

			//Save agency reference
			promoterReference.$save().then(() => {
				//Success notification
				this.ToastService.show({
					content: 'Modification effectuée avec succès',
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}

	/**
	 * updateNbBackUpPublications
	 * @desc Update Nbr Publication BackUp promoter
	 * @param {string} promoterId - promoter uid
	 * @param {integer} nbrPublication - publication number
	 */
	updateNbBackUpPublications(promoterId, nbrPublication) {

		//Get agency reference
		const promoterReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('agency')
				.child(promoterId)
		);

		promoterReference.$loaded((promoter) => {
			//Toggle validation
			promoter.nbrBackUpPublication = nbrPublication;

			//Save agency reference
			promoterReference.$save().then(() => {
				//Success notification
				this.ToastService.show({
					content: 'Modification effectuée avec succès',
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}

	/**
	 * $inject
	 * @desc Inject dependencies
	 * @return {array}
	 */
	static get $inject() {
		return [
			'firebaseArrayCustom',
			'firebaseObjectCustom',
			'$firebaseObject',
			'MailService',
			'$mdPanel',
			'$mdDialog',
			'$mdToast'
		];
	}
}

/**
 * @module promoter-service
 */
angular
	.module('promoter-service', [])
    .service('PromoterService', PromoterService);
