/**
 * LocalStorageService
 * @desc angular service for manage localStorage
 */
class LocalStorageService {

	/**
	 * constructor
	 * @desc Create LocalStorageService
	 */
	constructor() {}

	/**
	 * get
	 * Get localstorage item with id
	 * @param  {string} id - localstorage id
	 * @return {object} item
	 */
	get(id) {
		return JSON.parse(localStorage.getItem(id));
	}

	/**
	 * set
	 * Set new localstorage item
	 * @param {string} id - localstorage id
	 * @param {Any} data 
	 */
	set(id, data) {
		localStorage.setItem(id, JSON.stringify(data));
	}

	/**
	 * remove
	 * Remove localstorage item with id
	 * @param {string} id - localstorage id
	 */
	remove(id) {
		localStorage.removeItem(id);
	}
}

angular
	.module('app')
	.service('LocalStorageService', LocalStorageService);