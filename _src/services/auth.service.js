/**
 * AuthService
 * @desc angular service for app authentication
 */
class AuthService {

    /**
     * constructor
     * @desc Create AuthService
     * @param {Object} $q - service from angular-core
     * @param {Object} $rootScope - service from angular-core
     * @param {Object} Auth - factory return $auth service from angularfire
     * @param {Object} $firebaseObject - service from angularfire
     * @param {Object} $firebaseArray - service from angularfire
     * @param {Object} ToastService - service manage $mdToast
     * @param {Object} LoaderService - service manage app loader
     * @param {Object} $rootRouter - service from angular-component-router
     * @param {Object} $mdToast - service from angular-material
     */
    constructor($q, $rootScope, Auth, $firebaseObject, $firebaseArray, ToastService, LoaderService, $rootRouter, $mdToast) {
        this.$q              = $q;
        this.$rootScope      = $rootScope;
        this.Auth            = Auth;
        this.$firebaseObject = $firebaseObject;
        this.$firebaseArray  = $firebaseArray;
        this.ToastService    = ToastService;
        this.LoaderService   = LoaderService;
        this.$rootRouter     = $rootRouter;
        this.$mdToast        = $mdToast;
    }

    /**
     * isAuthenticated
     * @desc Check if user is authenticated
     * @return {object}
     */
    isAuthenticated() {

        //Create promise
        let defer = this.$q.defer();

        //Check if auth state change
        this.Auth.$onAuthStateChanged((firebaseUser) => {

            //If is not auth
            if(!firebaseUser) {

                //Reject promise
                defer.reject('ERROR, FIREBASE USER DOES NOT EXISTS');

            //If is auth
            }else {

                //Get user credentials
                this.createUserCredentials(firebaseUser).then(() => {

                    //Resolve promise
                    defer.resolve('SUCCESS, FIREBASE USER EXISTS');
                });
            }
        });

        return defer.promise;
    }

    /**
     * redirectUnauthenticated
     * @desc Redirect user if is not authenticated
     * @return {boolean}
     */
    redirectUnauthenticated() {

        //Check user is authenticated
        this.isAuthenticated().then(() => {
            return true;
        }, (error) => {

            //Navigate to signin component
            this.$rootRouter.navigate(['Signin']);
            return false;
        });
    }

    /**
     * createUserCredentials
     * @desc Set credentials into $rootscope
     * @param {Object} firebaseUser - user informations
     * @return {object}
     */
    createUserCredentials(firebaseUser) {

        //Create promise
        let defer = this.$q.defer();

        if(!firebaseUser) {
            return;
        }

        //Get user credentials
        let user = this.$firebaseObject(
            firebase
                .database()
                .ref('admin')
                .child(firebaseUser.uid)
        );

        user.$loaded(() => {

            //Set user credentials to $rootScope
            this.$rootScope.userCredential = {
                mail: firebaseUser.email,
                uid: firebaseUser.uid,
                fullaccess: user.fullaccess,
                name: `${user.firstname} ${user.lastname}`
            };

            //Resolve promise
            defer.resolve();

        });

        return defer.promise;
    }

    /**
     * signIn
     * @desc Sign in user and redirect to dashboard page
     * @param {object} credentials
     */
    signIn(credentials) {

        if(!credentials) {
            return;
        }

        //Start loading screen
        this.LoaderService.start();

        //Signin user
        this.Auth.$signInWithEmailAndPassword(credentials.mail, credentials.password).then((firebaseUser) => {

            //Check if user is admin
            this.isAdmin(firebaseUser.uid).then((isAdmin) => {

                //User is not admin
                if(!isAdmin) {

                    //Error notification
                    this.ToastService.show({
                        content: 'Erreur, vous n\'êtes pas administrateur',
                        type: 'simple',
                        position: 'bottom right'
                    });

                    //Stop loading screen
                    this.LoaderService.stop();
                    return;
                }

                //Get user profile
                this.createUserCredentials(firebaseUser).then(() => {

                    //Success notification
                    this.ToastService.show({
                        content: `Bienvenue ${this.$rootScope.userCredential.name}`,
                        type: 'simple',
                        position: 'bottom right'
                    });

                    //Navigate to dashboard component
                    this.$rootRouter.navigate(['Dashboard']);

                    //Stop loading screen
                    this.LoaderService.stop();
                });
            });
        }, (error) => {

            let message;

            //Check error code
            switch(error.code) {
                case 'auth/invalid-email':
                    message = 'Erreur, cette adresse email n\'est pas valide';
                    break;
                case 'auth/user-disabled':
                    message = 'Erreur, votre compte à été désactivé';
                    break;
                case 'auth/user-not-found':
                    message = 'Erreur, aucun compte n\'existe avec cette adresse email';
                    break;
                case 'auth/wrong-password':
                    message = 'Erreur, mot de passe incorrect';
                    break;
                default:
                    message = 'Oups, le couple email / mot de passe est incorrect';
                    break;
            }

            //Error notification
            this.ToastService.show({
                content: message,
                type: 'simple',
                position: 'bottom right'
            });

            //Stop loading screen
            this.LoaderService.stop();
        });
    }

    /**
     * signOut
     * @desc Sign out user and redirect to sign in page
     */
    signOut() {

        //Start loading screen
        this.LoaderService.start();

        //Configure notification
        const signOutToast = this.$mdToast.show({
            templateUrl: './components/toast/toast-signout.template.html',
            hideDelay: 0,
            position: 'bottom left'
        });

        //Signout user
        this.Auth.$signOut().then(() => {

            //Navigate to signin component
            this.$rootRouter.navigate(['Signin']).then(() => {

                //Hide notification
                this.$mdToast.hide(signOutToast);
            });

            //Stop loading screen
            this.LoaderService.stop();
        });
    }

    /**
     * isAdmin
     * @desc Check if user is admin
     * @param {string} uid
     */
    isAdmin(uid) {

        //Create promise
        let defer = this.$q.defer();

        //Get user profile
        const admin = this.$firebaseObject(
            firebase
                .database()
                .ref('admin')
                .child(uid)
        );

        admin.$loaded(() => {

            //Resolve promise
            defer.resolve(admin.$value !== null);
        }, (error) => {

            //Debug error
            console.log(error);
        });

        return defer.promise;
    }

    /**
     * $inject
     * @desc Inject dependencies
     * @return {array} dependencies
     */
    static get $inject() {
        return [
            '$q',
            '$rootScope',
            'Auth',
            '$firebaseObject',
            '$firebaseArray',
            'ToastService',
            'LoaderService',
            '$rootRouter',
            '$mdToast'
        ];
    }

}

angular
    .module('app')
    .service('AuthService', AuthService);
