/**
 * SpecialitesService
 * @desc angular service for manage sepcialites
 */
class SpecialitesService {

	/**
	 * constructor
	 * @desc Create SpecialitesService
	 * @param {object} $q - service from angular-core
	 * @param {object} $rootScope - service from angular-core
	 * @param {object} Auth - factory return $auth service from angularfire
	 * @param {object} $firebaseObject - service from angularfire
	 * @param {object} firebaseArrayCustom - service extends $firebaseArray from angularfire
	 * @param {object} firebaseObjectCustom - service extends $firebaseObject from angularfire
	 * @param {object} MailService - service manage email
	 * @param {object} ToastService - service manage $mdToast from angular-material
	 * @param {object} UtilsService - service with multiple tools
	 * @param {object} $mdDialog - service from angular-material
	 */
	constructor($q, $rootScope, Auth, $firebaseObject, firebaseArrayCustom, firebaseObjectCustom, MailService, ToastService, UtilsService, $mdDialog) {
		this.$q                   = $q;
		this.$rootScope           = $rootScope;
		this.Auth                 = Auth;
		this.$firebaseObject      = $firebaseObject;
		this.firebaseArrayCustom  = firebaseArrayCustom;
		this.firebaseObjectCustom = firebaseObjectCustom;
		this.MailService          = MailService;
		this.ToastService         = ToastService;
		this.UtilsService         = UtilsService;
		this.$mdDialog            = $mdDialog;
	}

	/**
	 * get
	 * @desc Get all specialites
	 * @return {array}
	 */
	get() {
		return this.firebaseArrayCustom(
			firebase
				.database()
				.ref('specialites')
				.orderByChild('name')
		);
	}

	/**
	 * getById
	 * @desc Get specialite by id
	 * @param {string} specialiteId - specialite uid
	 * @return {object}
	 */
	getById(specialiteId) {
		return this.firebaseObjectCustom(
			firebase
				.database()
				.ref('specialites')
				.child(specialiteId)
		);
	}

	/**
	 * addSpecialite
	 * @desc add new specialite
	 * @param {object} specialite - specialite
	 * @return {object}
	 */
	addSpecialite(specialite) {
		let specialiteRef = firebase.database().ref('specialites');
		specialiteRef.push(specialite).then(function (ref) {
			return ref;
		});
	}


	/**
	 * deleteSpecialite
	 * @desc delete specialite
	 * @param {object} specialite - specialite
	 * @return {object}
	 */
	deleteSpecialite(specialiteId) {
		firebase
			.database()
			.ref('specialites')
			.child(specialiteId).remove();
	}

	/**
	 * updateSpecialite
	 * @desc update specialite
	 * @param {object} specialite - specialite
	 * @return {object}
	 */
	updateSpecialite(specialite, currentUpdateId) {
		//Create promise
		let defer = this.$q.defer();

		let specialiteReference = this.firebaseObjectCustom(
			firebase
				.database()
				.ref('specialites')
				.child(specialite.$id)
		);

		specialiteReference.$loaded(() => {
			specialiteReference.name = specialite.name;
			specialiteReference.$save().then(() => {
				currentUpdateId.onUpdate = false;
				currentUpdateId.id = null;
			}).catch(()=> {
				this.ToastService.show({
					content: `Une erreur est survenue`,
					type: 'simple',
					position: 'bottom right'
				});
			});
		});
	}

	/**
     * $inject
     * @desc Inject dependencies
     * @return {array} dependencies
     */
	static get $inject() {
		return [
			'$q',
			'$rootScope',
			'Auth',
			'$firebaseObject',
			'firebaseArrayCustom',
			'firebaseObjectCustom',
			'MailService',
			'ToastService',
			'UtilsService',
			'$mdDialog'
		];
	}
}

/**
 * @module agencies-service
 */
angular
	.module('specialites-service', [])
    .service('SpecialitesService', SpecialitesService);
