/**
 * Run
 * Root angular module run
 * @param {object} $rootScope - service from angular-core
 * @param {object} $mdDialog - service from angular-material
 * @param {object} SidenavService - service manage $mdSidenav from angular-material
 * @param {object} $location - service from angular-core
 * @param {object} $templateCache - service from angular-core
 */
const run = ($rootScope, $mdDialog, SidenavService, $location, $templateCache) => {

    //Toggle sidenav
    $rootScope.sidebarToggle = function () {
    	SidenavService.toggle('left');
    };

    //Scroll to top on change success
    $rootScope.$on('$locationChangeSuccess', (e) => {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });

    //Assets path
    $rootScope.assets = '../img/';

    //Icons color palette
    $rootScope.iconColor = {
        pink: 'color: #FD3B6D',
        white: 'color: #FFFFFF',
        lightGrey: 'color: #CCCCCC'
    };

    //Close all dialogs
    $rootScope.closeDialog = function () {
        $mdDialog.hide();
    };

    //Filters configuration
    $rootScope.filterConfig = {
        performance: [
            {
                name: 'Accueil Evènementiel',
                value: 'Accueil Evènementiel'
            },
            {
                name: 'Animation Commerciale / Vente',
                value: 'Animation Commerciale / Vente'
            },
            {
                name: 'Accueil Entreprise',
                value: 'Accueil Entreprise'
            },
            {
                name: 'Street Marketing',
                value: 'Street Marketing'
            },
            {
                name: 'Manutention',
                value: 'Manutention'
            },
            {
                name: 'Service',
                value: 'Service'
            },
            {
                name: 'Modèle',
                value: 'Modèle'
            }
        ],
        sexe: [
            {
                name: 'Homme',
                value: 'homme'
            },
            {
                name: 'Femme',
                value: 'femme'
            }
        ],
        studyLevel: [
            {
                name: 'Sans diplôme',
                value: 'Sans diplôme'
            },
            {
                name: 'Brevet',
                value: 'Brevet'
            },
            {
                name: 'BAC, CAP',
                value: 'BAC, CAP'
            },
            {
                name: 'BTS, IUT',
                value: 'BTS, IUT'
            },
            {
                name: 'Licence',
                value: 'Licence'
            },
            {
                name: 'Master',
                value: 'Master'
            }
        ],
        // languages: [
        //     {
        //         name: 'Francais',
        //         value: 'Francais'
        //     },
        //     {
        //         name: 'Italien',
        //         value: 'Italien'
        //     },
        //     {
        //         name: 'Chinois',
        //         value: 'Chinois'
        //     },
        //     {
        //         name: 'Espagnol',
        //         value: 'Espagnol'
        //     },
        //     {
        //         name: 'Anglais',
        //         value: 'Anglais'
        //     },
        //     {
        //         name: 'Allemand',
        //         value: 'Allemand'
        //     },
        //     {
        //         name: 'Arabe',
        //         value: 'Arabe'
        //     },
        //     {
        //         name: 'Russe',
        //         value: 'Russe'
        //     },
        // ],
        languages: [
            {
                name: 'Allemand',
                value: 'Allemand'
            },
            {
                name: 'Anglais',
                value: 'Anglais'
            },
            {
                name: 'Arabe',
                value: 'Arabe'
            },
            {
                name: 'Chinois',
                value: 'Chinois'
            },
            {
                name: 'Coréen',
                value: 'Coréen'
            },
            {
                name: 'Espagnol',
                value: 'Espagnol'
            },
            {
                name: 'Flamand',
                value: 'Flamand'
            },
            {
                name: 'Italien',
                value: 'Italien'
            },
            {
                name: 'Néerlandais',
                value: 'Néerlandais'
            },
            {
                name: 'Portugais',
                value: 'Portugais'
            },
            {
                name: 'Russe',
                value: 'Russe'
            },
        ],
        dispoDerniereMinute: [
            {
                name: 'Oui',
                value: true
            },
            {
                name: 'Non',
                value: false
            }
        ],
        chiefHostess: [
            {
                name: 'Oui',
                value: true
            },
            {
                name: 'Non',
                value: false
            }
        ],
        stageManager: [
            {
                name: 'Oui',
                value: true
            },
            {
                name: 'Non',
                value: false
            }
        ],
        freelance: [
            {
                name: 'Oui',
                value: true
            },
            {
                name: 'Non',
                value: false
            }
        ],
        active: [
            {
                name: 'Oui',
                value: true
            },
            {
                name: 'Non',
                value: false
            }
        ],
        validate: [
            {
                name: 'Oui',
                value: true
            },
            {
                name: 'Non',
                value: false
            }
        ],
        vehicle: [
            {
                name: 'Oui',
                value: true
            },
            {
                name: 'Non',
                value: false
            }
        ],
        talents: [
            {
                name: 'Bafa',
                value: 'Bafa'
            },
            {
                name: 'Danse',
                value: 'Danse'
            },
            {
                name: 'Rollers',
                value: 'Rollers'
            },
            {
                name: 'Segway',
                value: 'Segway'
            },
            {
                name: 'Vélo',
                value: 'Vélo'
            }
        ],
        shoeSize: [
            {
                name: '36',
                value: 36
            },
            {
                name: '37',
                value: 37
            },
            {
                name: '38',
                value: 38
            },
            {
                name: '39',
                value: 39
            },
            {
                name: '40',
                value: 40
            },
            {
                name: '41',
                value: 41
            },
            {
                name: '42',
                value: 42
            },
            {
                name: '43',
                value: 43
            },
            {
                name: '44',
                value: 44
            },
            {
                name: '45',
                value: 45
            }
        ],
        makingUp: [
            {
                name: 'XS',
                value: 'XS'
            },
            {
                name: 'S',
                value: 'S'
            },
            {
                name: 'M',
                value: 'M'
            },
            {
                name: 'L',
                value: 'L'
            },
            {
                name: 'XL',
                value: 'XL'
            }
        ],
        makingLow: [
            {
                name: '34',
                value: 34
            },
            {
                name: '36',
                value: 36
            },
            {
                name: '38',
                value: 38
            },
            {
                name: '40',
                value: 40
            },
            {
                name: '42',
                value: 42
            },
            {
                name: '44',
                value: 44
            }
        ],
        hairColor: [
            {
                name: 'Blonds',
                value: 'Blonds'
            },
            {
                name: 'Bruns',
                value: 'Bruns'
            },
            {
                name: 'Chatains',
                value: 'Chatains'
            },
            {
                name: 'Roux',
                value: 'Roux'
            },
            {
                name: 'Autres',
                value: 'Autres'
            },
        ],
        heights: [
            {
                name: '-160 cm',
                value: '-160 cm'
            },
            {
                name: '160 - 165 cm',
                value: '160 - 165 cm'
            },
            {
                name: '165 - 170 cm',
                value: '165 - 170 cm'
            },
            {
                name: '170 - 175 cm',
                value: '170 - 175 cm'
            },
            {
                name: '175 - 180 cm',
                value: '175 - 180 cm'
            },
            {
                name: '+180 cm',
                value: '+180 cm'
            },
        ],
        state: [{"departement_code":1,"departement_nom":"Ain"},{"departement_code":2,"departement_nom":"Aisne"},{"departement_code":3,"departement_nom":"Allier"},{"departement_code":5,"departement_nom":"Hautes-Alpes"},{"departement_code":4,"departement_nom":"Alpes-de-Haute-Provence"},{"departement_code":6,"departement_nom":"Alpes-Maritimes"},{"departement_code":7,"departement_nom":"Ard\u00e8che"},{"departement_code":8,"departement_nom":"Ardennes"},{"departement_code":9,"departement_nom":"Ari\u00e8ge"},{"departement_code":10,"departement_nom":"Aube"},{"departement_code":11,"departement_nom":"Aude"},{"departement_code":12,"departement_nom":"Aveyron"},{"departement_code":13,"departement_nom":"Bouches-du-Rh\u00f4ne"},{"departement_code":14,"departement_nom":"Calvados"},{"departement_code":15,"departement_nom":"Cantal"},{"departement_code":16,"departement_nom":"Charente"},{"departement_code":17,"departement_nom":"Charente-Maritime"},{"departement_code":18,"departement_nom":"Cher"},{"departement_code":19,"departement_nom":"Corr\u00e8ze"},{"departement_code":"2a","departement_nom":"Corse-du-sud"},{"departement_code":"2b","departement_nom":"Haute-corse"},{"departement_code":21,"departement_nom":"C\u00f4te-d'or"},{"departement_code":22,"departement_nom":"C\u00f4tes-d'armor"},{"departement_code":23,"departement_nom":"Creuse"},{"departement_code":24,"departement_nom":"Dordogne"},{"departement_code":25,"departement_nom":"Doubs"},{"departement_code":26,"departement_nom":"Dr\u00f4me"},{"departement_code":27,"departement_nom":"Eure"},{"departement_code":28,"departement_nom":"Eure-et-Loir"},{"departement_code":29,"departement_nom":"Finist\u00e8re"},{"departement_code":30,"departement_nom":"Gard"},{"departement_code":31,"departement_nom":"Haute-Garonne"},{"departement_code":32,"departement_nom":"Gers"},{"departement_code":33,"departement_nom":"Gironde"},{"departement_code":34,"departement_nom":"H\u00e9rault"},{"departement_code":35,"departement_nom":"Ile-et-Vilaine"},{"departement_code":36,"departement_nom":"Indre"},{"departement_code":37,"departement_nom":"Indre-et-Loire"},{"departement_code":38,"departement_nom":"Is\u00e8re"},{"departement_code":39,"departement_nom":"Jura"},{"departement_code":40,"departement_nom":"Landes"},{"departement_code":41,"departement_nom":"Loir-et-Cher"},{"departement_code":42,"departement_nom":"Loire"},{"departement_code":43,"departement_nom":"Haute-Loire"},{"departement_code":44,"departement_nom":"Loire-Atlantique"},{"departement_code":45,"departement_nom":"Loiret"},{"departement_code":46,"departement_nom":"Lot"},{"departement_code":47,"departement_nom":"Lot-et-Garonne"},{"departement_code":48,"departement_nom":"Loz\u00e8re"},{"departement_code":49,"departement_nom":"Maine-et-Loire"},{"departement_code":50,"departement_nom":"Manche"},{"departement_code":51,"departement_nom":"Marne"},{"departement_code":52,"departement_nom":"Haute-Marne"},{"departement_code":53,"departement_nom":"Mayenne"},{"departement_code":54,"departement_nom":"Meurthe-et-Moselle"},{"departement_code":55,"departement_nom":"Meuse"},{"departement_code":56,"departement_nom":"Morbihan"},{"departement_code":57,"departement_nom":"Moselle"},{"departement_code":58,"departement_nom":"Ni\u00e8vre"},{"departement_code":59,"departement_nom":"Nord"},{"departement_code":60,"departement_nom":"Oise"},{"departement_code":61,"departement_nom":"Orne"},{"departement_code":62,"departement_nom":"Pas-de-Calais"},{"departement_code":63,"departement_nom":"Puy-de-D\u00f4me"},{"departement_code":64,"departement_nom":"Pyr\u00e9n\u00e9es-Atlantiques"},{"departement_code":65,"departement_nom":"Hautes-Pyr\u00e9n\u00e9es"},{"departement_code":66,"departement_nom":"Pyr\u00e9n\u00e9es-Orientales"},{"departement_code":67,"departement_nom":"Bas-Rhin"},{"departement_code":68,"departement_nom":"Haut-Rhin"},{"departement_code":69,"departement_nom":"Rh\u00f4ne"},{"departement_code":70,"departement_nom":"Haute-Sa\u00f4ne"},{"departement_code":71,"departement_nom":"Sa\u00f4ne-et-Loire"},{"departement_code":72,"departement_nom":"Sarthe"},{"departement_code":73,"departement_nom":"Savoie"},{"departement_code":74,"departement_nom":"Haute-Savoie"},{"departement_code":75,"departement_nom":"Paris"},{"departement_code":76,"departement_nom":"Seine-Maritime"},{"departement_code":77,"departement_nom":"Seine-et-Marne"},{"departement_code":78,"departement_nom":"Yvelines"},{"departement_code":79,"departement_nom":"Deux-S\u00e8vres"},{"departement_code":80,"departement_nom":"Somme"},{"departement_code":81,"departement_nom":"Tarn"},{"departement_code":82,"departement_nom":"Tarn-et-Garonne"},{"departement_code":83,"departement_nom":"Var"},{"departement_code":84,"departement_nom":"Vaucluse"},{"departement_code":85,"departement_nom":"Vend\u00e9e"},{"departement_code":86,"departement_nom":"Vienne"},{"departement_code":87,"departement_nom":"Haute-Vienne"},{"departement_code":88,"departement_nom":"Vosges"},{"departement_code":89,"departement_nom":"Yonne"},{"departement_code":90,"departement_nom":"Territoire de Belfort"},{"departement_code":91,"departement_nom":"Essonne"},{"departement_code":92,"departement_nom":"Hauts-de-Seine"},{"departement_code":93,"departement_nom":"Seine-Saint-Denis"},{"departement_code":94,"departement_nom":"Val-de-Marne"},{"departement_code":95,"departement_nom":"Val-d'oise"},{"departement_code":976,"departement_nom":"Mayotte"},{"departement_code":971,"departement_nom":"Guadeloupe"},{"departement_code":973,"departement_nom":"Guyane"},{"departement_code":972,"departement_nom":"Martinique"},{"departement_code":974,"departement_nom":"Réunion"}],
        dptTranslation : {"Ain": 1, "Aisne": 2, "Allier": 3, "Hautes-Alpes": 5, "Alpes-de-Haute-Provence": 4, "Alpes-Maritimes": 6, "Ardèche": 7, "Ardennes": 8, "Ariège": 9, "Aube": 10, "Aude": 11, "Aveyron": 12, "Bouches-du-Rhône": 13, "Calvados": 14, "Cantal": 15, "Charente": 16, "Charente-Maritime": 17, "Cher": 18, "Corrèze": 19, "Corse-du-sud": "2a", "Haute-corse": "2b", "Côte-d'or": 21, "Côtes-d'armor": 22, "Creuse": 23, "Dordogne": 24, "Doubs": 25, "Drôme": 26, "Eure": 27, "Eure-et-Loir": 28, "Finistère": 29, "Gard": 30, "Haute-Garonne": 31, "Gers": 32, "Gironde": 33, "Hérault": 34, "Ile-et-Vilaine": 35, "Indre": 36, "Indre-et-Loire": 37, "Isère": 38, "Jura": 39, "Landes": 40, "Loir-et-Cher": 41, "Loire": 42, "Haute-Loire": 43, "Loire-Atlantique": 44, "Loiret": 45, "Lot": 46, "Lot-et-Garonne": 47, "Lozère": 48, "Maine-et-Loire": 49, "Manche": 50, "Marne": 51, "Haute-Marne": 52, "Mayenne": 53, "Meurthe-et-Moselle": 54, "Meuse": 55, "Morbihan": 56, "Moselle": 57, "Nièvre": 58, "Nord": 59, "Oise": 60, "Orne": 61, "Pas-de-Calais": 62, "Puy-de-Dôme": 63, "Pyrénées-Atlantiques": 64, "Hautes-Pyrénées": 65, "Pyrénées-Orientales": 66, "Bas-Rhin": 67, "Haut-Rhin": 68, "Rhône": 69, "Haute-Saône": 70, "Saône-et-Loire": 71, "Sarthe": 72, "Savoie": 73, "Haute-Savoie": 74, "Paris": 75, "Seine-Maritime": 76, "Seine-et-Marne": 77, "Yvelines": 78, "Deux-Sèvres": 79, "Somme": 80, "Tarn": 81, "Tarn-et-Garonne": 82, "Var": 83, "Vaucluse": 84, "Vendée": 85, "Vienne": 86, "Haute-Vienne": 87, "Vosges": 88, "Yonne": 89, "Territoire de Belfort": 90, "Essonne": 91, "Hauts-de-Seine": 92, "Seine-Saint-Denis": 93, "Val-de-Marne": 94, "Val-d'oise": 95, "Mayotte": 976, "Guadeloupe": 971, "Guyane": 973, "Martinique": 972, "Réunion": 974}
    };

    //Check if element is an array
    $rootScope.isArray = (array) => {
        return Array.isArray(array);
    };

    //Remove template cache
    $templateCache.removeAll();
};

/**
 * $inject
 * @desc Inject dependencies
 * @return {array}
 */
run.$inject = [
    '$rootScope',
    '$mdDialog',
    'SidenavService',
    '$location',
    '$templateCache'
];

/**
 * @module app.run
 */
angular
    .module('app.run', [])
    .run(run);
