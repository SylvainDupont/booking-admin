/**
 * config
 * App global configuration
 * @param {object} $mdThemingProvider - service from angular-material
 * @param {object} $mdDateLocaleProvider - service from angular-material
 * @param {object} $locationProvider - service from angular-core
 * @param {object} $compileProvider - service from angular-core
 */
const config = ($mdThemingProvider, $mdDateLocaleProvider, $locationProvider, $compileProvider) => {
    
    //Disable compile debug info
    $compileProvider.debugInfoEnabled(false);

    //Format the date displayed by datepicker component
    $mdDateLocaleProvider.formatDate = function (date) {
        if (!date) {
            return null;
        }
        return moment(date).format('DD/MM/YYYY');
    };

    $mdDateLocaleProvider.parseDate = function(dateString) {
        var m = moment(dateString, 'DD/MM/YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };

    //Extend blue-grey palette (primary)
    const appThemePrimary = $mdThemingProvider.extendPalette('blue-grey', {
        '500': 'FD3B6D',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': ['50'],
        '100': 'FD3B6D',
        '50': 'ffffff'
    });

    //Extend grey palette (background)
    const appThemeBackground = $mdThemingProvider.extendPalette('grey', {
        '500': 'F6F9FC',
        '600': '2F3437',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': ['50'],
        '50': 'ffffff'
    });

    //Extend pink palette (accent)
    const appThemeAccent = $mdThemingProvider.extendPalette('pink', {
        '500': 'FD3B6D',
        'contrastDefaultColor': 'light',
        // 'contrastDarkColors': ['50'],
        '50': 'ffffff'
    });

    //Define palettes (primary, background, accent)
    $mdThemingProvider.definePalette('appThemePrimary', appThemePrimary);
    $mdThemingProvider.definePalette('appThemeBackground', appThemeBackground);
    $mdThemingProvider.definePalette('appThemeAccent', appThemeAccent);

    //Set palettes to default material theme
    $mdThemingProvider.theme('default')
        .primaryPalette('appThemePrimary', {
            'default': '500',
            'hue-1': '50',
            'hue-2': '100'
        })
        .backgroundPalette('appThemeBackground', {
            'default': '500',
            'hue-1': '50',
            'hue-2': '600'
        })
        .accentPalette('appThemeAccent', {
            'default': '500',
            'hue-1': '50',
        });

    //Enable theme watching
    $mdThemingProvider.alwaysWatchTheme(true);

    //Enable html5mode (without hashbang)
    if(window.history && window.history.pushState) {
        $locationProvider.html5Mode(true);
    }
};

/**
 * $inject
 * @desc Inject dependencies
 * @return {array}
 */
config.$inject = [
    '$mdThemingProvider', 
    '$mdDateLocaleProvider', 
    '$locationProvider', 
    '$compileProvider'
];

/**
 * @module app.config
 */
angular
    .module('app.config', [])
    .config(config);

