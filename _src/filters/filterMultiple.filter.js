/**
 * filterMultiple
 * @desc filter for ng-repeat, use this for search multiple query on list
 * @return {array} items
 */
const filterMultiple = () => {
    return (items, searchArray, key) => {
        if(items !== undefined && searchArray !== undefined && searchArray.length > 0 && key) {

            let tempItems = [];

            items.forEach((item) => {
                searchArray.forEach((i) => {
                    if(item[key]) {
                        item[key].forEach((m) => {
                            if (angular.equals(m, i) && !tempItems.includes(item)) {
                                tempItems.push(item);
                            }
                        });
                    }
                });
            });
            
            return tempItems;
        } else {
            return items;
        }
    };
};

angular
    .module('app')
    .filter('filterMultiple', filterMultiple);