const swiper = ($timeout) => {
	const link = (scope, element, attrs) => {
		$timeout(() => {
			let swiper = new Swiper('.swiper-container', {
		        direction: 'horizontal',
		        slidesPerView: 1,
		        spaceBetween: 0,
		        nextButton: (scope.nextButton) ? '.swiper-button-next' : false,
        		prevButton: (scope.prevButton) ? '.swiper-button-prev' : false,
        		keyboardControl: true,
        		initialSlide: scope.selected,
		    });
		});
	};

	const directive = {
		restrict: 'EA',
		link: link,
		scope: {
			swipeTo: '&',
			pictures: '=',
			nextButton: '=',
			prevButton: '=',
			selected: '=',
			uid: '='
		},
		replace: true,
		templateUrl: '../components/swiper/swiper.html'
	};

	return directive;
};

swiper.$inject = [
	'$timeout'
];

angular
	.module('app')
	.directive('swiper', swiper);
