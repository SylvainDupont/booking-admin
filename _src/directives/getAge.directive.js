/**
 * getAge
 * @desc Directive for generate user age with date
 */
const getAge = () => {
	const link = (scope, element, attrs) => {
		scope.getAge = moment().diff(attrs.getAge, 'years');
	};

	const directive = {
		link: link,
		restrict: 'EA',
		transclude: true,
		template: '{{getAge}} ans'
	};

	return directive;
};

angular
	.module('app.ageDirective', [])
	.directive('getAge', getAge);