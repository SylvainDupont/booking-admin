/**
 * firebaseStorage
 * @desc Directive for manage firebase storage service
 * @param {object} $q - service from angular-core
 * @param {object} $timeout - service from angular-core
 * @param {object} $compile - service from angular-core
 * @return {object} directive
 */
const firebaseStorage = ($q, $timeout, $compile) => {

	//Build directive on element
	const link = (scope, element, attrs) => {

		//get firebase storage instance
			const storage = firebase.storage();

		//Get element
			element = element[0];

		//If opacity
			if(attrs.opacity) element.style.opacity = 0;
		if(attrs.opacity) element.style.transition = 'opacity .3s ease';

		//Download pictures from firebase storage
			const downloadUrl = () => {

			//Wait DOM is loaded
				$timeout(() => {

					if(!firebase) return;
					if(!scope.bucketName) return;
					if(!scope.uid) return;

					//Get user firebase bucket reference
					const userPicture = storage.ref(scope.bucketName).child(scope.uid);

					//Get name in background or image attribute
					const imageName = scope.background || scope.image || scope.video;
					if(scope.ftype == 'image' || !scope.ftype){
						if(imageName && typeof imageName !== 'string') return;
						if(imageName === undefined) return;
						//Download URL
						userPicture.child(imageName).getDownloadURL().then((url) => {
							if(scope.background) {
									//Set background
									element.style.background = 'url(' + url + ') no-repeat center center / cover';
							}else {
									//Set source
									element.src = url;
							}
							//If opacity display element with opacity animation
							if(attrs.opacity) element.style.opacity = 1;
						});
					}
					else if(scope.ftype == 'video'){
						if(imageName && typeof imageName !== 'string') return;
						if(imageName === undefined) return;
						//Download URL
						userPicture.child(imageName).getDownloadURL().then((url) => {
							element.innerHTML="";
							let video = document.createElement('video');
							video.autoplay = false;
							video.controls = true;
							video.playsinline = true;
							let source = document.createElement('source');
							source.src = url;
							source.type = "video/mp4";
							video.appendChild(source);
							element.appendChild(video);
							// element.firstElementChild.src = url;
						});
					}
				});
			};

		//Watch image attribute for change url
			scope.$watch('image', () => {
				downloadUrl();
			});

		//Watch background attribute for change url
			scope.$watch('background', () => {
				downloadUrl();
			});
	};

	//Directive configuration
	const directive = {
		restrict: 'EA',
		scope: {
			uid: '=uid',
			bucketName: '@bucketName',
			background: '=',
			ftype: '@ftype',
			image: '=',
			video: '='
		},
		link: link
	};

	return directive;
};

/**
 * $inject
 * @desc Inject dependencies
 * @return {Array} dependencies
 */
firebaseStorage.$inject = [
	'$q',
	'$timeout',
	'$compile'
];

angular
	.module('app')
	.directive('firebaseStorage', firebaseStorage);
