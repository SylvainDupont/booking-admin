<?php

	namespace Mail;

	class MailManager
	{
		public $from;
		public $to;
		public $subject;
		public $body;
		public $headers;
		public $reply;
		public $defaultHeader = false;
		public $response = [];

		public function __construct (Array $data) 
		{
			$this->from           = $data['from'];
			$this->to             = $data['to'];
			$this->subject        = $data['subject'];
			$this->body           = $data['body'];
			$this->defaultHeaders = $data['default_headers'];
			$this->replay         = $data['reply'];

			if($this->defaultHeaders) $this->setDefaultHeaders();

			$response = [
				'status' => false
			];
		}

		public function setDefaultHeaders () 
		{
			$headers  = "MIME-Version: 1.0 \r\n";
			$headers .= "Content-type: text/html; charset=\"UTF-8\" r\n";
			$headers .= "From: Booking Hôtesses <". $this->from ."> \r\n";
			$headers .= "Return-Path: Booking Hôtesses <". $this->from ."> \r\n";
			$headers .= "Reply-To: <". $this->reply . "> \r\n";
			$headers .= "X-Priority: 1 \r\n";
			$headers .= "X-MSMail-Priority: High \r\n";
			$headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

			$this->setHeaders($headers);
		}

		public function send () 
		{
			if(mail($this->to, $this->subject, $this->body, $this->headers)) {
				return $response = [
					'status' => true
				];
			}

			return $response;	
		}

		public function getFrom ()
		{
			return $this->from;
		}

		public function getTo ()
		{
			return $this->to;
		}

		public function getSubject ()
		{
			return $this->subject;
		}

		public function getBody ()
		{
			return $this->body;
		}

		public function getHeader ()
		{
			return $this->header;
		}

		public function setFrom ($from)
		{
			$this->from = $from;
		}

		public function setTo ($to)
		{
			$this->to = $to;
		}

		public function setSubject ($subject)
		{
			$this->subject = $subject;
		}

		public function setBody ($body)
		{
			$this->body = $body;
		}

		public function setHeaders ($headers)
		{
			$this->headers = $headers;
		}

	}