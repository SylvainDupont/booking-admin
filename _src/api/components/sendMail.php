<?php
	require_once(__DIR__ . '/SendInBlue/vendor/autoload.php');

	require 'Autoloader.php';

	// use Mail\MailManager;
	error_reporting(E_ALL | E_STRICT);
	ini_set('display_errors', 1);
	ini_set('html_errors', 1);

	$postData = file_get_contents('php://input');
	$request  = json_decode($postData, true);

	if($request)
	{
		if(isset($request['code']) && isset($request['firstname']) && isset($request['mail']))
		{
			switch($request['code'])
			{
				case 'forgot-password':
					forgotPassword($request['mail'], $request['firstname']);
					break;
				case 'welcome':
					welcome($request['mail'], $request['firstname']);
					break;
				case 'invalid-picture':
					invalidPicture($request['mail'], $request['firstname'], $sg);
					break;
				case 'invalid-experience':
					invalidExperience($request['mail'], $request['firstname'], $sg);
					break;
				case 'candidate-inactive':
					candidateInactive($request['mail'], $request['firstname']);
					break;
				case 'candidate-active':
					candidateActive($request['mail'], $request['firstname']);
					break;
				case 'valid-agency':
					agencyValid($request['mail'], $request['firstname']);
					break;
				case 'agency-active':
					agencyValid($request['mail'], $request['firstname']);
					break;
			}
		}
	}

	function forgotPassword ()
	{

	}

	function welcome ()
	{

	}

	function invalidPicture ($to, $name, $sg)
	{
		$mailOptions = new stdClass();
		$mailOptions->candidateName = $name;

		$subject = 'Animation Commerciale | Photos non conformes';
		$body = include 'picture.mail.php';


		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');
		$apiInstance = new SendinBlue\Client\Api\AccountApi(
				new GuzzleHttp\Client(),
				$config
		);

		try {
				$result = $apiInstance->getAccount();
		} catch (Exception $e) {
				echo 'Exception when calling AccountApi->getAccount: ', $e->getMessage(), PHP_EOL;
		}

		$apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
			 new GuzzleHttp\Client(),
			 $config
	  );
		if(isset($name)):
	 		$recipient = ['name' => $name, 'email' => $to];
		else:
			$recipient = ['email' => $to];
		endif;
	  $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
	  $sendSmtpEmail['subject'] = $subject;
	  $sendSmtpEmail['htmlContent'] = $body;
	  $sendSmtpEmail['sender'] = array('name' => 'animationcommerciale.com', 'email' => 'candidat@animationcommerciale.com');
	  $sendSmtpEmail['to'] = [$recipient];
	  $sendSmtpEmail['replyTo'] = array('name' => 'animationcommerciale.com', 'email' => 'candidat@animationcommerciale.com');
		 try {
				 $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
				 echo json_encode([
		 			'status' => true
		 		]);
		 } catch (Exception $e) {
				 echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
		 }
	}

	function invalidExperience ($to, $name, $sg)
	{
		$mailOptions = new stdClass();
		$mailOptions->candidateName = $name;

		$subject = 'Animation Commerciale | Experiences non conformes';
		$body = include 'experience.mail.php';

		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');
		$apiInstance = new SendinBlue\Client\Api\AccountApi(
				new GuzzleHttp\Client(),
				$config
		);

		try {
				$result = $apiInstance->getAccount();
		} catch (Exception $e) {
				echo 'Exception when calling AccountApi->getAccount: ', $e->getMessage(), PHP_EOL;
		}

		$apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
			 new GuzzleHttp\Client(),
			 $config
	  );
		if(isset($name)):
	 		$recipient = ['name' => $name, 'email' => $to];
		else:
			$recipient = ['email' => $to];
		endif;
	  $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
	  $sendSmtpEmail['subject'] = $subject;
	  $sendSmtpEmail['htmlContent'] = $body;
	  $sendSmtpEmail['sender'] = array('name' => 'animationcommerciale.com', 'email' => 'candidat@animationcommerciale.com');
	  $sendSmtpEmail['to'] = [$recipient];
	  $sendSmtpEmail['replyTo'] = array('name' => 'animationcommerciale.com', 'email' => 'candidat@animationcommerciale.com');
		 try {
				 $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
				 echo json_encode([
		 			'status' => true
		 		]);
		 } catch (Exception $e) {
				 echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
		 }
	}

	function candidateInactive ($to, $name)
	{
	}

	function candidateActive ($to, $name)
	{
		$subject = 'ANIMATIONCOMMERCIALE.COM | Profil validé';
		$body = <<<HTML
				<!doctype html public "- / /w3c / /dtd xhtml 1.0 transitional / /en" "http: /
				/www.w3.org /tr /xhtml1 /dtd /xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
				  		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				  		<!--[if !mso]><!-->
				  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
				  		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" type="text/css">
				  		<!--<![endif]-->
				  		<meta name="viewport" content="width=device-width, initial-scale=1.0">
				  		<title></title>
				  		<style type="text/css">
				    		li {
				    			padding-bottom: 30px;
				    		}
				      		li span {
				      			color: #233239; font-size: 18px; line-height: 10px; vertical-align: 5px;
				      		}
				  		</style>
				  		<!--[if (gte mso 9)|(IE)]>
				      	<style type="text/css">
				        	table { border-collapse:collapse; }
				      	</style>
				    	<![endif]-->
					</head>
					<body style="margin:0 !important; font-family: 'Open Sans'; padding:0px 10px; background: #fafafa;background-image: url('https://www.booking-hotesses.com/ressources/mail/pink-email-bg-split_4x400.jpg'); background-repeat: repeat-x; background-position: center top;-webkit-font-smoothing: antialiased;">
					  	<!-- Pre Header Snippet -->
					  	<!-- Explore the great outdoors with Local Guides. -->
					  	<span class="preheader" style="display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;border-collapse: collapse;border: 0px;"></span>

					  	<!-- End of Pre Header -->
					  	<center style="width:100%; table-layout:fixed; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;">
						    <div style="margin:0 auto; max-width:580px;">
						      	<!--[if (gte mso 9)|(IE)]>
						          	<table align="center" width="580" border="0" cellspacing="0" cellpadding="0">
						            <tr>
						            <td>
						        <![endif]-->
						      	<table style="margin:0 auto; width:100%; max-width:580px;" align="center" border="0" cellspacing="0" cellpadding="0">
						        	<tr>
						          		<td align="center">
						            		<tr>
						              			<td style="padding:70px 0px 40px 0; text-align: center; color: white;">
						                			<img src="https://www.booking-hotesses.com/ressources/mail/logo-animationcommercial.png" width="350">
						              			</td>
						            		</tr>
						          		</td>
						        	</tr>
						        	<tr bgcolor="white">
						          		<td style="padding: 0px 5% 5% 5%; box-shadow: 0px 6.29px 11.85px rgba(0, 0, 0, 0.1);">
						            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
						              			<tbody>
						                			<tr>
						                  				<td style="padding-bottom: 30px; padding-top: 35px; font-weight: 600; font-size:20px; line-height:42px; color:#263238;">
						                  					Félicitations ! votre profil a été validé !
						                  				</td>
						                			</tr>
						                			<tr>
						                  				<td align="left" style="padding-bottom:0px; font-weight:normal; font-size:18px; line-height:32px; color:#6f8996;">
						                    				Bonjour $name,
						                    				<br>
						                    				<br>
															Merci pour votre inscription sur le site <a href="https://animationcommerciale.com/" style="color:#FD3B6D;text-decoration:none;">ANIMATIONCOMMERCIALE.COM.</a><br /> Votre profil vient d'être validé par notre équipe.
															<br>
															<br>
															<h4 style="color: #f8386c; margin:0">Et maintenant ?</h4>
															Les recruteurs déposent quotidiennement sur la plateforme des offres de missions d'animation commerciale. Dès qu'une demande est enregistrée et que celle-ci correspond à votre profil et à vos attentes, vous recevez par email la proposition de mission. Vous n'avez plus qu'à accepter ou refuser l’offre. Plus vous serez réactif (ive) et plus grandes seront vos chances d'être sélectionné (e)  surtout en cas de missions urgentes. 
															<br />Une fois votre candidature déposée, le recruteur a 48h pour vous contacter sinon cela signifie que vous n’avez pas été retenu (e) pour la mission.
															<br>
															<br>
															<h4 style="color: #f8386c; margin:0">Conseil</h4>
															N’hésitez pas à mettre régulièrement à jour, votre profil, vos expériences et vos photos. Votre profil en ligne est un véritable CV pour les recruteurs, alors alimentez le au maximum pour vous différencier des autres candidats et vous faire remarquer. 
															<br>
															<br>
															Si vous avez des amis qui recherchent un emploi, n'hésitez pas à leur communiquer notre site internet. 
															<br>
															<br>
															A bientôt !
															<br>
															<br>
															<span style="color: #f8386c; font-weight: 400">L'équipe ANIMATIONCOMMERCIALE.COM</span>
						                    			</td>
						                			</tr>
						                			<tr bgcolor="#FFFFFF">
											          	<td style="padding: 50px 5% 0 5%;">
											            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
											              		<tbody>
											                		<tr>
											                  			<td style="padding-top:0px; padding-bottom:10px;" align="center">
											                    			<table border="0" cellpadding="0" cellspacing="0">
											                      				<tbody>
											                        				<tr>
											                          					<td style="-webkit-border-radius:2px; -moz-border-radius:3px; border-radius:3px; background-color:#f8386c; color:#ffffff !important;" align="center">
											                            					<a href="https://app.candidat.animationcommerciale.com" style="display:block; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; color:#ffffff; text-decoration:none; font-weight:600; font-size:16px; letter-spacing:1px; border-top: 15px solid #f8386c;border-right: 50px solid #f8386c; border-bottom: 15px solid #f8386c;border-left: 50px solid #f8386c;text-transform: uppercase;" target="_blank">
						                              											<!--[if mso]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]-->
						                              											SE CONNECTER
						                              											<!--[if mso]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]-->
						                            										</a>
											                          					</td>
											                        				</tr>
											                      				</tbody>
											                    			</table>
											                  			</td>
											                		</tr>
											              		</tbody>
										            		</table>
											          	</td>
											        </tr>
						              			</tbody>
						            		</table>
						          		</td>
						        	</tr>
						        	<tr>
						          		<td style="padding-top: 50px;padding-left: 10%; padding-right: 10%">
						            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
						              			<tbody>
						                			<tr>
						                  				<td align="center" valign="top">
						                    				<a style="text-decoration: none; color: #03a9f4;" href="https://www.facebook.com/Booking-H%C3%B4tesses-626300620844110/?fref=ts" style="text-decoration: none;">
						                      					<img style="display:block; height:auto;" src="https://www.booking-hotesses.com/ressources/mail/social-icon-01.png" alt="" width="50%" border="0" class="">
						                   		 			</a>
						                  				</td>
						                  				<td align="center" valign="top">
						                    				<a style="text-decoration: none; color: #03a9f4;" href="https://twitter.com/BookingHotesses" style="text-decoration: none;">
						                      					<img style="display:block; height:auto;" src="https://www.booking-hotesses.com/ressources/mail/social-icon-02.png" alt="" width="50%" border="0" class="">
						                   		 			</a>
						                  				</td>
						                  				<td align="center" valign="top">
						                    				<a style="text-decoration: none; color: #03a9f4;" href="https://www.instagram.com/bookinghotesses/" style="text-decoration: none;">
						                      					<img style="display:block; height:auto;" src="https://www.booking-hotesses.com/ressources/mail/social-icon-03.png" alt="" width="50%" border="0" class="">
						                    				</a>
						                  				</td>
						                			</tr>
						              			</tbody>
						            		</table>
						          		</td>
						        	</tr>
						        	<tr>
						          		<td align="center" style="padding:0px 0px 10px 0; font-weight:normal; font-size:14px; line-height:24px; color:#9fb1c1;">
						            		<!-- <img src="http://www.gstatic.com/local/guides/email/images/base/2016-04_google-logo_gray_384x127.png" width="120" style="padding-bottom: 20px;"> -->
					            			<br>© ANIMATIONCOMMERCIALE.COM. ‌
						            		<br>13 avenue Lou Hemmer
														<br>L5627 Mondorf Les Bains
														<br>Luxembourg
																				           	</td>
						        	</tr>
						      	</table>
						      	<!--[if (gte mso 9)|(IE)]>
					          		</td>
					            	</tr>
					          		</table>
						        <![endif]-->
						    </div>
					  	</center>
					  	<img height="1" width="3" src="https://www.google.com/appserve/mkt/img/v7GNpIDNuXrB6l3wIlY1NoD4tpQ=.gif">
					</body>
				</html>
HTML;


		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');
		$apiInstance = new SendinBlue\Client\Api\AccountApi(
				new GuzzleHttp\Client(),
				$config
		);

		try {
				$result = $apiInstance->getAccount();
		} catch (Exception $e) {
				echo 'Exception when calling AccountApi->getAccount: ', $e->getMessage(), PHP_EOL;
		}

		$apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
			 new GuzzleHttp\Client(),
			 $config
	  );
		if(isset($name)):
	 		$recipient = ['name' => $name, 'email' => $to];
		else:
			$recipient = ['email' => $to];
		endif;
	  $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
	  $sendSmtpEmail['subject'] = $subject;
	  $sendSmtpEmail['htmlContent'] = $body;
	  $sendSmtpEmail['sender'] = array('name' => 'animationcommerciale.com', 'email' => 'candidat@animationcommerciale.com');
	  $sendSmtpEmail['to'] = [$recipient];
	  $sendSmtpEmail['replyTo'] = array('name' => 'animationcommerciale.com', 'email' => 'candidat@animationcommerciale.com');
		 try {
				 $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
				 echo json_encode([
		 			'status' => true
		 		]);
		 } catch (Exception $e) {
				 echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
		 }

	}

	function agencyValid ($to, $name)
	{
		$subject = 'Votre profil validé';
		$body = <<<HTML
				<!doctype html public "- / /w3c / /dtd xhtml 1.0 transitional / /en" "http: /
				/www.w3.org /tr /xhtml1 /dtd /xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
				  		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				  		<!--[if !mso]><!-->
				  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
				  		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" type="text/css">
				  		<!--<![endif]-->
				  		<meta name="viewport" content="width=device-width, initial-scale=1.0">
				  		<title></title>
				  		<style type="text/css">
				    		li {
				    			padding-bottom: 30px;
				    		}
				      		li span {
				      			color: #233239; font-size: 18px; line-height: 10px; vertical-align: 5px;
				      		}
				  		</style>
				  		<!--[if (gte mso 9)|(IE)]>
				      	<style type="text/css">
				        	table { border-collapse:collapse; }
				      	</style>
				    	<![endif]-->
					</head>
					<body style="margin:0 !important; font-family: 'Open Sans'; padding:0px 10px; background: #fafafa;background-image: url('https://www.booking-hotesses.com/ressources/mail/pink-email-bg-split_4x400.jpg'); background-repeat: repeat-x; background-position: center top;-webkit-font-smoothing: antialiased;">
					  	<!-- Pre Header Snippet -->
					  	<!-- Explore the great outdoors with Local Guides. -->
					  	<span class="preheader" style="display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;border-collapse: collapse;border: 0px;"></span>

					  	<!-- End of Pre Header -->
					  	<center style="width:100%; table-layout:fixed; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;">
						    <div style="margin:0 auto; max-width:580px;">

						      	<table style="margin:0 auto; width:100%; max-width:580px;" align="center" border="0" cellspacing="0" cellpadding="0">

						        	<tr bgcolor="white">
						          		<td style="padding: 0px 5% 5% 5%; box-shadow: 0px 6.29px 11.85px rgba(0, 0, 0, 0.1);">
						            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
						              			<tbody>
						                			<tr>
						                  				<td style="padding-bottom: 30px; padding-top: 35px; font-weight: 600; font-size:20px; line-height:42px; color:#263238;">
						                  					Félicitations, l'inscription de votre entreprise a été validée !
						                  				</td>
						                			</tr>
						                			<tr>
						                  				<td align="left" style="padding-bottom:0px; font-weight:normal; font-size:18px; line-height:32px; color:#6f8996;">
						                    				Bonjour $name,
						                    				<br>
						                    				<br>
																				L'équipe tient à vous remercier pour l'intérêt que vous portez à notre site de recrutement ANIMATIONCOMMERCIALE.COM. Nous nous sommes promis de révolutionner le recrutement du métier d'animation commerciale en abandonnant l'éternel CV souvent incomplet et rarement à jour afin de privilégier une approche par métier et par compétence. Le système de Matching géolocalisé que nous avons créé vous permet d'affiner votre recherche de profils en sélectionnant différents critères tels que le métier, la marque, le produit, l'enseigne, l'expérience et l'adresse du point d'animation. <br>
																				Notre algorithme propose votre mission en quelques secondes aux candidats sélectionnés et vous recevez par retour les candidatures en temps réel.<br>
																				En cas d'urgence due à un désistement sur un poste, notre offre SOS Backup vous permet de joindre très rapidement les candidats pour remplacer au pied levé la personne absente. Nous vous offrons une période d'essai de 7 jours afin que vous puissiez tester notre solution en temps réel.
																				<br> Nawell, notre chargée de clientèle, reste à votre disposition pour vous faire une démonstration de notre outil et répondre à l'ensemble de vos questions. <br>
															<br>
															<br>
															<span style="color: #f8386c; font-weight: 400">ANIMATIONCOMMERCIALE.COM</span>
															<p style="font-size:11px">
															13 avenue Lou Hemmer<br />
															L5627 Mondorf Les Bains<br />
															Luxembourg<br />
															</p>
						                    			</td>
						                			</tr>

						              			</tbody>
						            		</table>
						          		</td>
						        	</tr>
						      	</table>
						    </div>
					  	</center>
					  	<img height="1" width="3" src="https://www.google.com/appserve/mkt/img/v7GNpIDNuXrB6l3wIlY1NoD4tpQ=.gif">
					</body>
				</html>
HTML;

		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');
		$apiInstance = new SendinBlue\Client\Api\AccountApi(
				new GuzzleHttp\Client(),
				$config
		);

		try {
				$result = $apiInstance->getAccount();
		} catch (Exception $e) {
				echo 'Exception when calling AccountApi->getAccount: ', $e->getMessage(), PHP_EOL;
		}

		$apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
			 new GuzzleHttp\Client(),
			 $config
	  );
		if(isset($name)):
	 		$recipient = ['name' => $name, 'email' => $to];
		else:
			$recipient = ['email' => $to];
		endif;
	  $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
	  $sendSmtpEmail['subject'] = $subject;
	  $sendSmtpEmail['htmlContent'] = $body;
	  $sendSmtpEmail['sender'] = array('name' => 'animationcommerciale.com', 'email' => 'candidat@animationcommerciale.com');
	  $sendSmtpEmail['to'] = [$recipient];
	  $sendSmtpEmail['replyTo'] = array('name' => 'animationcommerciale.com', 'email' => 'candidat@animationcommerciale.com');
		 try {
				 $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
				 echo json_encode([
		 			'status' => true
		 		]);
		 } catch (Exception $e) {
				 echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
		 }
	}
