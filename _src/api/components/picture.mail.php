<?php 

    $body = <<<HTML
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang="en">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <base href="">
                <!--[if !mso]><!-->
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <!--<![endif]-->
                <title></title>
                <style type="text/css">
                    /* cyrillic-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 300;
                    src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTQ7aC6SjiAOpAWOKfJDfVRY.woff2) format('woff2');
                    unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
                    }
                    /* cyrillic */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 300;
                    src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTRdwxCXfZpKo5kWAx_74bHs.woff2) format('woff2');
                    unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
                    }
                    /* greek-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 300;
                    src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTZ6vnaPZw6nYDxM4SVEMFKg.woff2) format('woff2');
                    unicode-range: U+1F00-1FFF;
                    }
                    /* greek */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 300;
                    src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTfy1_HTwRwgtl1cPga3Fy3Y.woff2) format('woff2');
                    unicode-range: U+0370-03FF;
                    }
                    /* vietnamese */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 300;
                    src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTfgrLsWo7Jk1KvZser0olKY.woff2) format('woff2');
                    unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
                    }
                    /* latin-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 300;
                    src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTYjoYw3YTyktCCer_ilOlhE.woff2) format('woff2');
                    unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
                    }
                    /* latin */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 300;
                    src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTRampu5_7CjHW5spxoeN3Vs.woff2) format('woff2');
                    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
                    }
                    /* cyrillic-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 400;
                    src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/K88pR3goAWT7BTt32Z01m4X0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
                    unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
                    }
                    /* cyrillic */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 400;
                    src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/RjgO7rYTmqiVp7vzi-Q5UYX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
                    unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
                    }
                    /* greek-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 400;
                    src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/LWCjsQkB6EMdfHrEVqA1KYX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
                    unicode-range: U+1F00-1FFF;
                    }
                    /* greek */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 400;
                    src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/xozscpT2726on7jbcb_pAoX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
                    unicode-range: U+0370-03FF;
                    }
                    /* vietnamese */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 400;
                    src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/59ZRklaO5bWGqF5A9baEEYX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
                    unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
                    }
                    /* latin-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 400;
                    src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/u-WUoqrET9fUeobQW7jkRYX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
                    unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
                    }
                    /* latin */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 400;
                    src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3ZBw1xU1rKptJj_0jans920.woff2) format('woff2');
                    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
                    }
                    /* cyrillic-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 600;
                    src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSg7aC6SjiAOpAWOKfJDfVRY.woff2) format('woff2');
                    unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
                    }
                    /* cyrillic */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 600;
                    src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNShdwxCXfZpKo5kWAx_74bHs.woff2) format('woff2');
                    unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
                    }
                    /* greek-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 600;
                    src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSp6vnaPZw6nYDxM4SVEMFKg.woff2) format('woff2');
                    unicode-range: U+1F00-1FFF;
                    }
                    /* greek */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 600;
                    src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSvy1_HTwRwgtl1cPga3Fy3Y.woff2) format('woff2');
                    unicode-range: U+0370-03FF;
                    }
                    /* vietnamese */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 600;
                    src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSvgrLsWo7Jk1KvZser0olKY.woff2) format('woff2');
                    unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
                    }
                    /* latin-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 600;
                    src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSojoYw3YTyktCCer_ilOlhE.woff2) format('woff2');
                    unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
                    }
                    /* latin */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 600;
                    src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNShampu5_7CjHW5spxoeN3Vs.woff2) format('woff2');
                    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
                    }
                    /* cyrillic-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 700;
                    src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzA7aC6SjiAOpAWOKfJDfVRY.woff2) format('woff2');
                    unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
                    }
                    /* cyrillic */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 700;
                    src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzBdwxCXfZpKo5kWAx_74bHs.woff2) format('woff2');
                    unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
                    }
                    /* greek-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 700;
                    src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzJ6vnaPZw6nYDxM4SVEMFKg.woff2) format('woff2');
                    unicode-range: U+1F00-1FFF;
                    }
                    /* greek */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 700;
                    src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzPy1_HTwRwgtl1cPga3Fy3Y.woff2) format('woff2');
                    unicode-range: U+0370-03FF;
                    }
                    /* vietnamese */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 700;
                    src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzPgrLsWo7Jk1KvZser0olKY.woff2) format('woff2');
                    unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
                    }
                    /* latin-ext */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 700;
                    src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzIjoYw3YTyktCCer_ilOlhE.woff2) format('woff2');
                    unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
                    }
                    /* latin */
                    @font-face {
                    font-family: 'Open Sans';
                    font-style: normal;
                    font-weight: 700;
                    src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzBampu5_7CjHW5spxoeN3Vs.woff2) format('woff2');
                    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
                    }
                </style>
                <style type="text/css">


                    @media screen and (max-width: 567px) {

                        .headerContent .brandContainer img {
                            width: 70px;
                            margin: 0 auto;
                            display: block;
                        }

                        .headerContent .brandContainer h1 {
                            font-size: 30px;
                            display: block;
                        }

                        .mainContent .layoutContainer {
                            margin-bottom: 5px;
                        }

                        .mainContent .layoutContainer .columnContainer {
                            width: 100%!important;
                            display: block!important;
                        }

                        .mainContent .layoutContainer .col-4 {
                            width: 100%!important;
                            display: block!important;
                        }

                        .buttonContainerInner {
                            padding: 5px 0!important;
                        }
                    }

                    @media screen and (min-width: 568px) {

                        .bannerContent {
                            width: 100%;
                        }

                        .bannerContent td {
                            padding: 10px 15px;
                        }

                        .headerContent {
                            width: 100%;
                        }

                        .headerContent h1 {
                            font-size: 35px;  
                            padding: 10px 15px;
                        }

                        .mainContent {
                            width: 100%;
                        }

                        .mainContentInner {
                            padding: 25px;
                        }

                        .mainContent .layoutContainer .columnContainer {
                            width: 50%!important;
                            display: table-cell!important;
                        }

                        .mainContent .layoutContainer .col-4 {
                            width: 25%!important;
                            display: table-cell!important;
                        }

                        .buttonContainerInner {
                            padding: 5px 25%!important;
                        }
                    }

                    @media screen and (min-width: 768px) {

                        .bannerContent {
                            width: 100%;
                        }

                        .bannerContent td {
                            padding: 10px 25px;
                        }

                        .headerContent {
                            width: 100%;
                        }

                        .headerContent h1 {
                            font-size: 35px;  
                            padding: 10px 25px;
                        }

                        .mainContent {
                            width: 100%;
                        }

                        .mainContentInner {
                            padding: 25px;
                        }

                        .mainContent .layoutContainer .columnContainer {
                            width: 50%!important;
                            display: table-cell!important;
                        }

                        .mainContent .layoutContainer .col-4 {
                            width: 25%!important;
                            display: table-cell!important;
                        }

                        .buttonContainerInner {
                            padding: 5px 30%!important;
                        }

                        .socialContainer {
                            width: 100%;
                        }

                        .copyrightContainer {
                            width: 100%;
                        }
                    }

                    @media screen and (min-width: 992px) {

                        .bannerContent {
                            width:  80%;
                        }

                        .bannerContent td {
                            padding: 10px 25px;
                        }

                        .headerContent {
                            width: 80%;
                        }

                        .headerContent h1 {
                            font-size: 40px;  
                            padding: 10px 25px;
                        }

                        .mainContent {
                            width: 80%;
                        }

                        .mainContentInner {
                            padding: 25px;
                        }

                        .mainContent .layoutContainer .columnContainer {
                            width: 50%!important;
                            display: table-cell!important;
                        }

                        .mainContent .layoutContainer .col-4 {
                            width: 25%!important;
                            display: table-cell!important;
                        }

                        .buttonContainerInner {
                            padding: 5px 30%!important;
                        }

                        .socialContainer {
                            width: 80%;
                        }

                        .copyrightContainer {
                            width: 80%;
                        }
                    }

                    @media screen and (min-width: 1200px) {
                        
                        .bannerContent {
                            width: 60%;
                        }

                        .bannerContent td {
                            padding: 10px 25px;
                        }

                        .headerContent {
                            width: 60%;
                        }

                        .headerContent h1 {
                            font-size: 45px;  
                            padding: 10px 25px;
                        }

                        .mainContent {
                            width: 60%;
                        }

                        .mainContentInner {
                            padding: 25px;
                        }

                        .mainContent .layoutContainer .columnContainer {
                            width: 50%!important;
                            display: table-cell!important;
                        }

                        .mainContent .layoutContainer .col-4 {
                            width: 25%!important;
                            display: table-cell!important;
                        }

                        .buttonContainerInner {
                            padding: 5px 30%!important;
                        }

                        .socialContainer {
                            width: 60%;
                        }

                        .copyrightContainer {
                            width: 60%;
                        }
                    }

                    @media screen and (min-width: 1480px) {

                        .bannerContent {
                            width: 50%;
                            padding: 5px 25px;
                        }

                        .headerContent {
                            width: 50%;
                        }

                        .headerContent h1 {
                            font-size: 40px;  
                            padding: 10px 25px;
                        }

                        .mainContent {
                            width: 50%;
                        }

                        .mainContentInner {
                            padding: 25px;
                        }

                        .mainContent .layoutContainer .columnContainer {
                            width: 50%!important;
                            display: table-cell!important;
                        }

                        .mainContent .layoutContainer .col-4 {
                            width: 25%!important;
                            display: table-cell!important;
                        }

                        .buttonContainerInner {
                            padding: 5px 30%!important;
                        }

                        .socialContainer {
                            width: 50%;
                        }

                        .copyrightContainer {
                            width: 50%;
                        }
                    }
                </style>
                <!--[if gte mso 9]>
                <style type="text/css">
                    .rps_dd3c .x_mainContent .x_layoutContainer .x_columnContainer {
                        width: 50%!important;
                        display: table-cell!important;
                    }
                </style>
                <![endif]-->
            </head>
            <body style="margin: 0!important; font-family: Open Sans, Sans-Serif; -webkit-font-smoothing: antialiased; padding: 0;">
                <center>
                    <table width="100%" bgcolor="#F6F9FC" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                        <tr>
                            <td align="center" valign="top" style="border-collapse: collapse;">    
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                                    <tr>
                                        <td style="border-collapse: collapse;">
                                            <!--OK-->
                                            <table class="bannerContent" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin: 0 auto; background-color: #a1adb5;">
                                                <tr>
                                                    <td align="center" valign="top" style="border-collapse: collapse;color: white;font-size: 10px;font-weight: 600;opacity: 0.7;padding: 10px 15px;">
                                                        Ceci est un message automatique, merci de ne pas y répondre
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--OK-->
                                            <table class="headerContent" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin: 0 auto; background-color: #b6c3cc;">
                                                <tr>
                                                    <td class="brandContainer" align="center" valign="top" style="border-collapse: collapse;padding-left: 0;width: 100%;padding: 25px;">
                                                        <img src="https://www.booking-hotesses.com/ressources/mail/logo-icon.png" alt="logo booking hôtesses" style="border: 0 none;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline-block;vertical-align: middle;width: 60px;">
                                                        <h1 style="color: white;font-size: 35px;font-weight: 400;padding: 15px;display: inline-block;margin: 0;vertical-align: middle;">Booking Hôtesses</h1>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--OK-->
                                            <table class="mainContent" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin: 0 auto; background-color: white; display: block;">
                                                <tr>
                                                    <td class="mainContentInner" style="border-collapse: collapse; padding: 25px;">
                                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;">
                                                            <tr>
                                                                <td align="left" valign="top" width="100%" style="border-collapse: collapse;padding-left: 0;">
                                                                    <h2 style="font-size: 25px; font-weight: 600; color: #323232; margin: 0;">Merci pour ton inscription !</h2>
                                                                    <p style="color: #828282;font-size: 17px;line-height: 23px;">Bonjour {$mailOptions->candidateName},<br><br>
                                                                        Après vérification de ton profil par notre équipe, il nous est impossible de le valider pour le moment car ta/tes photos ne sont pas conformes.</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;">
                                                            <tr>
                                                                <td align="left" valign="top" style="border-collapse: collapse;padding-left: 0;">
                                                                    <h4 style="font-size: 18px; font-weight: 600; color: #262626; margin: 0;">Qu’est-ce qu’une photo conforme ?</h4>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;margin-bottom: 15px;">
                                                            <tr>
                                                                <td align="left" valign="top" style="border-collapse: collapse;padding-left: 0;">
                                                                    <h5 style="font-size: 17px; font-weight: 600; color: #f8386c; margin: 0 0 10px 0;">Photo portrait</h5>
                                                                    <p style="color: #828282;font-size: 17px;margin: 0;line-height: 23px;">Simple, HD et professionnelle (style CV), pas de selfie</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;margin-bottom: 15px;">
                                                            <tr>
                                                                <td align="left" valign="top" style="border-collapse: collapse;padding-left: 0;">
                                                                    <h5 style="font-size: 17px; font-weight: 600; color: #f8386c; margin: 0 0 10px 0;">Photo plein pied</h5>
                                                                    <p style="color: #828282;font-size: 17px;margin: 0;line-height: 23px;">Pas de photo de groupe, bonne qualité et professionnelle (fond sobre et correctement habillé)</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;margin-bottom: 15px;">
                                                            <tr>
                                                                <td align="left" valign="top" style="border-collapse: collapse;padding-left: 0;">
                                                                    <h5 style="font-size: 17px; font-weight: 600; color: #f8386c; margin: 0 0 10px 0;">Photos facultatives</h5>
                                                                    <p style="color: #828282;font-size: 17px;margin: 0;line-height: 23px;">Si possible en mission ou sur un lieu de travail</p>
                                                                </td>
                                                            </tr>
                                                        </table>                                            
                                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;">
                                                            <tr>
                                                                <td style="border-collapse: collapse;padding-left: 0;">
                                                                    <p style="color: #f8386c;font-size: 14px;margin: 0;opacity: 0.5;line-height: 23px;">* Plus tu ajoutes de photos plus tu auras de chance d'être sélectionné !</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;margin-bottom: 15px;">
                                                            <tr>
                                                                <td align="left" valign="top" style="border-collapse: collapse;padding-left: 0;">
                                                                    <p style="color: #828282;font-size: 17px;margin: 0;line-height: 23px;">
                                                                        Ton profil est ta vitrine personnelle, tu représenteras l'image de l'entreprise<br> qui te choisiras. Plus tes photos sont professionnelles et de bonne qualité, plus le recruteur sera séduit par ton profil.<br><br>

                                                                        Une fois tes photos modifiées, tu recevras un email de confirmation.<br>
                                                                        Nous restons disponibles si besoin
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>                                        
                                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;">
                                                            <tr>
                                                                <td style="border-collapse: collapse;padding-left: 0;">
                                                                    <p style="color: #f8386c;font-size: 17px;font-weight: 600;margin: 30px 0;line-height: 23px;">L'équipe Booking Hôtesses</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="buttonContainer" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;padding-left: 0;width: 100%;background-color: transparent;box-shadow: none;padding: 0;border: 0;display: table;">
                                                            <tr>
                                                                <td class="buttonContainerInner" style="border-collapse: collapse;">
                                                                    <a href="https://app.candidat.booking-hotesses.com/signin" target="_blank" style="background-color: #f8386c;padding: 15px 25px;text-decoration: none;font-weight: 600;font-size: 16px;color: white;text-transform: uppercase;display: block;border-radius: 4px;text-align: center;">
                                                                        Se connecter
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--OK-->
                                            <table class="socialContainer" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin: 0 auto; background-color: #b6c3cc;">
                                                <tr>
                                                    <td style="border-collapse: collapse; padding: 15px 0;">
                                                        <table align="center" style="border-collapse: collapse;">
                                                            <tr width="100%">
                                                                <td align="left" valign="top" width="33.3333333%" style="border-collapse: collapse;">
                                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                                                        <tr>
                                                                            <td style="border-collapse: collapse;">
                                                                                <a href="https://www.facebook.com/Booking-H%C3%B4tesses-626300620844110/?fref=ts" style="background-color: #f8386c; height: 55px; width: 55px; border-radius: 100%; display: block;" target="_blank">
                                                                                    <img style="display: block; height: auto; border: 0; line-height: 100%; outline: none; text-decoration: none;" src="https://www.booking-hotesses.com/ressources/mail/social-icon-01.png" alt="" width="100%" border="0">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="left" valign="top" width="33.3333333%" style="border-collapse: collapse;">
                                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                                                        <tr>
                                                                            <td style="border-collapse: collapse;">
                                                                                <a href="https://twitter.com/BookingHotesses" style="background-color: #f8386c; height: 55px; width: 55px; border-radius: 100%; display: block;" target="_blank">
                                                                                    <img style="display: block; height: auto; border: 0; line-height: 100%; outline: none; text-decoration: none;" src="https://www.booking-hotesses.com/ressources/mail/social-icon-02.png" alt="" width="100%" border="0">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="left" valign="top" width="33.3333333%" style="border-collapse: collapse;">
                                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                                                        <tr>
                                                                            <td style="border-collapse: collapse;">
                                                                                <a href="https://www.instagram.com/bookinghotesses/" style="background-color: #f8386c; height: 55px; width: 55px; border-radius: 100%; display: block;" target="_blank">
                                                                                    <img style="display: block; height: auto; border: 0; line-height: 100%; outline: none; text-decoration: none;" src="https://www.booking-hotesses.com/ressources/mail/social-icon-03.png" alt="" width="100%" border="0">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--OK-->
                                            <table class="copyrightContainer" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin: 0 auto; background-color: #a1adb5;">
                                                <tr width="100%" align="center">
                                                    <td width="100%" style="border-collapse: collapse;padding-left: 0;">
                                                        <p style="color: #ffffff; font-size: 11px; font-weight: 600; margin: 0; padding: 25px 0; opacity: 0.7; line-height: 23px;">© {$mailOptions->footerDate}, Booking Hôtesses<br>Hôtel Technologique, 45 Rue Frédéric Joliot Curie<br>13013 Marseille</p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </center>
            </body>
        </html>

HTML;

return $body;