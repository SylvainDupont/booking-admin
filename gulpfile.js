/**
 * Gulp tasks
 * @desc Automatic tasks manager
 */
const gulp          = require('gulp');
const uglify        = require('gulp-uglify');
const rename        = require('gulp-rename');
const concat        = require('gulp-concat');
const sass          = require('gulp-sass');
const gutil         = require('gulp-util');
const ftp           = require('vinyl-ftp');
const notify        = require('gulp-notify');
const ts            = require('gulp-typescript');
const annotate      = require('gulp-ng-annotate');
const connect       = require('gulp-connect-php');
const addsrc        = require('gulp-add-src');
const jshint 		= require('gulp-jshint');
const stylish       = require('jshint-stylish');
const livereload    = require('gulp-livereload');
const prefix        = require('gulp-autoprefixer');
const gulpUtil      = require('gulp-util');
const babel         = require('gulp-babel');


const src = {
	folder: './_src/',
	html: './_src/**/*.html',
	php: './_src/**/*.php',
	sass: {
		main: './_src/global.scss',
		all: '_src/**/*.scss'
	},
	css: {
		material: './_src/css/material.css'
	},
	js: {
		app: [
			'./_src/config/core.js',
			'./_src/config/*.js',
			'./_src/components/**/*.js',
			'./_src/directives/**/*.js',
			'./_src/filters/*.js',
			'./_src/services/*.js',
		],
		libs: [
			'./_src/libs/ieDetector.js',
			'./_src/libs/firebaseConfig.js',
			'./_src/libs/firebase-util.min.js',
			'./_src/libs/angular.min.js',
			'./_src/libs/angular-animate.min.js',
			'./_src/libs/angular-aria.min.js',
			'./_src/libs/angular-ui-router.min.js',
			'./_src/libs/angular-material.js',
			'./_src/libs/angular-messages.min.js',
			'./_src/libs/angular-sanitize.min.js',
			'./_src/libs/angular-component-router.js',
			'./_src/libs/angular-component-router-active.js',
			'./_src/libs/angularfire.js',
			'./_src/libs/ng-infinite-scroll.min.js',
			'./_src/libs/countdown.min.js',
			'./_src/libs/moment.min.js',
			'./_src/libs/moment-countdown.min.js',
			'./_src/libs/moment-locale-fr.js',
			'./_src/libs/swiper.min.js',
			'./_src/libs/underscore-min.js',
			'./_src/libs/fastclick.min.js',
			'./_src/libs/firebase-util.min.js',
			'./_src/libs/underscore-min.js',
			'./_src/libs/angular-locale_fr-fr.min.js',
			'./_src/libs/facebookInit.js',
			'./_src/libs/jspdf.debug.js',
			'./_src/libs/html2canvas.min.js'
		],
		all: './_src/**/*.js'
	}
};

const dist = {
	folder: './_dist/',
	js: {
		folder: './_dist/js/',
		app: 'app.min.js',
	},
	sass: {
		folder: './_dist/css/'
	},
	img: {
		folder: './_dist/img/'
	},
	icons: {
		folder: './_dist/img/icons/'
	}
};

const serverHost = {
	aries: '192.168.2.32',
	ironova: '192.168.10.104',
	local: 'localhost',
	home: '192.168.1.37'
};

/**
 * app
 * @desc Build all js files to bundle and move to dist folder
 */
gulp.task('app', () => {

	gulp.src(src.js.app)
		.pipe(annotate())
		.on("error", notify.onError({
			message: 'Error: <%= error.message %>'
		}))
		.pipe(jshint())
    	.pipe(jshint.reporter('jshint-stylish'))
    	.pipe(babel({
            presets: ['es2015']
        }))
		.pipe(addsrc.prepend(src.js.libs))
		.pipe(uglify().on('error', gulpUtil.log))
		.pipe(concat(dist.js.app))
		.pipe(gulp.dest(dist.js.folder))
		.pipe(livereload());
});

/**
 * sass
 * @desc Compile sass to css and move bundle to dist folder
 */
gulp.task('sass', () => {
	gulp.src([src.sass.main, src.sass.all])
		.pipe(sass({
			outputStyle: 'compressed'
		})
		.on("error", notify.onError({
			message: 'Error: <%= error.message %>'
		})))
		.pipe(prefix('last 30 version', '> 1%', 'ie 8', 'ie 9'))
		.pipe(rename({
			extname: '.min.css'
		}))
		.pipe(gulp.dest(dist.sass.folder))
		.pipe(livereload());
	gulp.src([src.css.material])
	    .pipe(gulp.dest(dist.sass.folder))
});
gulp.task('css', () => {
	gulp.src([src.css.material])
		.pipe(gulp.dest(dist.sass.folder))
		.pipe(livereload());
});
gulp.task('img', () => {
	gulp.src(['./_src/img/*'])
		.pipe(gulp.dest(dist.img.folder))
		.pipe(livereload());
	gulp.src(['./_src/img/icons/*'])
		.pipe(gulp.dest(dist.icons.folder))
		.pipe(livereload());
});

/**
 * html
 * @desc Move html files to dist folder
 */
gulp.task('html', () => {
	gulp.src(src.html)
		.pipe(gulp.dest(dist.folder))
		.pipe(livereload());
});

/**
 * php
 * @desc Move php files to dist folder
 */
gulp.task('php', () => {
	gulp.src(src.php)
		.pipe(gulp.dest(dist.folder));
});

/**
 * server
 * @desc Start web server
 */
gulp.task('server', ['build', 'watch'], () => {
	connect.server({
		base: dist.folder,
		port: 9099,
		hostname: serverHost.local,
		keepalive: true,
		open: true,
		livereload: false,
		fallback: dist + 'index.html',
		// /php5.5.10
		bin: '/Applications/MAMP/bin/php/php5.6.40/bin/php',
    ini: '/Applications/MAMP/bin/php/php5.6.40/conf/php.ini',
	});
});

// gulp.task('server', ['build', 'watch'], () => {
// 	connect.server({
// 		base: dist.folder,
// 		port: 9009,
// 		hostname: serverHost.local,
// 		keepalive: true,
// 		open: true,
// 		livereload: true,
// 		fallback: dist + 'index.html',
// 		bin: '/Applications/MAMP/bin/php/php7.2.34/bin/php',
//     	ini: '/Applications/MAMP/bin/php/php7.2.34/conf/php.ini',
// 	});
// });

/**
 * watch
 * @desc Watch all files
 */
gulp.task('watch', () => {

	livereload.listen();

	gulp.watch([src.sass.main, src.sass.all], ['sass']);
	gulp.watch(src.js.all, ['app']);
	gulp.watch(src.html, ['html']);
	gulp.watch(src.php, ['php']);
});

/**
 * build
 * @desc Build web application task
 */
gulp.task('build', ['app', 'sass', 'html', 'php', 'css', 'img']);

/**
 * default
 * @desc Default gulp task
 */
gulp.task('default', ['server']);
