## Mise en production

### Gestion des admins
Créer un utilisateur gestion@booking-hotesses.com sur le comptee firebase, récupérer l'id et l'insérer dans le fichier firebaseSources/admin.json et l'importer sur firebase. Attention dans ce fichier reprendre la bonne clé également de manager@booking-hotesses.com

### Gestion des nouvelles données
Importer les fichiers :
- firebaseSources/marques.json
- firebaseSources/professions.json
- firebaseSources/specialites.json
